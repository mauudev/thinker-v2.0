#include "PythonScript.h"

PythonScript::PythonScript()
{
}

PythonScript::~PythonScript()
{
}

void PythonScript::setFileName(const string &p_fileName)
{
    m_fileName = p_fileName;
    if(m_fileName.find(PY_FILE) == std::string::npos)
    {
        m_fileName += PY_FILE;
    }
}

void PythonScript::setOutput(const string &p_output)
{
    m_output = p_output;
}

void PythonScript::setScriptPath(const string &p_scriptPath)
{
    m_scriptPath = p_scriptPath;
    if (p_scriptPath.back() == '/')
    {
        m_scriptPath.pop_back();
    }
}

string PythonScript::getFileName() const
{
    return m_fileName;
}

string PythonScript::getOutput() const
{
    return m_output;
}

string PythonScript::getScriptPath() const
{
    return m_scriptPath;
}