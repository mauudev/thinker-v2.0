#include "PythonReader.h"
#include "Definitions.h"

#include <fstream>
#include <iostream>

PythonReader::PythonReader(const string &p_scriptPath,
    const string &p_fileName)
{
    setScriptPath(p_scriptPath);
    setFileName(p_fileName);

    ifstream stream(p_scriptPath + SLASH_CHAR + getFileName());
    m_scriptContent.assign((istreambuf_iterator<char>(stream)),
        (istreambuf_iterator<char>()));
}

PythonReader::~PythonReader()
{
}

string PythonReader::getScriptContent() const
{
    return m_scriptContent;
}

vector<string> PythonReader::getLines() const
{
    vector<string> lines;
    int size = m_scriptContent.size();
    string cont = "";
    for(int i = 0; i < size; i++)
    {
        cont +=  m_scriptContent[i];
        if(m_scriptContent[i] == '\n' || (i == (size - 1))) 
        {
            string ns = cont;
            lines.push_back(ns);
            cont = "";
        }
    }
    return lines;
}