#include "PythonException.h"
#include "Definitions.h"

#include <fstream>
#include <cstring>
#include <algorithm>
#include <sstream>
#include <iterator>
#include <iostream>

PythonException::PythonException(const string &p_file)
{
    m_errorToString = DUMP_DIR + p_file;
    ifstream stream(m_errorToString);
    m_errorToString.assign((istreambuf_iterator<char>(stream)),
        (istreambuf_iterator<char>()));
    evaluateException();
}

PythonException::~PythonException()
{
}

void PythonException::evaluateException()
{
    size_t len = strlen(m_errorToString.c_str());
    char *completeException = new char[len + 1];
    memcpy(completeException, m_errorToString.c_str(), len + 1);
    char *iterator = strtok (completeException, ",\n");
    while(iterator != NULL)
    {
        evaluateLine(iterator);
        iterator = strtok (NULL, ",\n");
    }
    if(!m_errorStack.empty())
    {
        m_errorMessage = "Error On : " + m_errorFile + "\n" + m_errorType + 
            "\nFound in: Line " + to_string(m_errorStack.back().first) + "\n" +
            m_errorStack.back().second;
    }
    delete [] completeException;
    delete [] iterator;
}

void PythonException::evaluateLine(const char *p_line)
{
    string realLine{p_line};
    if(realLine.find("File") != string::npos)
    {
        m_errorFile = realLine;
    }
    else if(realLine.find("Error") != string::npos)
    {
        m_errorType = realLine;
    }
    else if(realLine.find(" line ") != string::npos)
    {
        realLine.erase(realLine.begin(), realLine.begin() + 5);
        try
        {
            m_errorStack.push_back(make_pair(stoi(realLine), ""));
        }
        catch(...)
        {
            cout << "Can't format the error, please use toString()\n";
        }
    }
    else if((realLine.find("Traceback ")) == string::npos &&
        (realLine.find(" in ") == string::npos) &&
        (realLine.find("^") == string::npos))
    {
        realLine.erase(remove(realLine.begin(), realLine.end(), '\t'),
            realLine.end());
        if(!m_errorStack.empty())
        {
            m_errorStack.back().second = realLine;
        }
    }
}

const char *PythonException::what() const noexcept
{
    return m_errorMessage.c_str();
}

string PythonException::type() const
{
    return m_errorType;
}

string PythonException::file() const
{
    return m_errorFile;
}

string PythonException::stackTrace() const
{
    string result = "StackTrace:\n";
    for (vector<pair<int,string>>::const_iterator iterator =
        m_errorStack.begin();
        iterator != m_errorStack.end(); ++iterator)
    {
        result = result + "On line : " + to_string(iterator->first) +
            "\n=>" + iterator->second + "\n";
    }
    return result;
}

string PythonException::toString() const
{
    return m_errorToString;
}