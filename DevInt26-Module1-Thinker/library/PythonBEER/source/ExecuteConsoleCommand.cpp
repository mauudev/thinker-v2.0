#include "ExecuteConsoleCommand.h"
#include "Definitions.h"
#include "PythonException.h"

#include <sys/stat.h>
#include <ctype.h>
#include <cstring>
#include <memory>
#include <algorithm>
#include <fstream>

ExecuteConsoleCommand::ExecuteConsoleCommand()
{
    struct stat statStruct;
    if(stat(DUMP_DIR, &statStruct) != 0)
    {
        string createPath = CREATE_DIR + string{DUMP_DIR};
        system(createPath.c_str());
    }
}

string ExecuteConsoleCommand::operator()(const string &p_command) const
{
    string withoutSpaces = p_command;
    withoutSpaces.erase(
        remove_if(
            withoutSpaces.begin(),
            withoutSpaces.end(),
            [](char c)
            { 
                return (c =='\r' || c == '\t' || c == ' '
                    || c == '\n' || c == '/');
            }
        ),
        withoutSpaces.end());
    withoutSpaces += ERROR_TXT; 
    string realCommand = p_command + DUMP + withoutSpaces;
    string result;
    array<char, 128> buffer;
    // Runs the command
    shared_ptr<FILE> pipe(popen(realCommand.c_str(), "r"), pclose);
    while(!feof(pipe.get()))
    {
        if(fgets(buffer.data(), 128, pipe.get()) != nullptr)
        {
            result += buffer.data();
        }
    }
    // Exception validation
    ifstream stream(DUMP_DIR + withoutSpaces);
    realCommand.assign((istreambuf_iterator<char>(stream)),
        (istreambuf_iterator<char>()));
    // If the content of the file is not empty the
    // subprocess throwed an exception.
    if(!realCommand.empty())
    {
        throw PythonException{withoutSpaces};
    }
    else
    {
        // If is empty, remove the file created.
        realCommand = REMOVE_FILE + (DUMP_DIR + withoutSpaces);
        system(realCommand.c_str());
    }
    return result;
}