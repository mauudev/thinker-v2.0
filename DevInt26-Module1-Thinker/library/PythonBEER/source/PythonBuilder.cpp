#include "PythonBuilder.h"
#include "ExecuteConsoleCommand.h"

#include <sys/stat.h>
#include <fstream>
#include <ostream>

PythonBuilder::PythonBuilder(const string &p_fileName)
:m_fileFlag{false}
{
    setScriptPath(SCRIPTS_PATH);
    setFileName(p_fileName);
    struct stat statStruct;
    if(stat(getScriptPath().c_str(), &statStruct) != 0)
    {
        string createPath = CREATE_DIR + getScriptPath();
        ExecuteConsoleCommand executor;
        executor(createPath);
    }
}

PythonBuilder::PythonBuilder(const string &p_scriptPath, 
    const string &p_fileName)
:m_fileFlag{false}
{
    setScriptPath(p_scriptPath);
    setFileName(p_fileName);
    struct stat statStruct;
    if(stat(getScriptPath().c_str(), &statStruct) != 0)
    {
        string createPath = CREATE_DIR+getScriptPath();
        ExecuteConsoleCommand executor;
        executor(createPath);
    }
}

PythonBuilder::~PythonBuilder()
{
}

bool PythonBuilder::getFileFlag() const
{
    return m_fileFlag;
}

void PythonBuilder::createPythonFile()
{
    try
    {
        string completeDir = getScriptPath() + SLASH_CHAR + getFileName();
        ofstream scriptBuilder{completeDir.c_str()};
        scriptBuilder << generateCode();
        scriptBuilder.close();
        m_fileFlag = true;
    }
    catch(...)
    {
        m_fileFlag = false;
    }
}

