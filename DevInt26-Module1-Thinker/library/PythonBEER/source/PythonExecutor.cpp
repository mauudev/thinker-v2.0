#include "PythonExecutor.h"
#include "ExecuteConsoleCommand.h"
#include "PythonException.h"

#include <sys/stat.h>
#include <cstring>
#include <iostream>

PythonExecutor::PythonExecutor()
{
}

PythonExecutor::PythonExecutor(const string &p_scriptPath,
    const string &p_fileName)
{
    setScriptPath(p_scriptPath);
    setFileName(p_fileName);
}

PythonExecutor::~PythonExecutor()
{
}

bool PythonExecutor::run()
{
    string completeDir = getScriptPath() + SLASH_CHAR + getFileName();

    struct stat buffer;
    if(stat(completeDir.c_str(), &buffer) == 0)
    {
        ExecuteConsoleCommand executor;
        string command = PYTHON_CONSOLE + completeDir;
        setOutput(executor(command));
        return true;
    }
    return false;
}

bool PythonExecutor::erase()
{
    string completeDir = getScriptPath() + SLASH_CHAR + getFileName();

    struct stat buffer;
    if(stat(completeDir.c_str(), &buffer) == 0)
    {
        ExecuteConsoleCommand executor;
        string command = REMOVE_FILE + completeDir;
        executor(command);
        return true;
    }
    return false;
}