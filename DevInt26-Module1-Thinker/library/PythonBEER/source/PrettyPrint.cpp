#include "PrettyPrint.h"
#include "PrettyPrintDefinitions.h"

PrettyPrint::PrettyPrint()
{
}

PrettyPrint::~PrettyPrint()
{
}

// Color Fonts
string PrettyPrint::redFont(const string &p_source) const
{
    return COLOR_RED + p_source + END;
}

string PrettyPrint::blueFont(const string &p_source) const
{
    return COLOR_BLUE + p_source + END;
}

string PrettyPrint::yellowFont(const string &p_source) const
{
    return COLOR_YELLOW + p_source + END;
}

string PrettyPrint::greenFont(const string &p_source) const
{
    return COLOR_GREEN + p_source + END;
}

string PrettyPrint::magentaFont(const string &p_source) const
{
    return COLOR_MAGENTA + p_source + END;
}

string PrettyPrint::whiteFont(const string &p_source) const
{
    return COLOR_WHITE + p_source + END;
}

string PrettyPrint::cyanFont(const string &p_source) const
{
    return COLOR_CYAN + p_source + END;
}

// Font styles
string PrettyPrint::boldFont(const string &p_source) const
{
    return BOLD_FONT + p_source + END;
}

string PrettyPrint::underLineFont(const string &p_source) const
{
    return UNDER_LINE_FONT + p_source + END;
}

// Color Bold Fonts
string PrettyPrint::redBoldFont(const string &p_source) const
{
    return boldFont(redFont(p_source));
}

string PrettyPrint::blueBoldFont(const string &p_source) const
{
    return boldFont(blueFont(p_source));
}

string PrettyPrint::whiteBoldFont(const string &p_source) const
{
    return boldFont(whiteFont(p_source));    
}

string PrettyPrint::cyanBoldFont(const string &p_source) const
{
    return boldFont(cyanFont(p_source));    
}

string PrettyPrint::yellowBoldFont(const string &p_source) const
{
    return boldFont(yellowFont(p_source));    
}

string PrettyPrint::greenBoldFont(const string &p_source) const
{
    return boldFont(greenFont(p_source));    
}

string PrettyPrint::magentaBoldFont(const string &p_source) const
{
    return boldFont(magentaFont(p_source));    
}

// Color Underline Fonts
string PrettyPrint::redUnderLineFont(const string &p_source) const
{
    return underLineFont(redFont(p_source));
}

string PrettyPrint::blueUnderLineFont(const string &p_source) const
{
    return underLineFont(blueFont(p_source));
}

string PrettyPrint::whiteUnderLineFont(const string &p_source) const
{
    return underLineFont(whiteFont(p_source));    
}

string PrettyPrint::cyanUnderLineFont(const string &p_source) const
{
    return underLineFont(cyanFont(p_source));    
}

string PrettyPrint::yellowUnderLineFont(const string &p_source) const
{
    return underLineFont(yellowFont(p_source));    
}

string PrettyPrint::greenUnderLineFont(const string &p_source) const
{
    return underLineFont(greenFont(p_source));    
}

string PrettyPrint::magentaUnderLineFont(const string &p_source) const
{
    return underLineFont(magentaFont(p_source));    
}