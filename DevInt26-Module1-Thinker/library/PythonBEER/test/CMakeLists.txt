CMAKE_MINIMUM_REQUIRED(VERSION 3.0.0)
PROJECT(PythonBEER_tests)

ADD_COMPILE_OPTIONS(-std=c++11)

INCLUDE_DIRECTORIES(../include)
#ADD_DEFINITIONS(-DGTEST)

ADD_SUBDIRECTORY(../../../thirdparty/googletest googletest)

ENABLE_TESTING()

SET(src_program 
                PythonBuilderTest.cpp
                PythonExecutorTest.cpp
                PythonExceptionTest.cpp
                PythonReaderTest.cpp
                ExecuteConsoleCommandTest.cpp
                ../source/ExecuteConsoleCommand.cpp
                ../source/PythonScript.cpp
                ../source/PythonBuilder.cpp
                ../source/PythonExecutor.cpp
                ../source/PythonException.cpp
                ../source/PythonReader.cpp
                ../source/PrettyPrint.cpp
                TestMain.cpp
)

ADD_EXECUTABLE(PythonBEER_tests ${src_program})

TARGET_LINK_LIBRARIES(PythonBEER_tests gtest gtest_main -lpthread)

ADD_TEST(PythonBEER_tests PythonBEER_tests)