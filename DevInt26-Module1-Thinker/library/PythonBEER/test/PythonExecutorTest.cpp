#include "PythonExecutor.h"
#include "PythonBuilder.h"
#include "gtest/gtest.h"
#include <cstring>
#include <string>

using namespace std;


TEST(ExecutePythonFile, PythonExecutorTest)
{
    string expected;

    PythonExecutor executor{"/home/ubuntu/Desktop", "SecondAgent.py"};
    if(executor.run())
    {
        expected="This line will be printed\n";
    }

    ASSERT_EQ(expected, executor.getOutput());
}

TEST(ExecutePythonFileNotRunning, PythonExecutorTest)
{
    string expected="This line will be printed\n";
    PythonExecutor executor{"/home/ubuntu/Desktop", "SecondAgen"};
    if(executor.run())
    {
        ASSERT_NE(expected, executor.getOutput());
    }
}

// Tecnically this function can erase any archive in the
// computer, if the dir and name are correct
TEST(ErasePythonFile, PythonExecutorTest)
{
    string scriptPath = "/home/ubuntu/Desktop/";
    string fileName = "SecondAgent.py";
    PythonExecutor executor{scriptPath, fileName};
    bool validator = executor.erase();

    string completeDir = scriptPath + fileName;

    // Verifying if the dir still exists in the computer
    struct stat buffer;
    if(stat(completeDir.c_str(), &buffer) == 0)
    {
        validator = false;
    }

    ASSERT_TRUE(validator);
}
// As should be the function Eraser returns a boolean
// that indicates if it worked or not.
// In this case, the file /home/Desktop/SecondAgent.py does not exist.
TEST(ErasePythonFileWithWrongDir, PythonExecutorTest)
{
    string scriptPath = "Scripts/";
    string fileName = "FirstAgen.py";
    PythonExecutor executor{scriptPath, fileName};
    bool validator = executor.erase();

    string completeDir = scriptPath + fileName;

    // Verify if the dir still exists in the computer
    struct stat buffer;
    if(stat(completeDir.c_str(), &buffer) == 0 && validator)
    {
        validator = false;
    }

    ASSERT_FALSE(validator);
}

class ScriptClass : public PythonBuilder, public PythonExecutor
{
public:
    ScriptClass(const string &p_name)
    :PythonBuilder{p_name}
    {
    }

    ~ScriptClass()
    {
    }

    string generateCode() override
    {
        return "print('This line will be printed')";
    }
};
// You can inherit from PythonBuilder and PythonExecutor
// as the class above does. So you can create, run and erase
// your own Python File.
TEST(BuildAndExecuteFile, PythonExecutorTest)
{
    bool validator;

    ScriptClass builderTest{"BuildAndExecute.py"};
    builderTest.createPythonFile();
    validator = builderTest.run();

    string expected = "This line will be printed\n";

    ASSERT_TRUE(builderTest.getFileFlag());
    ASSERT_EQ(expected, builderTest.getOutput());
}

TEST(BuildAndEraseTest, PythonExecutor)
{
    bool validator;

    ScriptClass builderTest{"BuildAndErase.py"};
    builderTest.createPythonFile();
    validator = builderTest.erase();

    ASSERT_TRUE(builderTest.getFileFlag());
    ASSERT_TRUE(validator);
}

TEST(ExecuteFileWithOutBuild, PythonExecutor)
{
    bool validator;

    ScriptClass builderTest{"Exec.py"};
    validator = builderTest.run();

    string expected = "";

    ASSERT_FALSE(builderTest.getFileFlag());
    ASSERT_EQ(expected, builderTest.getOutput());
}