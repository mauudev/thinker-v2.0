#include "PythonReader.h"
#include "gtest/gtest.h"
#include <iostream>

using namespace std;

TEST(ReadPythonFile, PythonReaderTest)
{
    string expected = "print('This lines will be printed)";

    PythonReader pr{"Scripts/", "FirstAgent.py"};

    ASSERT_EQ(expected, pr.getScriptContent());
}

TEST(ReadPythonFileWithWrongDir, PythonReaderTest)
{
    try
    {
        string expected = "print('This line will be printed)\n";

        PythonReader pr{"Scripts/", "FirstAgn"};
    }
    catch(...)
    {
        ASSERT_TRUE(true);
    }
}