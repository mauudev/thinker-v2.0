#include "PythonBuilder.h"
#include "gtest/gtest.h"
#include <sys/stat.h>
#include <cstring>
#include <string>
// PythonBuilder is a class that declares generateCode() as a pure
// abstract method so the only thing you need to do is
// implement that method and call the function createPythonFile()
class TestClass : public PythonBuilder
{
public:
    TestClass(const string &p_name)
    :PythonBuilder{p_name}
    {
    }

    TestClass(const string &p_scriptPath, const string &p_name)
    :PythonBuilder(p_scriptPath, p_name)
    {
    }

    ~TestClass()
    {
    }

    string generateCode() override
    {
        return "print('This line will be printed')";
    }
};

class TestClass2 : public PythonBuilder
{
public:
    TestClass2(const string &p_name)
    :PythonBuilder{p_name}
    {
    }

    TestClass2(const string &p_scriptPath, const string &p_name)
    :PythonBuilder(p_scriptPath, p_name)
    {
    }

    ~TestClass2()
    {
    }

    string generateCode() override
    {
        return "print('This lines will be printed)";
    }
};
// Python Builder gives the possibility to just call its contructor
// with the name of the file you want and creating the file
// in the default directory (Scripts/)
TEST(generatePythonFile, PythonBuilderTest)
{
    bool validator = false;
    
    TestClass2 builderTest{"FirstAgent"};
    builderTest.createPythonFile();

    string completeDir = builderTest.getScriptPath() + SLASH_CHAR +
        builderTest.getFileName();
    // Verifying if the dir still exists in the computer
    struct stat buffer;
    if(stat(completeDir.c_str(), &buffer) == 0)
    {
        validator = true;
    }
    ASSERT_TRUE(validator && builderTest.getFileFlag());
}
// Python Builder gives the possibility to call its contructor
// with the full path and name of the file you want
TEST(generatePythonFileWithPathAndFullName, PythonBuilderTest)
{
    bool validator = false;

    TestClass builderTest{"/home/ubuntu/Desktop/", "SecondAgent.py"};
    builderTest.createPythonFile();

    string completeDir = builderTest.getScriptPath() + SLASH_CHAR +
        builderTest.getFileName();

    // Verifying if the dir still exists in the computer
    struct stat buffer;
    if(stat(completeDir.c_str(), &buffer) == 0)
    {
        validator = true;
    }

    ASSERT_TRUE(validator && builderTest.getFileFlag());
}
// This test actually should work but the PythonBuilder
// throws a exception because we dont have the Permission
// to create a file in that dir.
TEST(generatePythonFileWithWrongPath, PythonBuilderTest)
{
    bool validator;
    try
    {
        TestClass builderTest{"/home/Desktop/", "SecondAgent.py"};
        builderTest.createPythonFile();
        string completeDir = builderTest.getScriptPath() + SLASH_CHAR +
            builderTest.getFileName();

        // Verifying if the dir still exists in the computer
        struct stat buffer;
        if(stat(completeDir.c_str(), &buffer) == 0)
        {
            validator = true;
        }
    }
    catch(...)
    {
        validator = false;
    }

    ASSERT_FALSE(validator);
}