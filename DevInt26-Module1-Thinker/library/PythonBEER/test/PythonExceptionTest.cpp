#include "PythonExecutor.h"
#include "gtest/gtest.h"
#include "PythonException.h"

using namespace std;

TEST(PythonExceptionWhat, PythonException)
{
    string expected = "Error On :   File \"Scripts/FirstAgent.py\"\n"
        "SyntaxError: EOL while scanning string literal\nFound in: Line 1\n"
        "    print('This lines will be printed)";
        string result;
    try
    {
        PythonExecutor exec{"Scripts/", "FirstAgent"};
        if (exec.run())
        {
            result = exec.getOutput();
        }
    }
    catch(const PythonException &e)
    {
        result = e.what();
    }
    ASSERT_EQ(expected, result);
}

TEST(PythonExceptionStackTrace, PythonException)
{
    string expected = "StackTrace:\nOn line : 1\n=>"
        "    print('This lines will be printed)\n";
    string result;
    try
    {
        PythonExecutor exec{"Scripts/", "FirstAgent"};
        if (exec.run())
        {
            result = exec.getOutput();
        }
    }
    catch(const PythonException &e)
    {
        result = e.stackTrace();
    }
    ASSERT_EQ(expected, result);
}

TEST(PythonExceptionType, PythonException)
{
    string expected = "SyntaxError: EOL while scanning string literal";
    string result;
    try
    {
        PythonExecutor exec{"Scripts/", "FirstAgent"};
        if (exec.run())
        {
            result = exec.getOutput();
        }
    }
    catch(const PythonException &e)
    {
        result = e.type();
    }
    
    ASSERT_EQ(expected, result);
}

TEST(PythonExceptionFile, PythonException)
{
    string expected = "  File \"Scripts/FirstAgent.py\"";
    string result;
    try
    {
        PythonExecutor exec{"Scripts/", "FirstAgent"};
        if (exec.run())
        {
            result = exec.getOutput();
        }
    }
    catch(const PythonException &e)
    {
        result = e.file();
    }

    ASSERT_EQ(expected, result);
}