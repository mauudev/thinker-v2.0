#include "ExecuteConsoleCommand.h"
#include "gtest/gtest.h"
#include <iostream>

using namespace std;

// This is a powerfull functor that does the ugly part
// and allows its user to execute a console command
// with just a string and obtains the standard output.
TEST(ExecuteCustomCommandInTheConsole, ExecuteConsoleCommand)
{
    //This command is a simple print in the linux terminal
    string command = "printf \"It works!\"";
    ExecuteConsoleCommand executor;
    string result = executor(command);
    string expected = "It works!"; 
    ASSERT_EQ(expected, result);
}

TEST(ExecuteCustomCommandInTheConsole2, PythonRExecuteConsoleCommand)
{
    //This command is a simple print in the linux terminal
    string command = "printf \"Hello\n\tWorld!\n\"";
    ExecuteConsoleCommand executor;
    string result = executor(command);
    string expected = "Hello\n\tWorld!\n"; 
    // Functor returns a string that will have special characters.
    ASSERT_EQ(expected, result);
}