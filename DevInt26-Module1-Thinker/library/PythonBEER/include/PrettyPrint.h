#ifndef PRETTY_PRINT_H
#define PRETTY_PRINT_H

#include <string>

using namespace std;

class PrettyPrint
{
public:
    PrettyPrint();
    ~PrettyPrint();

    // FONT COLORS
    string redFont(const string&) const;
    string blueFont(const string&) const;
    string yellowFont(const string&) const;
    string greenFont(const string&) const;
    string magentaFont(const string&) const;
    string whiteFont(const string&) const;
    string cyanFont(const string&) const;

    // FONT STYLES
    string boldFont(const string&) const;
    string underLineFont(const string&) const;

    // COMBINATIONS
    string redBoldFont(const string&) const;
    string yellowBoldFont(const string&) const;
    string blueBoldFont(const string&) const;
    string cyanBoldFont(const string&) const;
    string greenBoldFont(const string&) const;
    string whiteBoldFont(const string&) const;
    string magentaBoldFont(const string&) const;

    string redUnderLineFont(const string&) const;
    string yellowUnderLineFont(const string&) const;
    string blueUnderLineFont(const string&) const;
    string cyanUnderLineFont(const string&) const;
    string greenUnderLineFont(const string&) const;
    string whiteUnderLineFont(const string&) const;
    string magentaUnderLineFont(const string&) const;
};

#endif