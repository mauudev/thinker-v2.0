#ifndef PYTHON_EXECUTOR_H
#define PYTHON_EXECUTOR_H

#include "PythonScript.h"
#include "Definitions.h"
#include <string>

using namespace std;

class PythonExecutor : public virtual PythonScript
{
private:
    void evaluateOutput(const string&);

protected:
    PythonExecutor();

public:
    PythonExecutor(const string&, const string&);
    ~PythonExecutor();

    bool run();
    bool erase();
};

#endif