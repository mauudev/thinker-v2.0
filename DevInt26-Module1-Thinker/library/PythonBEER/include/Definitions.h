#ifndef DEFINITIONS_H
#define DEFINITIONS_H

#define CREATE_DIR "mkdir -p "
#define SCRIPTS_PATH "Scripts/"
#define PYTHON_CONSOLE "python "
#define SLASH_CHAR "/"
#define PY_FILE ".py"
#define REMOVE_FILE "rm "
#define DUMP " 2>Dump/"
#define DUMP_DIR "Dump/"
#define ERROR_TXT "-ERROR.txt"

#endif