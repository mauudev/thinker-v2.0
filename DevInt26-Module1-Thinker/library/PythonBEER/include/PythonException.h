#ifndef PYTHON_EXCEPTION_H
#define PYTHON_EXCEPTION_H

#include <exception>
#include <string>
#include <vector>

using namespace std;

class PythonException : public exception
{
private:
    string m_errorMessage;
    string m_errorToString;
    string m_errorType;
    string m_errorFile;
    vector<pair<int, string>> m_errorStack;

    void evaluateException();
    void evaluateLine(const char*);
    
public:
    PythonException(const string&);
    ~PythonException();
    
    const char *what() const noexcept override;
    string type() const;
    string file() const;
    string stackTrace() const;
    string toString() const;
};

#endif