#ifndef EXECUTE_CONSOLE_COMMAND_H
#define EXECUTE_CONSOLE_COMMAND_H

#include <string>

using namespace std;

struct ExecuteConsoleCommand
{
public:
    ExecuteConsoleCommand();
    string operator()(const string&) const; 
};

#endif