#ifndef PYTHON_SCRIPT_H
#define PYTHON_SCRIPT_H

#include "Definitions.h"
#include <string>

using namespace std;

class PythonScript
{
private:
    string m_scriptPath;
    string m_fileName;
    string m_output;

protected:
    PythonScript();
    
    void setFileName(const string&);
    void setOutput(const string&);
    void setScriptPath(const string&);

public:
    virtual ~PythonScript();

    string getFileName() const;
    string getOutput() const;
    string getScriptPath() const;
};

#endif