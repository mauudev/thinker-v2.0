#ifndef PYTHON_BUILDER_H
#define PYTHON_BUILDER_H

#include "PythonScript.h"
#include "Definitions.h"

#include <string>

using namespace std;

class PythonBuilder : public virtual PythonScript
{
private:
    bool m_fileFlag;

public:
    PythonBuilder(const string&);
    PythonBuilder(const string&, const string&);
    ~PythonBuilder();

    bool getFileFlag() const;

    void createPythonFile(); 
    virtual string generateCode() = 0;
};

#endif