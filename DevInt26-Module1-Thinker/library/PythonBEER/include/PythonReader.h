#ifndef PYTHON_READER_H
#define PYTHON_READER_H

#include "PythonScript.h"

#include <string>
#include <vector>

using namespace std;

class PythonReader : public virtual PythonScript
{
private:
    string m_scriptContent;

public:
    PythonReader(const string&, const string&);
    ~PythonReader();

    string getScriptContent() const;
    vector<string> getLines() const;
};

#endif