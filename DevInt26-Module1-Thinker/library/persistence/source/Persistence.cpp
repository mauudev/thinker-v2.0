#include "../include/Persistence.h"
#include <iomanip>      // setw

Persistence::Persistence()
{
    m_fileName = "default.json"; // default fileName
}

Persistence::Persistence(const string &p_fileName)
{
    m_fileName = p_fileName;
}

Persistence::~Persistence()
{
}

void Persistence::setFileName(const string &p_fileName)
{
    m_fileName = p_fileName;
}

string Persistence::getFileName()
{
    return m_fileName;
}

void Persistence::setJsonFile(const Json &p_jsonFile)
{
    m_jsonFile = p_jsonFile;
}

void Persistence::save()
{
    Json fileJson = serialize();
    setJsonFile(fileJson); 
    ofstream fileWriter {m_fileName};
    fileWriter << setw(4)<< m_jsonFile << "\n";
    fileWriter.close();
    cout << "******* JsonFile export to "
        + getFileName() + " succesfully! *******\n";
}

//LoadJson - SaveJson -> implementacion Institucion (Ipersistence)
void Persistence::load()
{
    string nameJsonFile = getFileName();// + ".json";
    Json jsonFile;
    ifstream fileJson (nameJsonFile);
    if(fileJson.good())
    {
        fileJson >> jsonFile;
    }
    else
    {
        cout<< "******* Don't exist Json File to import. *******\n";
        exit(-1);
    }
    fileJson.close();
    deserialize(jsonFile);
    cout << "******* JsonFile import to "
        + getFileName() +" succesfully! *******\n";
}

void Persistence::save(const string &p_fileNameJson)
{
    Json fileJson = serialize();
    // setJsonFile(fileJson);
    ofstream fileWriter {p_fileNameJson};
    fileWriter << setw(4)<< fileJson << "\n";
    fileWriter.close();

    cout << "******* JsonFile export to "
        + p_fileNameJson + " succesfully! *******\n";
}

// donde directamente recibe la ruta y nombre del archivo.json
void Persistence::load(const string &p_fileNameJson)
{
    Json jsonFile;
    ifstream fileJson (p_fileNameJson);
    if(fileJson.good())
    {
        fileJson >> jsonFile;
    }
    else
    {
        cout<< "******* Don't exist Json File to import. *******\n";
        exit(-1);
    }
    fileJson.close();
    deserialize(jsonFile);
    cout << "******* JsonFile import to "
        + p_fileNameJson +" succesfully! *******\n";
}
