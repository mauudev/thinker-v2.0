cmake_minimum_required(VERSION 3.0.0)
set(NAME_PROJECT persistence)
project(${NAME_PROJECT})

add_compile_options(-std=c++11)

INCLUDE_DIRECTORIES(../../thirdparty/jsonlib)

set(library
    source/Persistence.cpp
)

add_library(persistence STATIC
    ${library}
)

# Specify here the include directories exported
# by this library
target_include_directories(persistence PUBLIC
    ${CMAKE_CURRENT_SOURCE_DIR}/include
)
