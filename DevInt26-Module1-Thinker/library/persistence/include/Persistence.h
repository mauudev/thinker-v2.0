#ifndef PERSISTENCE_H
#define PERSISTENCE_H

#include <json.hpp>
#include <string>
#include <fstream>
#include <IPersistence.h>
#include <IDTO.h>

using namespace std;
using Json = nlohmann::json;

class Persistence: public IPersistence, public IDTO
{
private:
    Json m_jsonFile;            //File JSON - Archivo tipo Json
    string m_fileName;     //File Name - Nombre del archivo

public:
    //constructor que no recibe nada, pero tiene valores
    Persistence();
    //constructor que recibe un nombre de archivo
    Persistence(const string&);

    ~Persistence();

    void setJsonFile(const Json&);

    void setFileName(const string&);

    string getFileName();

    void save();

    void load();

    void save(const string&);

    void load(const string&);
};
#endif
