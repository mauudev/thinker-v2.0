#ifndef IDTO_H
#define IDTO_H

#include <json.hpp>

using namespace std;
using Json = nlohmann::json;

// Abstract Class IDTO (Transfer Data Object)
class IDTO
{
public:
    virtual Json serialize()=0;

    virtual void deserialize(Json&)=0;
};
#endif
