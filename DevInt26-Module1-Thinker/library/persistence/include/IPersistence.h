#ifndef IPERSISTENCE_H
#define IPERSISTENCE_H

#include <json.hpp>

using namespace std;
using Json = nlohmann::json;

// Interface Class IPersistence
class IPersistence
{
public:
    virtual string getFileName()=0;

    virtual void save()=0;

    virtual void load()=0;
};
#endif
