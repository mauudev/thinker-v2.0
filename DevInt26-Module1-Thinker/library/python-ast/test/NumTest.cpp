#include <limits.h>
#include "gtest/gtest.h"

class NumTest   : public ::testing::Test 
{
   

};

TEST_F(NumTest, IntNum)
{

    Number nInt(10);
    BuilderCode visitor = BuilderCode();
    visitor.visit(&nInt);
    string expected = "10";
    ASSERT_EQ(expected, visitor.getCode());
    
}

TEST_F(NumTest, DoubleNum)
{
    Number nDouble(45.67);
    BuilderCode visitor = BuilderCode();
    visitor.visit(&nDouble);
    string expected = "45.67";
    ASSERT_EQ(expected, visitor.getCode());
    
}

TEST_F(NumTest, FloatNum)
{
    Number nDouble(45.6785);
    BuilderCode visitor = BuilderCode();
    visitor.visit(&nDouble);
    string expected = "45.6785";
    ASSERT_EQ(expected, visitor.getCode());

}

TEST_F(NumTest, FloatNumWithZero)
{
    Number nDouble(45.0f);
    BuilderCode visitor = BuilderCode();
    visitor.visit(&nDouble);
    string expected = "45.0";
    ASSERT_EQ(expected, visitor.getCode());
      
}

