#include <limits.h>
#include "gtest/gtest.h"

#include <BuilderCode.h>
#include <PythonInjector.h>

class PythonInjectorTest   : public ::testing::Test {

};


TEST_F(PythonInjectorTest, insideWhile)
{

	Number c1 = Number(2);
	Name var1 = Name("variable");
	Compare bool1(&var1,Eq, &c1);
	While while1 = While(&bool1);
	PythonInjector pi(
R"foo(n = 100
h = ''
while n >= 20:
	h += ' %i' % n
	n -= 5
print h)foo");

	string expected = 
R"foo(while(variable == 2):
	n = 100
	h = ''
	while n >= 20:
		h += ' %i' % n
		n -= 5
	print h)foo";
	while1.addBody(&pi); 
	BuilderCode visitor = BuilderCode();
	visitor.visit(&while1);
	ASSERT_EQ(expected, visitor.getCode());
	
}

TEST_F(PythonInjectorTest, insideFor)
{
	Number c2 = Number(4);
    Name var = Name("variable");
    Name var2 = Name("list");
    Assignment assing = Assignment(&var, &c2);
    PythonInjector pi("lista = [(i, i + 2) for i in range(5)]");

    For for1 = For(&var, &var2);
    for1.addBody(&assing).addBody(&pi);
   
    BuilderCode visitor = BuilderCode();
    visitor.visit(&for1);
   
    string expected = 
R"foo(for variable in list :
	variable = 4
	lista = [(i, i + 2) for i in range(5)])foo";
    ASSERT_EQ(expected, visitor.getCode());
}

TEST_F(PythonInjectorTest, insideFunction)
{
	Number n1 = Number(32);
    Number n2 = Number(54);

    Name var = Name("test");

    BinaryOperator add = BinaryOperator(&n1, ADD, &n2);

    Assignment assign = Assignment(&var, &add);
    PythonInjector pi("suma = lambda x, y = 2: x + y");

    FunctionDef function = FunctionDef("method");
    function.addParameter(&var);
    function.addBodyNode(&assign).addBodyNode(&pi);

    BuilderCode visitor = BuilderCode();
    visitor.visit(&function);

    string expected = 
R"foo(def method(test):
	test = (32+54)
	suma = lambda x, y = 2: x + y)foo";

    ASSERT_EQ(expected, visitor.getCode());
}
