#include <limits.h>
#include "gtest/gtest.h"

class WithTest   : public ::testing::Test 
{
};

TEST_F(WithTest, withOneExpression)
{
	BuilderCodePy py;
	CallFunction& call = py.call(&py.name("open"))
							.addArgument(&py.name("\"note1.txt\""))
							.addArgument(&py.name("\"w\""));
	With& with = py.withStmt(&call,"w")
					.addBody( &py.name("f.write(\" I am here\")"));
	string expected = 
R"foo(with open("note1.txt", "w") as w:
	f.write(" I am here"))foo";
	ASSERT_EQ(expected, py.generateCode(&with));
}

TEST_F(WithTest, withTwoExpressions)
{
	
	Import import("sys");
	FunctionDef funOpen("open");
	Name aa("a"),bb("b");
	funOpen .addParameter(&aa).addParameter(&bb);
	CallFunction callOpen1 = funOpen.Call();
	CallFunction callOpen2 = funOpen.Call();
	Name o1("\"note1.txt\""), o2("\"note2.txt\""), w("\"w\"");
	callOpen1.addArgument(&o1).addArgument(&w);
	callOpen2.addArgument(&o2).addArgument(&w);

	Name write1("f1.write(\" I am here\")");
	Name write2("f2.write(\" I am here\")");

	With with(&callOpen1,"f1");
	with.addExpr(&callOpen2,"f2");
	with.addBody(&write1).addBody(&write2);

	BuilderCode visitor = BuilderCode();
	visitor.visit(&with);
	string expected = 
R"foo(with open("note1.txt", "w") as f1, open("note2.txt", "w") as f2:
	f1.write(" I am here")
	f2.write(" I am here"))foo"; 
	ASSERT_EQ(expected, visitor.getCode()); 
}

TEST_F(WithTest, addBody)
{
	
	FunctionDef funOpen("open");
	Name aa("a"),bb("b");
	funOpen .addParameter(&aa).addParameter(&bb);
	CallFunction callOpen1 = funOpen.Call();
	Name o1("\"note1.txt\""), w("\"w\"");
	callOpen1.addArgument(&o1).addArgument(&w);

	Name write1("f.write(\" I am here\")");

	Name cnt("cnt");
	Number zero(0);
	Assignment assigCnt(&cnt,&zero);

	With with(&callOpen1,"f");
	with.addBody(&write1).addBody(&assigCnt);

	BuilderCode visitor = BuilderCode();
	visitor.visit(&with);
	string expected = 
R"foo(with open("note1.txt", "w") as f:
	f.write(" I am here")
	cnt = 0)foo";
	ASSERT_EQ(expected, visitor.getCode());
}
