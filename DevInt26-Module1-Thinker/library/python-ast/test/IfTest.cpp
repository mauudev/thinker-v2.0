#include <limits.h>
#include "gtest/gtest.h"

#include <BuilderCode.h>

class IfTest : public ::testing::Test
{
};

TEST_F(IfTest, simpleIfWithBoolExpression)
{
    Number c1 = Number(2);
    Name var = Name("variable");
    Compare bool1(&var,Eq, &c1);
    If if1 = If(&bool1);
    BuilderCode visitor = BuilderCode();
    visitor.visit(&if1);
    string expected = "if variable == 2:";
    ASSERT_EQ(expected, visitor.getCode());
}

TEST_F(IfTest, simpleIfWithCallExpression)
{
    Number c1 = Number(2);
    Name var = Name("variable");
    FunctionDef function = FunctionDef("method");
    CallFunction call = function.Call();
    call.addArgument(&c1).addArgument(&var);
    If if1 = If(&call);
    BuilderCode visitor = BuilderCode();
    visitor.visit(&if1);
    string expected = "if method(2, variable):";
    ASSERT_EQ(expected, visitor.getCode());
}

TEST_F(IfTest, WhileIfCall)
{
    Number c1 = Number(2);
    Number c2 = Number(4);
    Name var1 = Name("variable");
    Name var2 = Name("a");
    Assignment assing = Assignment(&var2, &c2);
    Compare bool1(&var1,Eq, &c1);
    FunctionDef function = FunctionDef("method");
    CallFunction call = function.Call();
    call.addArgument(&c1).addArgument(&var1);
    If if1 = If(&bool1);
    if1.addIfNode(&assing).addIfNode(&call); 
    BuilderCode visitor = BuilderCode();
    visitor.visit(&if1);
    string expected = "if variable == 2:\n\ta = 4\n\tmethod(2, variable)";
    ASSERT_EQ(expected, visitor.getCode());
}

TEST_F(IfTest, IfWithWhile)
{
    Number c1 = Number(2);
    Number c2 = Number(4);
    Name var1 = Name("variable");
    Name var2 = Name("a");
    Assignment assing1 = Assignment(&var2, &c2);
    Assignment assing2 = Assignment(&var1, &c1);
    Compare bool1(&var1,Eq, &c1);
    BoolOp bool2 = BoolOp(&c1,OR, &c2);
    While while1 = While(&bool1);
    while1.addBody(&assing1).addBody(&assing2);
    If if1 = If(&bool2);
    if1.addIfNode(&assing1).addIfNode(&while1);
    BuilderCode visitor = BuilderCode();
    visitor.visit(&if1);
    string expected = "if (2 or 4):\n\ta = 4\n\twhile(variable == 2):\n\t\ta = 4\n\t\tvariable = 2";
    ASSERT_EQ(expected, visitor.getCode());
}

TEST_F(IfTest, IfWithAnotherIf)
{
	Number c1 = Number(2);
	Number c2 = Number(4);
	Name var1 = Name("variable");
	Name var2 = Name("a");
	Assignment assing1 = Assignment(&var2, &c2);
	Assignment assing2 = Assignment(&var1, &c1);
	Compare bool1(&var1,Eq, &c1);
	BoolOp bool2 = BoolOp(&c1,AND, &c2);
	If if1 = If(&bool1);
	if1.addIfNode(&assing1).addIfNode(&assing2);
	If if2 = If(&bool2);
	if2.addIfNode(&assing1).addIfNode(&if1);
	BuilderCode visitor = BuilderCode();
	visitor.visit(&if2);
	string expected = "if (2 and 4):\n\ta = 4\n\tif variable == 2:\n\t\ta = 4\n\t\tvariable = 2";
	ASSERT_EQ(expected, visitor.getCode());
}

TEST_F(IfTest, IfWithElse)
{
	Name score("score");
	Name letter("letter");
	Name letterA("letter = \'A\'");
	Name letterB("letter = \'B\'");
	Name letterC("letter = \'C\'");
	Name letterD("letter = \'D\'");
	Name letterE("letter = \'F\'");
	Number n90(90);
	Number n80(80);
	Number n70(70);
	Number n60(60);
	Compare compare90(&score, GTE, &n90);
	Compare compare80(&score, GTE, &n80);
	Compare compare70(&score, GTE, &n70);
	Compare compare60(&score, GTE, &n60);
	Return ret(&letter);

	If if60 = If(&compare60);
	if60.addIfNode(&letterD).addElifNode(&letterE).addElifNode(&ret);

	If if70 = If(&compare70);
	if70.addIfNode(&letterC).addElifNode(&if60);

	If if80 = If(&compare80);
	if80.addIfNode(&letterB).addElifNode(&if70);

	If if90 = If(&compare90);
	if90.addIfNode(&letterA).addElifNode(&if80);

	FunctionDef function("letterGrade");
	function.addParameter(&score);
	function.addBodyNode(&if90);

	BuilderCode visitor = BuilderCode();
	visitor.visit(&function);
	string expected =
R"foo(def letterGrade(score):
	if score >= 90:
		letter = 'A'
	elif score >= 80:
		letter = 'B'
	elif score >= 70:
		letter = 'C'
	elif score >= 60:
		letter = 'D'
	else:
		letter = 'F'
		return letter
)foo";

	ASSERT_EQ(expected, visitor.getCode());
}

TEST_F(IfTest, IfWithElseif)
{
	Name score("score");
	Name letter("letter");
	Name letterA("letter = \'A\'");
	Name letterB("letter = \'B\'");
	Name letterC("letter = \'C\'");
	Name letterD("letter = \'D\'");
	Name letterE("letter = \'F\'");
	Number n90(90);
	Number n80(80);
	Number n70(70);
	Number n60(60);
	Compare compare90(&score, GTE, &n90);
	Compare compare80(&score, GTE, &n80);
	Compare compare70(&score, GTE, &n70);
	Compare compare60(&score, GTE, &n60);

	If if60 = If(&compare60);
	if60.addIfNode(&letterD).addElifNode(&letterE);

	If if70 = If(&compare70);
	if70.addIfNode(&letterC);

	If if80 = If(&compare80);
	if80.addIfNode(&letterB);

	If if90 = If(&compare90);
	if90.addIfNode(&letterA).addElifNode(&if80).addElifNode(&if70).addElifNode(&if60);

	Return ret(&letter);
	FunctionDef function("letterGrade");
	function.addParameter(&score);
	function.addBodyNode(&if90).addBodyNode(&ret);

	BuilderCode visitor = BuilderCode();
	visitor.visit(&function);

	string expected =
R"foo(def letterGrade(score):
	if score >= 90:
		letter = 'A'
	elif score >= 80:
		letter = 'B'
	elif score >= 70:
		letter = 'C'
	elif score >= 60:
		letter = 'D'
	else:
		letter = 'F'
	return letter
)foo";

	ASSERT_EQ(expected, visitor.getCode());

}
