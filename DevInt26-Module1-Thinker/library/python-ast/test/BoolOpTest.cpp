#include <limits.h>
#include "gtest/gtest.h"

#include <BuilderCode.h>

#include <BoolOp.h>

class BoolOpTest : public ::testing::Test {
   
};
TEST_F(BoolOpTest, simpleBoolOpNamesAnd)
{
    string expected = "(b and c)";
    BuilderCodePy py;
    BoolOp& opAnd=py.boolOp(&py.name("b"),AND,&py.name("c"));
    ASSERT_EQ(expected, py.generateCode(&opAnd));
    
}

TEST_F(BoolOpTest, simpleBoolOpBetweenNamesAndWithOtherBoolOperator){

    BuilderCodePy py;
    BoolOp& op=py.boolOp(&py.name("c"),AND,&py.boolOp(&py.name("a"),OR,&py.name("b")));
    string expected = "(c and (a or b))";

    ASSERT_EQ(expected, py.generateCode(&op));
    
}


TEST_F(BoolOpTest, simpleBoolOpEquals){

    Name nameA = Name("a");
    Name nameB = Name("b");

    Compare equal(&nameA,Eq, &nameB);

    string expected = "a == b";

    BuilderCode visitor = BuilderCode();
    visitor.visit(&equal);

    ASSERT_EQ(expected, visitor.getCode());
    
}
