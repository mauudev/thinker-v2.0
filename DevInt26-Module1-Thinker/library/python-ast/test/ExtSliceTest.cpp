#include <limits.h>
#include "gtest/gtest.h"

#include <iostream>
#include <Visitor.h>
#include <BuilderCode.h>

using namespace std;

class ExtSliceTest : public ::testing::Test 
{
   

};

TEST_F(ExtSliceTest, SimpleSliceType)
{
    Name nameA("a");
    Index index = Index(&nameA); 
    ExtSlice slice = ExtSlice(&index); 
    BuilderCode visitor = BuilderCode();
    visitor.visit(&slice);
    string expected = "a";
    ASSERT_EQ(expected, visitor.getCode());
    
}

TEST_F(ExtSliceTest, MultipleSliceTypes)
{
    Number c1(2), c2(4);
    Name var1("a"), var2("x"), var3("y");
    Slice slice1(&var1, &c1), slice2(&var2, &var3);
    Index index = Index(&c2);
    ExtSlice slice = ExtSlice(&slice1);
    slice.AddSlice(&index).AddSlice(&slice2);
    BuilderCode visitor = BuilderCode();
    visitor.visit(&slice);
    string expected = "a : 2, 4, x : y";
    ASSERT_EQ(expected, visitor.getCode());
    
}
