#include <limits.h>
#include "gtest/gtest.h"

class CompareTest : public ::testing::Test 
{
   

};

TEST_F(CompareTest, SimpleComparisonEq)
{
    BuilderCodePy py;
    Compare& cmp=py.compare(&py.call(&py.name("len")).addArgument(&py.name("pilaO")),
                            NotEq,
                            &py.num(0));
    string expected = "len(pilaO) != 0";
    ASSERT_EQ(expected, py.generateCode(&cmp));
    
}

TEST_F(CompareTest, SimpleComparisonGt)
{
    BuilderCodePy py;
    Compare cmp=py.compare(&py.name("fullReward"),Gt,&py.name("expectedReward"));   
    string expected = "fullReward > expectedReward";
    ASSERT_EQ(expected, py.generateCode(&cmp));    
}

TEST_F(CompareTest, SimpleComparisonIsNot)
{
    BuilderCodePy py;
    Compare cmp=py.compare(&py.name("obs"),IsNot,&py.name("None"));  
    string expected="obs is not None";
    ASSERT_EQ(expected, py.generateCode(&cmp));
}

TEST_F(CompareTest, SimpleComparisonEqLikeIFExpression)
{
    Name cont("cont");
    Number five(5);
    Compare compare(&cont,Eq,&five);
    If ifCond(&compare);
    Break brk = Break();
    ifCond.addIfNode(&brk);
    BuilderCode visitor;
    visitor.visit(&ifCond);
    string expected="if cont == 5:\n\tbreak";
    ASSERT_EQ(expected, visitor.getCode());
}