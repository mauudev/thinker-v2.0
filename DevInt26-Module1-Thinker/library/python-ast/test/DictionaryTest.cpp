#include <limits.h>
#include "gtest/gtest.h"

class DictionaryTest : public ::testing::Test 
{
   

};

TEST_F(DictionaryTest, SimpleDictionary)
{
    BuilderCodePy py;
    Dictionary& dict = py.dict(&py.str("variable"), &py.num(2));
    string expected = "{\n\t\"variable\" : 2 }";
    ASSERT_EQ(expected, py.generateCode(&dict));
    
}

TEST_F(DictionaryTest, MultipleKeysDictionary)
{
    BuilderCodePy py;
    Dictionary& dict = py.dict(&py.str("variable"), &py.num(2));
    dict.AddKey(&py.str("prueba"), &py.name("a")).AddKey(&py.str("prueba2"), &py.num(4));
    string expected = "{\n\t\"variable\" : 2,\n\t\"prueba\" : a,\n\t\"prueba2\" : 4 }";
    ASSERT_EQ(expected, py.generateCode(&dict));

}
