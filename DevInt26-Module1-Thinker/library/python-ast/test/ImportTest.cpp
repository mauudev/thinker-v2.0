#include <limits.h>
#include "gtest/gtest.h"

class ImportTest   : public ::testing::Test 
{
   

};

TEST_F(ImportTest, simpleImportOneModule)
{
    BuilderCodePy py;
    Import& import = py.import("sys");    
    string expected = "import sys";    
    ASSERT_EQ(expected, py.generateCode(&import));
}

TEST_F(ImportTest, simpleImportManyModules)
{
    BuilderCodePy py;
    Import& import = py.import("sys")
                        .addImport("imp")
                        .addImport("_builtin_")  
                        .addImport("module1")
                        .addImport("module2");
    string expected = "import sys, imp, _builtin_, module1, module2";    
    ASSERT_EQ(expected, py.generateCode(&import));   
}

TEST_F(ImportTest, simpleImportModuleAlias)
{
    BuilderCodePy py;
    Import& import = py.import("sys", "alias");
    string expected = "import sys as alias";    
    ASSERT_EQ(expected, py.generateCode(&import));

}

TEST_F(ImportTest, simpleImportMixed)
{
    BuilderCodePy py;
    Import& import = py.import("sys")
                        .addImport("_builtin_", "built")  
                        .addImport("module1", "md1")
                        .addImport("module2");
    string expected = "import sys, _builtin_ as built, module1 as md1, module2";    
    ASSERT_EQ(expected,py.generateCode(&import));
    
}

TEST_F(ImportTest, importFromOneModule)
{
    BuilderCodePy py;
    ImportFrom& import = py.importFrom("sounds"); 
    string expected = "from sounds import *\n";    
    ASSERT_EQ(expected, py.generateCode(&import));
}

TEST_F(ImportTest, importFromExtenssionAsAlias)
{

    ImportFrom import("sounds", "effect", "eff");
    BuilderCode visitor = BuilderCode();
    visitor.visit(&import);
    string expected = "from sounds import effect as eff\n";    
    ASSERT_EQ(expected, visitor.getCode());
    
}

TEST_F(ImportTest, importFromMixed)
{
    BuilderCodePy py;
    ImportFrom& import = py.importFrom("sounds","melody")
                            .addImportFrom("sounds", "melody", "mdy")
                            .addImportFrom("pkgutil", "extend_path")
                            .addImportFrom("sounds.effects")
                            .addImportFrom("sounds")
                            .addImportFrom("graphics"); 
    string expected = "from sounds import melody\n"
                        "from sounds import melody as mdy\n"
                        "from pkgutil import extend_path\n"
                        "from sounds.effects import *\n"
                        "from sounds import *\n"
                        "from graphics import *\n"; 
    ASSERT_EQ(expected, py.generateCode(&import));
}


