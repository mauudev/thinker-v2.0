#include <limits.h>
#include "gtest/gtest.h"

class ForTest   : public ::testing::Test 
{
   

};

TEST_F(ForTest, simpleForSignDefinition)
{
    BuilderCodePy py;
    For& f=py.forStmt(&py.name("variable"),&py.name("variable2"));
    string expected = "for variable in variable2 :";
    ASSERT_EQ(expected, py.generateCode(&f));
}


TEST_F(ForTest, simpleForWithBody)
{
    BuilderCodePy py;
    For& f=py.forStmt(&py.name("variable"),&py.name("variable2"))
                .addBody(&py.assign(&py.name("variable2"),&py.num(4)));
    string expected = "for variable in variable2 :\n\tvariable2 = 4";
    ASSERT_EQ(expected, py.generateCode(&f));
}

TEST_F(ForTest, complexForWithBodyFor)
{
    Number c2 = Number(4);
    Name var = Name("variable");
    Name var2 = Name("variable2");
    Assignment assing = Assignment(&var2, &c2);
    For for2 = For(&var, &var2);
    For for1 = For(&var, &var2);
    for1.addBody(&assing);
    for1.addBody(&for2);
    BuilderCode visitor = BuilderCode();
    visitor.visit(&for1);
    string expected = "for variable in variable2 :\n\tvariable2 = 4\n\tfor variable in variable2 :";
    ASSERT_EQ(expected, visitor.getCode());
}