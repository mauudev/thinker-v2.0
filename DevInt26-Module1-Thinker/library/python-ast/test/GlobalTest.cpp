#include <limits.h>
#include "gtest/gtest.h"

#include <BuilderCode.h>
#include <PythonInjector.h>
#include <Module.h>
#include <FunctionDef.h>

class GlobalTest   : public ::testing::Test {

};

TEST_F(GlobalTest, oneGlobal)
{  
	BuilderCodePy py;
    FunctionDef& func = py.functionDef("configNetwork")
				.addParameter(&py.name("inputX"))
				.addParameter(&py.name("numberKeysY"))
				.addParameter(&py.name("nameGame"))
				.addBodyNode(&py.global(&py.name("_inputX")))
				.addBodyNode(&py.assign(&py.name("_inputX"), &py.name("inputX")))
				.addBodyNode(&py.global(&py.name("_numberKeysY")))
				.addBodyNode(&py.assign(&py.name("_numberKeysY"), &py.name("numberKeysY")))
				.addBodyNode(&py.global(&py.name("_nameGame")))
				.addBodyNode(&py.assign(&py.name("_nameGame"), &py.name("nameGame")));

	string expected = 
R"foo(def configNetwork(inputX, numberKeysY, nameGame):
	global _inputX
	_inputX = inputX
	global _numberKeysY
	_numberKeysY = numberKeysY
	global _nameGame
	_nameGame = nameGame)foo";

	ASSERT_EQ(expected, py.generateCode(&func));
}


TEST_F(GlobalTest, manyGlobals)
{
	BuilderCodePy py;
    FunctionDef& func = py.functionDef("train")
				.addParameter(&py.name("numberCiclos"))
				.addParameter(&py.name("numGames"))
				.addParameter(&py.name("sizeNumberKeysY"))
				.addParameter(&py.name("observations"))
				.addBodyNode(&py.global(&py.name("sess"))
								.addGlobal(&py.name("production"))
								.addGlobal(&py.name("optimizer"))
								.addGlobal(&py.name("env"))
								.addGlobal(&py.name("xInputObservations")))
				.addBodyNode(&py.global(&py.name("yExpectedActions"))
								.addGlobal(&py.name("saver")));
	string expected = 
R"foo(def train(numberCiclos, numGames, sizeNumberKeysY, observations):
	global sess ,production ,optimizer ,env ,xInputObservations
	global yExpectedActions ,saver)foo";
	
	ASSERT_EQ(expected, py.generateCode(&func));

}