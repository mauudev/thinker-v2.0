#ifndef FOR_H_
#define FOR_H_

#include <NodeAST.h>
#include <Expression.h>
#include <Statement.h>
#include <Visitor.h>
#include <string>

class For: public  Statement
{

private:

	Expression* _expression;
	Expression* _expressionIt;
	string _reservedWord;
	vector<NodeAST*> bodyElse;

public:

	For();
	
	For(Expression* expression, Expression* expressionIt);

	For& addBody(Statement* statement);

	For& addBody(Expression* expressions);

	For& addBodyElse(Statement* statement);

	For& addBodyElse(Expression* expressions);

	Expression* getExprAssignment();

	Expression* getExprIterable();

	bool hasBodyElse();

	vector<NodeAST*> getBodyElse();

	string toString() override;

	string getReservedWord();
	
  	void accept(Visitor &visitor) override;
};

#endif