#ifndef CALLFUNCTION_H_
#define CALLFUNCTION_H_ 

#include <ParameterList.h>
#include <FunctionDef.h>
#include <ClassDef.h>
#include <Expression.h>
#include <KeyWord.h>

class CallFunction : public Expression
{
	friend class FunctionDef;
	friend class ClassDef;
	friend class Concat;
	ParameterList _argumentsList;
	ParameterList _keyWordsList;
	FunctionDef* _function;
	ClassDef* _class;
	Concat* _concat;
	Expression* _func;

	CallFunction(FunctionDef& function);	

	CallFunction(ClassDef& classdef);

	CallFunction(Concat& concat);

	CallFunction(FunctionDef& function, Expression* func);

	CallFunction(ClassDef& clas,Expression* cl);
	
	CallFunction(Concat& concat,Expression* cl);
public:		

	CallFunction();

	CallFunction(Expression* func);

	string toString() override;

	void accept(Visitor& visitor) override;
	
	CallFunction& addArgument(Expression* argument);

	CallFunction& AddKeyWord(KeyWord* key);

	ParameterList GetArguments();

	ParameterList GetKeyWords();

	Expression* GetFunc();

	FunctionDef* GetFunction();
};

#endif