#ifndef KEYWORD_H_
#define KEYWORD_H_ 

#include <Visitor.h>
#include <Expression.h>
#include <NodeAST.h>

class KeyWord: public NodeAST
{
	string _arg;
	Expression* _value;
public:
	KeyWord();

	KeyWord(const string& arg, Expression* value);

	Expression* GetValue();

	string toString() override;

	void accept(Visitor& visitor) override;
};

#endif