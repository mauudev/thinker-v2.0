#ifndef MODULE_H_
#define MODULE_H_ 

#include <ModType.h>
#include <Expression.h>
#include <Statement.h>
#include <Visitor.h>

class Module: public ModType
{
private:

	vector<NodeAST*> _body;

public:

	Module();
	
	Module(Expression* value);	

	Module(Statement* value);	

	Module& addBody(Expression* value);

	Module& addBody(Statement* value);

	vector<NodeAST*> getBody();

	string toString() override;

	void accept(Visitor& visitor) override;

};

#endif