#ifndef UNARYOP_H_
#define UNARYOP_H_

#include <Expression.h>
#include <Visitor.h>
#include <string>

using namespace std;

enum UnaryOpType { Not, Invert, UAdd, USub};

class UnaryOp : public Expression {

private:

    string _unary;
    Expression* _operand;

public:

	UnaryOp();

  	UnaryOp(UnaryOpType ut, Expression* operand);  

    string getUnaryOp();

    Expression* getOperand();

    string toString() override;

    void accept(Visitor &visitor) override;

private:

  	void setUnaryOp(UnaryOpType u);

};

#endif