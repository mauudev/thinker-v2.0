#ifndef _RETURN_H_
#define _RETURN_H_
#include <NodeAST.h>
#include <Statement.h>
#include <Expression.h>
#include <string>
#include <Visitor.h>

using namespace std;

class Return : public Statement
{

    const static string _keyWord;
    Expression* _value;

public:
	Return();

	Return(Expression* value);
	
	string getPalabraReservada();	

	Expression* getValue();

	string toString() override;

	void accept(Visitor& visitor) override;
};

#endif