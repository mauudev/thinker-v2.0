#ifndef WHILE_H_
#define WHILE_H_

#include <NodeAST.h>
#include <Expression.h>
#include <Statement.h>
#include <Visitor.h>
#include <string>

class While: public  Statement
{

private:

	Expression* _boolExpression;
	vector<NodeAST*> bodyElse;

public:

	While();

	While(Expression* expression);

	While& addBody(Statement* statement);

	While& addBody(Expression* expression);

	While& addBodyElse(Statement* statement);

	While& addBodyElse(Expression* expressions);

	Expression* getExprBool();

	bool hasBodyElse();

	vector<NodeAST*> getBodyElse();

	string toString() override;
	
  	void accept(Visitor &visitor) override;
};

#endif