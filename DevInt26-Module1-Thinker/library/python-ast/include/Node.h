#ifndef NODE_H_
#define NODE_H_

#include <string>

using namespace std;
class Visitor;

class Node{
public:
  virtual string toString() = 0;
  
  virtual void accept(Visitor &visitor) = 0;
};

#endif