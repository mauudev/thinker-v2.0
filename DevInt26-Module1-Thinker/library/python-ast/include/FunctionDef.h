#ifndef FUNCTIONDEFINITION_H_
#define FUNCTIONDEFINITION_H_

#include <NodeAST.h>
#include <Visitor.h>
#include <Return.h>
#include <string>
#include <CallFunction.h>
#include <Expression.h>
#include <Statement.h>


class FunctionDef : public Statement {
protected:
  friend class CallFunction;
  vector<NodeAST*> _body;
  string _nameFunction;
  ParameterList _paramList;
  bool _isEmpty;

public:

  FunctionDef();

  FunctionDef(string funcName);  

  FunctionDef& addParameter(Expression* parameter);

  FunctionDef& addBodyNode(Statement* statementNode);

  FunctionDef& addBodyNode(Expression* expressionNode);

  string NoBody(); 

  string toString() override;

  void accept(Visitor& visitor) override;

  int SizeBodyList();

  vector<NodeAST*> GetBodyList();

  void addParameters(ParameterList paramList);

  CallFunction Call();

  CallFunction Call(Expression* func);
  
  string getName();

  ParameterList& getParameters();
  
};

#endif