#ifndef ATTRIBUTE_H_
#define ATTRIBUTE_H_ 

#include <Expression.h>
#include <Name.h>

class Attribute: public Expression
{
	Expression* _value;
	string _attr;
public:
	Attribute();	

	Attribute(Expression* value,string attr);

	Expression* GetValue();

	string toString() override;

	void accept(Visitor& visitor) override;
};

#endif