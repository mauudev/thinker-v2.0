#ifndef IMPORTFROM_H_
#define IMPORTFROM_H_

#include <NodeAST.h>
#include <Expression.h>
#include <Statement.h>
#include <utility> 
#include <Visitor.h>
#include <string>

using namespace std;

struct Imp 
{
	string relativeModule;
	string identifier;
	string name;
};

class ImportFrom: public  Statement
{

private:

	vector<Imp> _listImp;
	vector<string> _listImpAll;
	vector<pair<string, string>> _importList;
	string _moduleRelative;

public:

	ImportFrom();

	ImportFrom(string module);

	ImportFrom(string relativeModule, string identifier);

	ImportFrom(string relativeModule, string identifier, string alias);

	ImportFrom& addImportFrom(string module);

	ImportFrom& addImportFrom(string relativeModule, string identifier);

	ImportFrom& addImportFrom(string relativeModule, string identifier, string alias);

	vector<Imp> getImportList();

	vector<string> getImportListAll();

	string getModuleRelative();

	string toString() override;
	
  	void accept(Visitor &visitor) override;

};

#endif