#ifndef PARAMETERLIST_H_
#define PARAMETERLIST_H_ 

#include <NodeAST.h>
#include <Visitor.h>

using namespace std;

class ParameterList: public NodeAST
{

public:
	ParameterList();

	void addParameter(NodeAST* parameterNode);	

	string toString() override;

	void accept(Visitor &visitor) override;
	
	int getSize();
};

#endif