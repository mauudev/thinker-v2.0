#ifndef PYTHONINJECTOR_H_
#define PYTHONINJECTOR_H_

#include <Expression.h>
#include <Visitor.h>
#include <string>

using namespace std;


class PythonInjector : public Expression 
{

private:

	vector<string> lines;
	string _pythonCode;
	int _numberLines;

public:

	PythonInjector(string text);

	int getNumberLines();

	vector<string> getLines();

	string getCode();

	string toString() override;
	
  	void accept(Visitor &visitor) override;

private:

	void splitLines();

};

#endif