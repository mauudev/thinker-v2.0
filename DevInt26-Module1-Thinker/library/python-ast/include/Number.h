#ifndef NUMBER_H_
#define NUMBER_H_

#include <Expression.h>
#include <Visitor.h>
#include <string>
#include <sstream>
#include <iostream>
#include <iomanip>

using namespace std;

class Number : public Expression {
  
  string number;

public:

  Number();

  Number(int val);

  Number(float val);

  Number(double val);

  string toString() override;
  
  void accept(Visitor &visitor) override;
};

#endif