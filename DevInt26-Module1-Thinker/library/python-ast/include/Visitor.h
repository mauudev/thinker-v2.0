#ifndef VISITOR_H_
#define VISITOR_H_

#include <iostream>
#include <string>
#include <cstring>
#include <sstream>
#include <vector>

class BinaryOperator;
class Number;
class Assignment;
class BoolOp;
class FunctionDef;
class ClassDef;
class Concat;
class ParameterList;
class CallFunction;
class While;
class For;
class If;
class Print;
class StringExp;
class Import;
class ImportFrom;
class Return;
class With;
class Dictionary;
class List;
class Name;
class InsertFun;
class Attribute;
class PythonInjector;
class KeyWord;
class Slice;
class Index;
class ExtSlice;
class UnaryOp;
class Subscript;
class Compare;
class Break;
class Module;
class Global;
class Tuple;
class AugAssign;

class Visitor{

public:

  virtual void visit(BinaryOperator*) = 0;

  virtual void visit(Number*) = 0;

  virtual void visit(Assignment*) = 0;

  virtual void visit(BoolOp*) = 0;

  virtual void visit(FunctionDef*) = 0;

  virtual void visit(ClassDef*) = 0;

  virtual void visit(Concat*) = 0;

  virtual void visit(ParameterList*) = 0;

  virtual void visit(CallFunction*) = 0;

  virtual void visit(While*) = 0;
  
  virtual void visit(If*) = 0;

  virtual void visit(Print*) = 0;

  virtual void visit(StringExp*) = 0;

  virtual void visit(For*) = 0;

  virtual void visit(Import*) = 0;

  virtual void visit(ImportFrom*) = 0;

  virtual void visit(With*) = 0;  

  virtual void visit(Return*) = 0;

  virtual void visit(Dictionary*) = 0;  

  virtual void visit(List*) = 0;

  virtual void visit(Name*) = 0;

  virtual void visit(InsertFun*) = 0;

  virtual void visit(Attribute*) = 0;

  virtual void visit(PythonInjector*) = 0;

  virtual void visit(UnaryOp*) = 0;

  virtual void visit(KeyWord*) = 0;

  virtual void visit(Slice*) = 0;

  virtual void visit(Index*) = 0;

  virtual void visit(ExtSlice*) = 0;

  virtual void visit(Subscript*) = 0;

  virtual void visit(Compare*) = 0;

  virtual void visit(Break*) = 0;

  virtual void visit(Module*) = 0;

  virtual void visit(Global*) = 0;

  virtual void visit(Tuple*) = 0;

  virtual void visit(AugAssign*) = 0;

};

#endif