#ifndef BOOLOPERATOR_H_
#define BOOLOPERATOR_H_

#include <NodeAST.h>
#include <Expression.h>
#include <Visitor.h>
#include <string>

using namespace std;
//class Visitor;
#define AND Bool::And
#define OR Bool::Or

enum Bool{ And, Or };
static string BoolopExp[2]{"and","or"};

class BoolOp : public Expression {

protected:

  Bool _op;
  Expression* _valueLeft;
  Expression* _valueRight;

public:
  BoolOp();
  BoolOp( Expression* left, Bool op, Expression* right);

  // Bool getOp(Bool op);

  Expression* getValueLeft();

  string getBool();

  Expression* getValueRight();

  string toString() override;
  
  void accept(Visitor &visitor) override;

private:

  void setBool(Bool op);
};

#endif

