#ifndef SLICE_H_
#define SLICE_H_

#include <SliceType.h>
#include <Expression.h>
#include <Visitor.h>
#include <string>

using namespace std;

class Slice : public SliceType 
{

  Expression* _lower;
  Expression* _upper;
  Expression* _step;

public:

  Slice();

  Slice(Expression* lower, Expression* upper);

  Slice& SetLower(Expression* lower);

  Slice& SetUpper(Expression* upper);

  Slice& SetStep(Expression* step);

  Expression* GetLower();

  Expression* GetUpper();

  Expression* GetStep();

  string toString() override;
  
  void accept(Visitor &visitor) override;
};

#endif