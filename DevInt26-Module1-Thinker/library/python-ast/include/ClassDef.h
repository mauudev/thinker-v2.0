#ifndef CLASS_DEF_H
#define CLASS_DEF_H

#include <NodeAST.h>
#include <Visitor.h>
#include <Return.h>
#include <string>
#include <CallFunction.h>
#include <Expression.h>
#include <Statement.h>


class ClassDef : public Statement {
protected:
  friend class CallFunction;
  vector<NodeAST*> _body;
  string _nameFunction;
  ParameterList _paramList;
  bool _isEmpty;

public:

  ClassDef();

  ClassDef(string funcName);  

  ClassDef& addParameter(Expression* parameter);

  ClassDef& addBodyNode(Statement* statementNode);

  ClassDef& addBodyNode(Expression* expressionNode);

  string NoBody(); 

  string toString() override;

  void accept(Visitor& visitor) override;

  int SizeBodyList();

  vector<NodeAST*> GetBodyList();

  void addParameters(ParameterList paramList);

  CallFunction Call();

  CallFunction Call(Expression* func);
  
  string getName();

  ParameterList& getParameters();
  
};

#endif