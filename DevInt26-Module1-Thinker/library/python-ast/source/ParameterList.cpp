#include <ParameterList.h>

ParameterList:: ParameterList()
{

}

void ParameterList:: addParameter(NodeAST* node)
{
	addChild(node);
}

string ParameterList::toString()
{
	string parameterString = "";
	for (int indexPar = 0; indexPar < list.size(); ++indexPar)
	{
		if(indexPar>0) parameterString+=", ";
		parameterString += list[indexPar]->toString();	
	}
	return parameterString;
}

void ParameterList::accept(Visitor &visitor) 
{
	visitor.visit(this);
}

int ParameterList:: getSize() 
{
	return 0;
}

