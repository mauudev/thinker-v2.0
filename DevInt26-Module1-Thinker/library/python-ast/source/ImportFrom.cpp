#include <ImportFrom.h>

ImportFrom::ImportFrom()
{

}

ImportFrom::ImportFrom(string module)
{
	_listImpAll.push_back(module);
}

ImportFrom::ImportFrom(string relativeModule, string identifier)
{
	_listImp.push_back(Imp{relativeModule,identifier,""});
}

ImportFrom::ImportFrom(string relativeModule, string identifier, string alias)
{
	_listImp.push_back(Imp{relativeModule,identifier,alias});
}

ImportFrom& ImportFrom::addImportFrom(string relativeModule, string identifier, string alias)
{
	_listImp.push_back(Imp{relativeModule,identifier,alias});
	return *this;
}

ImportFrom& ImportFrom::addImportFrom(string relativeModule, string identifier)
{
	_listImp.push_back(Imp{relativeModule,identifier,""});
	return *this;
}

ImportFrom& ImportFrom::addImportFrom(string module)
{
	_listImpAll.push_back(module);
	return *this;
}

vector<Imp> ImportFrom::getImportList()
{
	return _listImp;
}

vector<string> ImportFrom::getImportListAll()
{
	return _listImpAll;
}

string ImportFrom::toString()
{
	return "ImportFrom";
}
	
void ImportFrom::accept(Visitor &visitor)
{
	visitor.visit(this);
}
