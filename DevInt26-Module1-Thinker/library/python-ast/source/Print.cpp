#include <Print.h>

Print::Print()
{

}

Print::Print(Expression* leftExpression, Expression* rightExpression)
{
	_chevron = " >>";
	addChild(leftExpression);
	addChild(rightExpression);
}

Print& Print::AddExpression(Expression* expression)
{
	addChild(expression);
	return *this;
}

Print& Print::AddParameter(Expression* parameter)
{
	_parameterList.addParameter(parameter);
	return *this;
}

string Print::GetChevron()
{
	return _chevron;
}

bool Print::GetNewLine()
{
	return _newLine;
}

void Print::GetParameters(Visitor &visitor)
{
	_parameterList.accept(visitor);
}

int Print::GetParameterSize()
{
	return _parameterList.sizeChildList();
}

void Print::NoNewLine()
{
	_newLine = false;
}

string Print::toString()
{

}

void Print::accept(Visitor &visitor)
{
	visitor.visit(this);
}