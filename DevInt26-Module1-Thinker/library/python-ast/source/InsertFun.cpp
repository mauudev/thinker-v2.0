#include <InsertFun.h>

using namespace std;

InsertFun::InsertFun()
{
	_pythonCode = "";
}

InsertFun::InsertFun(string text)
{
	_pythonCode = text;
 	splitLines();
}

void InsertFun::splitLines()
{
  int size = _pythonCode.size();
  string cont="";
  for(int i=0; i<size;i++)
  {
    cont +=  _pythonCode[i];
    if(_pythonCode[i]=='\n' or (i==(size-1))) 
    {
      string ns = cont;
      lines.push_back(ns);
      cont="";
    }
  }
}

int InsertFun::getNumberLines()
{
	return _numberLines;
}

vector<string> InsertFun::getLines()
{
	return lines;
}

string InsertFun::getCode()
{
  return _pythonCode;
}

string InsertFun::toString()
{
  return "InsertFun";
}
  
void InsertFun::accept(Visitor &visitor)
{
  visitor.visit(this);
}
