#include <While.h>

While::While()
{
	
}

While:: While(Expression* expression)
{
	_boolExpression = expression;
}

While& While::addBody(Statement* statement)
{
	addChild(statement);
	return *this;
}

While& While::addBody(Expression* expression)
{
	addChild(expression);
	return *this;
}

While& While::addBodyElse(Statement* statement)
{
	bodyElse.push_back(statement);
	return *this;
}

While& While::addBodyElse(Expression* expression)
{
	bodyElse.push_back(expression);
	return *this;
}

Expression* While::getExprBool()
{
	return _boolExpression;
}

bool While::hasBodyElse()
{
	return (bodyElse.size()>0)? true : false;
}

vector<NodeAST*> While::getBodyElse()
{
	return bodyElse;
}

string While::toString() 
{
    return "while";
}

void While::accept(Visitor &visitor) 
{
    visitor.visit(this);
}


