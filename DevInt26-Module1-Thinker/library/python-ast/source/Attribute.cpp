#include <Attribute.h>

Attribute::Attribute()
{

}

Attribute::Attribute(Expression* value,string attr):
_value(value),_attr(attr){}

Expression* Attribute::GetValue()
{
	return _value;
}

string Attribute::toString()
{
	return _attr;
}

void Attribute::accept(Visitor& visitor)
{
	visitor.visit(this);
}
