#include <UnaryOp.h>
#include <stdexcept>

UnaryOp::UnaryOp(){}

UnaryOp::UnaryOp(UnaryOpType u, Expression* operand)
{

	_operand = operand;
	setUnaryOp(u);
}

void UnaryOp::setUnaryOp(UnaryOpType u)
{
	switch(u)
	{
    	case UnaryOpType::Not  	: 
    		_unary = "not";   
    		break;
    	case UnaryOpType::Invert: 
    		_unary = "~"; 
    		break;
    	case UnaryOpType::UAdd 	:  
    		_unary = "+"; 
    		break;
    	case UnaryOpType::USub 	: 
    		_unary = "-";   
    		break;
	}
}

string UnaryOp::getUnaryOp()
{
	return _unary;
}

Expression* UnaryOp::getOperand()
{
	return _operand;
}

string UnaryOp::toString()
{
	return "UnaryOp -->"+_unary;
}

void UnaryOp::accept(Visitor &visitor)
{
	visitor.visit(this);
}
