PROJECT(canvasparty)

CMAKE_MINIMUM_REQUIRED(VERSION 3.0.0)

ADD_COMPILE_OPTIONS(-std=c++11)

FIND_PACKAGE(X11 REQUIRED)
FIND_PACKAGE(PkgConfig REQUIRED)

PKG_CHECK_MODULES(GTKMM REQUIRED gtkmm-3.0)
PKG_CHECK_MODULES(GLIB REQUIRED glib-2.0)

INCLUDE_DIRECTORIES(${X11_INCLUDE_DIR})
INCLUDE_DIRECTORIES(${GTKMM_INCLUDE_DIRS})
INCLUDE_DIRECTORIES(${GLIB_INCLUDE_DIRS})

LINK_DIRECTORIES(${GTKMM_LIBRARY_DIRS})
LINK_DIRECTORIES(${GLIB_LDFLAGS})

ADD_DEFINITIONS(${GTKMM_CFLAGS_OTHER})

INCLUDE_DIRECTORIES(include)
INCLUDE_DIRECTORIES(include/events)
INCLUDE_DIRECTORIES(include/configure)
INCLUDE_DIRECTORIES(include/data/DataCollection)
INCLUDE_DIRECTORIES(include/data/ToolData)
INCLUDE_DIRECTORIES(include/events)
INCLUDE_DIRECTORIES(include/view)
INCLUDE_DIRECTORIES(include/component)
INCLUDE_DIRECTORIES(include/service)

SET(UIcanvas
    source/events/DragAndDropEvent.cpp
    source/view/CanvasToggleToolBox.cpp
    source/view/CanvasButtonToolBox.cpp
    source/component/GenericInput.cpp
    source/component/IntegerLabel.cpp
    source/component/TextArea.cpp
    source/component/InfoLabel.cpp
    source/component/TextLabel.cpp
    source/component/ToolBox.cpp
    source/component/ActionToolButton.cpp
    source/component/ActionToolToggle.cpp
    source/component/Linkbutton.cpp
    source/component/Togglebutton.cpp
    source/component/Menubar.cpp
    source/component/Infobar.cpp
    source/component/Canvas.cpp
    source/component/Tool.cpp
    source/component/RadioButton.cpp
    source/component/CheckButton.cpp
    source/component/RadioBox.cpp
    source/component/CheckBox.cpp
    source/component/ToolDefinition.cpp
    source/component/Point.cpp
    source/component/PointDouble.cpp
    source/component/Linked.cpp
    source/component/ToolDefinitionGroup.cpp
    source/data/DataCollection/DataCollection.cpp
    source/data/ToolData/ToolData.cpp
    source/events/ToolDeleteEvent.cpp
    source/events/ToolCopyEvent.cpp
    source/view/NewForm.cpp
    source/view/Buttonbox.cpp
    source/view/WindowUI.cpp
    source/view/MainWindowLayout.cpp
    source/Main.cpp
)

ADD_EXECUTABLE(UIcanvas ${UIcanvas})

TARGET_LINK_LIBRARIES(UIcanvas ${GTKMM_LIBRARIES})
