#include "CanvasToggleToolBox.h"


CanvasToggleToolBox::CanvasToggleToolBox()
:m_layout(Gtk::ORIENTATION_VERTICAL, 2),
m_actionToolSelectArea{"../resources/add_row.svg"},
m_actionToolLink{"../resources/serial_tasks.svg"}
{
    eventsToggle();
    m_layout.pack_start(m_actionToolSelectArea,Gtk::PACK_SHRINK);
    m_layout.pack_start(m_actionToolLink,Gtk::PACK_SHRINK);
}

void CanvasToggleToolBox::eventsToggle()
{
    m_actionToolLink.signal_pressed().connect( sigc::mem_fun(*this,
        &CanvasToggleToolBox::mutuallyExclusiveLink) );
    m_actionToolSelectArea.signal_pressed().connect( sigc::mem_fun(*this,
        &CanvasToggleToolBox::mutuallyExclusiveSelectArea) );
}


StatusCanvasToolBox CanvasToggleToolBox::whichIsActive()
{
    if(m_actionToolSelectArea.isActive())
    {
        return StatusCanvasToolBox::ACTION_TOOL_SELECT_AREA;
    }
    else if(m_actionToolLink.isActive())
    {
        return StatusCanvasToolBox::ACTION_TOOL_LINK;
    }
    else
    {
        return StatusCanvasToolBox::ACTION_TOOL_DEFAULT;
    }
}

void CanvasToggleToolBox::mutuallyExclusiveSelectArea()
{
    m_actionToolLink.deactivate();
}
void CanvasToggleToolBox::mutuallyExclusiveLink()
{
    m_actionToolSelectArea.deactivate();
}

CanvasToggleToolBox::~CanvasToggleToolBox()
{

}

Gtk::Box &CanvasToggleToolBox::getInstance()
{
    return m_layout;
}
