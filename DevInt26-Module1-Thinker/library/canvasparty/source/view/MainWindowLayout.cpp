#include "MainWindowLayout.h"

MainWindowLayout::MainWindowLayout()
:m_infobar{new InfoBar()}, 
 m_canvas{new Canvas()}, 
 m_canvasToggleToolBox{new CanvasToggleToolBox()}, 
 m_canvasButtonToolBox{new CanvasButtonToolBox()},
 m_menubar{new MenuBar()},
 m_toolBox{new ToolBox()},
 m_verticalLayout{Gtk::ORIENTATION_VERTICAL},
 m_horizontalLayout{Gtk::ORIENTATION_HORIZONTAL}
{

}

void MainWindowLayout::build()
{
    m_verticalLayout.pack_start(*(m_menubar->getInstance()),
        Gtk::PACK_SHRINK, 5);
    m_horizontalLayout.pack_start(m_toolBox->getInstance(),
        Gtk::PACK_SHRINK, 5);
    m_horizontalLayout.pack_start(*m_canvas,
        Gtk::PACK_EXPAND_WIDGET, 5);
    m_horizontalLayout.pack_start(m_canvasToggleToolBox->getInstance(),
        Gtk::PACK_SHRINK, 5);
    m_horizontalLayout.pack_end(m_canvasButtonToolBox->getInstance(),
        Gtk::PACK_SHRINK, 5);
    m_verticalLayout.pack_start(m_horizontalLayout,
        Gtk::PACK_EXPAND_WIDGET, 5);
    m_verticalLayout.pack_start(m_infobar->getInstance(),
         Gtk::PACK_SHRINK, 5);
    m_toolBox->getInstance().add_drag_dest(*m_canvas,
        Gtk::DEST_DEFAULT_HIGHLIGHT, Gtk::TOOL_PALETTE_DRAG_ITEMS,
        Gdk::ACTION_COPY);
    m_infobar->setMessageQuestion("Main Layout");
}


void MainWindowLayout::connect()
{
    m_canvas->connectWithActionBar(m_canvasToggleToolBox);
    m_canvasButtonToolBox->connectWithCanvas(m_canvas);
}

Canvas *MainWindowLayout::getCanvas()
{
    return m_canvas;
}

CanvasToggleToolBox *MainWindowLayout::getCanvasToggleToolBox()
{
    return m_canvasToggleToolBox;
}

CanvasButtonToolBox *MainWindowLayout::getCanvasButtonToolBox()
{
    return m_canvasButtonToolBox;
}

InfoBar *MainWindowLayout::getInfobar()
{
    return m_infobar;
}

MenuBar *MainWindowLayout::getMenubar()
{
    return m_menubar;
}

ToolBox *MainWindowLayout::getToolBox()
{
    return m_toolBox;
}

Gtk::Box &MainWindowLayout::getInstance()
{
    return m_verticalLayout;
}
