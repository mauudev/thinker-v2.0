#include "WindowUI.h"

WindowUI::WindowUI()
{
    set_title("Thinker");
    set_border_width(6);
    set_default_size(500, 500);
}

WindowUI::~WindowUI()
{
}

void WindowUI::addLayout(MainWindowLayout *p_layout)
{
    m_layout = p_layout;
    m_layout->build();
    m_layout->connect();
    add(m_layout->getInstance());
    show_all_children();
    show();
}
