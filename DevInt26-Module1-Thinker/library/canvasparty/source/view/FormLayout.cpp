#include "FormLayout.h"
#include <iostream>

FormLayout::FormLayout()
    : Gtk::Box(Gtk::ORIENTATION_VERTICAL)
{
}

void FormLayout::build()
{
}

void FormLayout::attachItem(Input &p_input)
{
    Input *accesorInput = &p_input;
    items.push_back(accesorInput);
    this->pack_start(p_input.getInstance(), Gtk::PACK_EXPAND_WIDGET, 5);
}
