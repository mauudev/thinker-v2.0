#include "ToolPalette.h"
ToolPalette::ToolPalette()
{
    loadItems();
}
ToolPalette::~ToolPalette()
{
}

void ToolPalette::loadItems()
{
    Gtk::ToolItemGroup* group = Gtk::manage(new Gtk::ToolItemGroup());

    Gtk::Label *labelButton = Gtk::manage(new Gtk::Label("Tool Definitions"));
    labelButton->show();
    group->set_label_widget(*labelButton);

    Gtk::ToolButton *button;
    button = Gtk::manage(new Gtk::ToolButton());
    button->set_icon_name("go-up");
    button->set_tooltip_text("Show on vertical palettes only");
    group->insert(*button);
    button->set_visible_horizontal(false);

    button = Gtk::manage(new Gtk::ToolButton());
    button->set_icon_name("go-next");
    button->set_tooltip_text("Show on horizontal palettes only");
    group->insert(*button);
    button->set_visible_vertical(false);

    button = Gtk::manage(new Gtk::ToolButton());
    button->set_icon_name("edit-delete");
    button->set_tooltip_text("Do not show at all");
    button->set_no_show_all();
    group->insert(*button);
    button->set_visible_vertical(false);

    button = Gtk::manage(new Gtk::ToolButton());
    button->set_icon_name("view-fullscreen");
    button->set_tooltip_text("Expanded this item");
    group->insert(*button);
    gtk_container_child_set (GTK_CONTAINER (group->gobj()), 
        GTK_WIDGET (button->gobj()),
        "homogeneous",
        TRUE,
        "expand",
        TRUE,
        NULL);

    button = Gtk::manage(new Gtk::ToolButton());
    button->set_icon_name("help-contents");
    button->set_tooltip_text("A regular item");
    group->insert(*button);

    m_toolPalette.add(*group);
}
Gtk::ToolPalette &ToolPalette::getInstance()
{
    return m_toolPalette;
}
