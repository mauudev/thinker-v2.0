#include "NewForm.h"
#include <string>
#include <iostream>

using namespace std;

NewForm::NewForm(const string &p_nameForm)
    :m_buttonCancel("Cancel"),
    m_buttonAccept("Ok"),
    m_boxWindow(Gtk::ORIENTATION_VERTICAL),
    m_boxComponents(Gtk::ORIENTATION_VERTICAL)
{
    this->set_default_size(300, 300);
    this->set_title(p_nameForm);
    builder();
}

NewForm::~NewForm()
{
}

void NewForm::onButtonCancelClicked()
{
    hide();
}

void NewForm::onButtonOkClicked()
{
    std::vector<Widget *> boxes=m_boxComponents.get_children();
    for(auto &box:boxes)
    {
        Gtk::Box *iteration = static_cast<Gtk::Box *>(box);
        std::vector<Widget *> iterator = iteration->get_children();
        for(auto &inputs:iterator)
        {
            if(inputs->get_name() != "gtkmm__GtkLabel")
            {
                GenericInput *input = dynamic_cast<GenericInput *>(inputs);
            }
        }
    }
}

void NewForm::addGenericInput(GenericInput &input)
{
    m_boxComponent.push_back(new Gtk::Box(Gtk::ORIENTATION_HORIZONTAL));
    m_label.push_back(new Gtk::Label{input.getDisplay()});
    (m_boxComponent.back())->pack_start(*(m_label.back()),
        Gtk::PACK_EXPAND_WIDGET, 5);
    (m_boxComponent.back())->pack_start(input.get(),
        Gtk::PACK_EXPAND_WIDGET, 5);
    m_boxComponents.pack_start(*(m_boxComponent.back()), Gtk::PACK_SHRINK, 10);
}

void NewForm::buttonsEvents()
{
    m_buttonAccept.signal_clicked().connect(sigc::mem_fun(*this,
        &NewForm::onButtonOkClicked) );

    m_buttonCancel.signal_clicked().connect(sigc::mem_fun(*this,
        &NewForm::onButtonCancelClicked) );
}

NewForm &NewForm::getInstance()
{
    return *this;
}

void NewForm::builder()
{
    m_scrolledWindow.add(m_boxComponents);
    m_scrolledWindow.set_policy(Gtk::POLICY_AUTOMATIC, Gtk::POLICY_AUTOMATIC);
    m_scrolledWindow.set_hexpand(true);
    m_scrolledWindow.set_vexpand(true);
    m_scrolledWindow.set_size_request(300,300);
    m_boxWindow.pack_start(m_scrolledWindow, Gtk::PACK_SHRINK);

    buttonsEvents();

    m_boxButtons.pack_start(m_buttonAccept, Gtk::PACK_EXPAND_PADDING);
    m_boxButtons.pack_end(m_buttonCancel, Gtk::PACK_EXPAND_PADDING);
    m_boxWindow.pack_end(m_boxButtons, Gtk::PACK_SHRINK);
    this->add(m_boxWindow);
    this->show_all_children();
}