#include "CanvasButtonToolBox.h"

CanvasButtonToolBox::CanvasButtonToolBox()
:m_layout(Gtk::ORIENTATION_VERTICAL, 2),
m_actionToolCopy{"../resources/add_image.svg"},
m_actionToolDelete{"../resources/remove_image.svg"},
m_actionToolClear{"../resources/clear.svg"}
{
    eventsButton();
    m_layout.pack_start(m_actionToolCopy,Gtk::PACK_SHRINK);
    m_layout.pack_start(m_actionToolDelete,Gtk::PACK_SHRINK);
    m_layout.pack_start(m_actionToolClear,Gtk::PACK_SHRINK);
}

void CanvasButtonToolBox::eventsButton()
{
    m_actionToolCopy.signal_clicked().connect( sigc::mem_fun(*this,
        &CanvasButtonToolBox::actionCanvasCopy) );
    m_actionToolDelete.signal_clicked().connect( sigc::mem_fun(*this,
        &CanvasButtonToolBox::actionCanvasDelete) );
    m_actionToolClear.signal_clicked().connect( sigc::mem_fun(*this,
        &CanvasButtonToolBox::actionCanvasClear) );
}

void CanvasButtonToolBox::actionCanvasCopy()
{
    if(m_canvas->getCanvasToolSize() > 0)
    {
        std::vector<Tool*> *tools = m_canvas->getToolVector();
        for(size_t i = 0U; i < tools->size(); i ++)
        {
            Tool *tool = tools->at(i);
            if(tool->isSelected())
            {
                auto *event = tool->getToolCopyEvent();
                auto canvas_context = m_canvas;
                event->addCallback([&] (Tool* tool) {
                    canvas_context->copyToolFromCanvas(tool);
                }); 
                event->trigger(tool);
            }
        }
    }
}

void CanvasButtonToolBox::actionCanvasDelete()
{
    if(m_canvas->getCanvasToolSize() > 0)
    {
        size_t i = 0U;
        std::vector<Tool*> *tools = m_canvas->getToolVector();
        while(i < tools->size())
        {
            Tool *tool = tools->at(i);
            if(tool->isSelected())
            {
                Linked *link = m_canvas->getToolLink(tool->getToolId());
                if(link != nullptr)
                {
                    m_canvas->removeLink(tool->getToolId());
                } 
                auto *event = tool->getToolDeleteEvent();
                auto canvas_context = m_canvas;
                event->addCallback([&] (Tool* tool) {
                     canvas_context->deleteToolFromCanvas(tool);
                }); 
                event->trigger(tool);
                i = 0U;
                continue;
            }
            i++;
        }
    }
}

CanvasButtonToolBox::~CanvasButtonToolBox()
{

}

Gtk::Box &CanvasButtonToolBox::getInstance()
{
    return m_layout;
}

void CanvasButtonToolBox::actionCanvasClear()
{
    m_canvas->functionClear(); 
}

void CanvasButtonToolBox::connectWithCanvas(Canvas *p_canvas)
{
    m_canvas = p_canvas;
}