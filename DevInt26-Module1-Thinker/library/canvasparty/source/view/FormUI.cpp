#include "FormUI.h"

FormUI::FormUI(const Glib::ustring &p_nameForm)
    : m_buttonCancel("Cancel"),
    m_buttonSubmit("Send"),
    m_box(Gtk::ORIENTATION_VERTICAL),
    m_windowName{p_nameForm}
{
}

FormUI::~FormUI()
{
}

void FormUI::onCancelEvent()
{
    m_window.hide();
}

void FormUI::onSubmitEvent()
{
}

void FormUI::loadListeners()
{
    m_buttonCancel.signal_clicked()
        .connect( sigc::mem_fun(
            *this,
            &FormUI::onCancelEvent)
        );

    m_buttonSubmit.signal_clicked()
        .connect( sigc::mem_fun(
            *this,
            &FormUI::onSubmitEvent)
        );
}

void FormUI::addInput(Input &p_input)
{
    m_layout.attachItem(p_input);
}

Gtk::Window &FormUI::getInstance()
{
    build();
    m_window.show_all_children();
    return m_window;
}

void FormUI::build()
{
    m_window.set_default_size(300, 300);
    m_window.set_title(m_windowName);

    m_boxButtons.pack_end(m_buttonSubmit, Gtk::PACK_EXPAND_PADDING);
    m_boxButtons.pack_end(m_buttonCancel, Gtk::PACK_EXPAND_PADDING);
    m_box.pack_end(m_boxButtons, Gtk::PACK_SHRINK);
    loadListeners();

    m_scroll.set_border_width(10);
    m_scroll.set_policy(Gtk::POLICY_AUTOMATIC, Gtk::POLICY_ALWAYS);
    m_box.add(m_scroll);

    m_scroll.add(m_layout);

    m_window.add(m_box);
    m_window.show_all_children();
    m_window.show();
}
