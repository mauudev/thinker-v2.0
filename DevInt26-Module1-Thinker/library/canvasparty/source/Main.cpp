#include "WindowUI.h"
#include <gtkmm/application.h>
#include "MessageType.h"


int main(int argc, char *argv[])
{

    //encapsular las nuevas instancias
    Glib::RefPtr<Gtk::Application> app= Gtk::Application::create();

    WindowUI window;
    //pedir a windowUI la instancia de mainwindow layout
    MainWindowLayout *mainW =new MainWindowLayout();

    MenuBar* menu1 = mainW->getMenubar();
    // //no tiene sentido que tiene sentido que reciba el mismo menu
    auto subMenu1 = menu1->addMenu("Project");
    auto itemMenu1 =menu1->addSubMenu(subMenu1,"New project");
    menu1->addEvent(itemMenu1,[]()
    {
        WindowUI *window2 = new WindowUI;
        MainWindowLayout *main_window =new MainWindowLayout();
        MenuBar *menu2 = main_window->getMenubar();
        auto subMenu2 = menu2->addMenu("Items");
        auto itemMenu2 =menu2->addSubMenu(subMenu2,"New Form");
        menu2->addEvent(itemMenu2,[]()
            {
                WindowUI* window3 = new WindowUI;
                MainWindowLayout *main_window2 =new MainWindowLayout();
                window3->addLayout(main_window2);
            });
        ToolDefinitionGroup* group1 = 
            main_window->getToolBox()->addToolDefinitionGroup(
                "Age","Agent");
        ToolDefinition* humanDefinition = group1->addToolDefinition(
            "Hum","Human","Human.png","Some text");


        ToolDefinition* agenDefinition = group1->addToolDefinition(
            "Agen", "Agente","Random.png","Random text");

        auto canvas = main_window->getCanvas();
        cout << "EL SIZE INICIAL DEL CANVAS ES: " << canvas->getCanvasToolSize() << endl;
        humanDefinition->getDragAndDropEvent()->addFunction(
            [&canvas](Tool *p_aux)
        {
            //auto aux_canvas = main_window->getCanvas();
            auto *delete_event = p_aux->getToolDeleteEvent();
            auto *copy_event = p_aux->getToolCopyEvent();
            delete_event->sayHello(); 
            copy_event->sayHello();
            // delete_event->addCallback([&] (Tool* tool) {
            //     cout << "Eliminando como loco !" << endl;
            //     aux_canvas->deleteToolFromCanvas(p_aux);
            // }); 
            // copy_event->addCallback([&] (Tool* tool) {
            //     cout << "Copiando como loco !" << endl;
            //     canvas->copyToolFromCanvas(tool);
            // });
            cout << "Size DELETE: "<< delete_event->getCallbacksSize() << "\n";
            cout << "Size COPY: "<< copy_event->getCallbacksSize() << "\n";
            cout << "Canvas size: " << canvas->getIdToolCount() << "\n";
            cout<<p_aux->getId()<<" idTool\n";
            puts("hola humanDefinition");
            //sirve para no dibujar
            // p_aux->destroy();
            
        });
        
        //////////////
        window2->addLayout(main_window);
    });
    window.addLayout(mainW);
   
    return app->run(window);
  
}
