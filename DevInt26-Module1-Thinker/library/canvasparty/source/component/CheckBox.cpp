#include "CheckBox.h"
#include <iostream>

CheckBox::CheckBox(const std::string &p_id, const std::string &p_display)
:m_id{p_id}, m_display{p_display}, Gtk::ButtonBox(Gtk::ORIENTATION_VERTICAL)
{
}

CheckBox::CheckBox(const std::string &p_id, const std::string &p_display,
    const Gtk::Orientation &p_orientation)
:m_id{p_id}, m_display{p_display}, Gtk::ButtonBox(p_orientation)
{
}

CheckBox::~CheckBox()
{
}

void CheckBox::joinInBox(const Gtk::PackOptions &p_option, int p_padding,
    CheckButton &p_child)
{
    this->pack_start(p_child, p_option, p_padding);
}

std::vector<CheckButton *> CheckBox::getSelected()
{
    std::vector<Gtk::Widget *> child = this->get_children();
    std::vector<CheckButton *> active;
    for(int i = 0; i < child.size(); ++i)
    {
        CheckButton *iteration = static_cast<CheckButton *>(child[i]);
        if(iteration->get_active())
            active.push_back(iteration);
    }
    return active;
}

const std::string &CheckBox::getId() const
{
    return m_id;
}

const std::string &CheckBox::getDisplay() const
{
    return m_display;
}

std::string CheckBox::getValue()
{
    std::string values;
    std::vector<CheckButton *> selected = getSelected();
    for (auto &iterator:selected)
        values+=iterator->getId()+" ";
    return values;
}

Gtk::ButtonBox &CheckBox::get()
{
    return *this;
}