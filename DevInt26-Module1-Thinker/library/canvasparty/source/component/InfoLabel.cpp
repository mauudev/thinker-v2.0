#include "InfoLabel.h"
#include <string>

using namespace std;

InfoLabel::InfoLabel(const string &p_message)
:Gtk::Label(p_message)
{
    this->set_line_wrap(true);
}

InfoLabel::~InfoLabel()
{
}

const std::string InfoLabel::getId() const
{
    return this->get_text();
}

const std::string InfoLabel::getDisplay() const
{
    return " ";
}

std::string InfoLabel::getValue()
{
    return this->get_text();
}

Gtk::Label &InfoLabel::get()
{
    return *this;
}
