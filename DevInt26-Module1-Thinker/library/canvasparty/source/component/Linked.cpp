#include "Linked.h"

Linked::Linked()
:m_widthLine{3.5},m_widthArrow{15.0},m_a{0,0},m_b{0,0},m_c{0,0}
{

}

Linked::~Linked()
{
    delete this;
}

Linked::Linked(Tool* p_tool1, Tool* p_tool2)
:m_tool1{p_tool1},m_tool2{p_tool2},m_widthLine{3.5},m_widthArrow{15.0}
,m_a{0,0},m_b{0,0},m_c{0,0},m_direccion{'1'}
{
}

Tool *Linked::getOriginTool()
{
    return m_tool1;
}

Tool *Linked::getDestinationTool()
{
    return m_tool2;
}

void Linked::link(Tool* p_tool1, Tool* p_tool2)
{
    m_tool1 = p_tool1;
    m_tool2 = p_tool2;
}

double Linked::calculateDistance(PointDouble& p_a,PointDouble& p_b)
{
    double distancia = 0.0;
    double ax = p_a.getCoorX();
    double ay = p_a.getCoorY();
    double bx = p_b.getCoorX();
    double by = p_b.getCoorY();
    distancia = sqrt( pow( bx-ax ,2) + pow( by-ay ,2) );
    return distancia;
}

void Linked::calculateCoordinatesA()
{
    double tool1x = (double) m_tool1->getPoint().getCoorX()+25;
    double tool1y = (double) m_tool1->getPoint().getCoorY()+25;
    double tool2x = (double) m_tool2->getPoint().getCoorX()+25;
    double tool2y = (double) m_tool2->getPoint().getCoorY()+25;

    double x=tool1x;
    double y=tool1y;

    if(tool2x>tool1x && tool2y<tool1y)
    {
        while(m_tool1->pointIsInside(Point{(int)x,(int)y}))
        {   
            y = (((tool2y-tool1y)/(tool2x-tool1x))*(x-tool1x))+tool1y;
            x = x+0.50;
        }
    }
    else if(tool2x>tool1x && tool2y>tool1y)
    {
        while(m_tool1->pointIsInside(Point{(int)x,(int)y}))
        {
            y = (((tool2y-tool1y)/(tool2x-tool1x))*(x-tool1x))+tool1y;
            x = x+0.50;
        }
    }
    else if(tool2x<tool1x && tool2y>tool1y)
    {
        while(m_tool1->pointIsInside(Point{(int)x,(int)y}))
        {
            y = (((tool2y-tool1y)/(tool2x-tool1x))*(x-tool1x))+tool1y;
            x = x-0.50;
        }
    }
    else if(tool2x<tool1x && tool2y<tool1y)
    {
        while(m_tool1->pointIsInside(Point{(int)x,(int)y}))
        {
            y = (((tool2y-tool1y)/(tool2x-tool1x))*(x-tool1x))+tool1y;
            x = x-0.50;
        }
    }
    else if(tool2x==tool1x && tool2y<tool1y)
    {
        while(m_tool1->pointIsInside(Point{(int)x,(int)y}))
        {
            y = y-0.5;
        }
    }
    else if(tool2x>tool1x && tool2y==tool1y)
    {
        while(m_tool1->pointIsInside(Point{(int)x,(int)y}))
        {
            x = x+0.5;
        }
    }
    else if(tool2x==tool1x && tool2y>tool1y)
    {
        while(m_tool1->pointIsInside(Point{(int)x,(int)y}))
        {
            y = y+0.5;
        }
    }
    else if(tool2x<tool1x && tool2y==tool1y)
    {
        while(m_tool1->pointIsInside(Point{(int)x,(int)y}))
        {
            x = x-0.5;
        }
    }
    m_a.setCoorX(x);
    m_a.setCoorY(y);
}

void Linked::calculateCoordinatesB()
{
    //saco cordenadas de los centros
    double tool1x = (double) m_tool2->getPoint().getCoorX()+25;
    double tool1y = (double) m_tool2->getPoint().getCoorY()+25;
    double tool2x = (double) m_tool1->getPoint().getCoorX()+25;
    double tool2y = (double) m_tool1->getPoint().getCoorY()+25;

    double x=tool1x;
    double y=tool1y;

    if(tool2x>tool1x && tool2y<tool1y)
    {
        while(m_tool2->pointIsInside(Point{(int)x,(int)y}))
        {   
            y = (((tool2y-tool1y)/(tool2x-tool1x))*(x-tool1x))+tool1y;
            x = x+0.50;
        }
    }
    else if(tool2x>tool1x && tool2y>tool1y)
    {
        while(m_tool2->pointIsInside(Point{(int)x,(int)y}))
        {
            y = (((tool2y-tool1y)/(tool2x-tool1x))*(x-tool1x))+tool1y;
            x = x+0.50;
        }
    }
    else if(tool2x<tool1x && tool2y>tool1y)
    {
        while(m_tool2->pointIsInside(Point{(int)x,(int)y}))
        {
            y = (((tool2y-tool1y)/(tool2x-tool1x))*(x-tool1x))+tool1y;
            x = x-0.50; 
        }
    }
    else if(tool2x<tool1x && tool2y<tool1y)
    {
        while(m_tool2->pointIsInside(Point{(int)x,(int)y}))
        {
            y = (((tool2y-tool1y)/(tool2x-tool1x))*(x-tool1x))+tool1y;
            x = x-0.50;
        }
    }
    else if(tool2x==tool1x && tool2y<tool1y)
    {
        while(m_tool2->pointIsInside(Point{(int)x,(int)y}))
        {
            y = y-0.5;
        }
    }
    else if(tool2x>tool1x && tool2y==tool1y)
    {
        while(m_tool2->pointIsInside(Point{(int)x,(int)y}))
        {
            x = x+0.5;
        }
    }
    else if(tool2x==tool1x && tool2y>tool1y)
    {
        while(m_tool2->pointIsInside(Point{(int)x,(int)y}))
        {
            y = y+0.5;
        }
    }
    else if(tool2x<tool1x && tool2y==tool1y)
    {
        while(m_tool2->pointIsInside(Point{(int)x,(int)y}))
        {
            x = x-0.5;
        }
    }

    m_b.setCoorX(x);
    m_b.setCoorY(y);
}

void Linked::calculateCoordinatesC()
{
    double distancia = calculateDistance(m_a,m_b);
    double arrow = 15.0;

    double distanciaC = distancia-arrow;
    PointDouble aux{m_a.getCoorX(),m_a.getCoorX()};
    double distanciaAux = calculateDistance(aux,m_a);

    double tool1x = aux.getCoorX();
    double tool1y = aux.getCoorY();
    double tool2x = m_b.getCoorX();
    double tool2y = m_b.getCoorY();

    double x=tool1x;
    double y=tool1y;

    while(distanciaAux<distanciaC)
    {
        if(tool2x>tool1x && tool2y<tool1y)
        {
            y = (((tool2y-tool1y)/(tool2x-tool1x))*(x-tool1x))+tool1y;
            x = x+0.50;
        }
        else if(tool2x>tool1x && tool2y>tool1y)
        {
            y = (((tool2y-tool1y)/(tool2x-tool1x))*(x-tool1x))+tool1y;
            x = x+0.50;
        }
        else if(tool2x<tool1x && tool2y>tool1y)
        {
            y = (((tool2y-tool1y)/(tool2x-tool1x))*(x-tool1x))+tool1y;
            x = x-0.50;
        }
        else if(tool2x<tool1x && tool2y<tool1y)
        {
            y = (((tool2y-tool1y)/(tool2x-tool1x))*(x-tool1x))+tool1y;
            x = x-0.50;
        }
        else if(tool2x==tool1x && tool2y<tool1y)
        {
            y = y-0.5;
        }
        else if(tool2x>tool1x && tool2y==tool1y)
        {
            x = x+0.5;
        }
        else if(tool2x==tool1x && tool2y>tool1y)
        {
            y = y+0.5;
        }
        else if(tool2x<tool1x && tool2y==tool1y)
        {
            x = x-0.5;
        }
        aux.setCoorX(x);
        aux.setCoorY(y);
        distanciaAux = calculateDistance(m_a,aux);
    }
    m_c.setCoorX(aux.getCoorX());
    m_c.setCoorY(aux.getCoorY());
}


PointDouble* Linked::getPointA()
{
    return &m_a;
}

PointDouble* Linked::getPointB()
{
    return &m_b;
}
PointDouble* Linked::getPointC()
{
    return &m_c;
}