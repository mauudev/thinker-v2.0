#include "Infobar.h"
#include <iostream>

InfoBar::InfoBar()
{
    Gtk::Container *infoBarContainer = static_cast<Gtk::Container*>(
    m_infoBar.get_content_area());
    infoBarContainer->add(m_messageLabel);
    m_infoBar.add_button("Hide", 0);
    m_infoBar.add_button("Clear", 1);
    m_infoBar.signal_response().connect(sigc::mem_fun(*this, 
        &InfoBar::onInfoBarResponse));
    m_infoBar.set_message_type(Gtk::MESSAGE_OTHER);
}

InfoBar::~InfoBar()
{
}

void InfoBar::onInfoBarResponse(int p_response)
{
    if(p_response)
    {
        m_messageLabel.set_text(" ");
        m_infoBar.set_message_type(Gtk::MESSAGE_OTHER);
        m_infoBar.show();
    }
    else
    {
        m_messageLabel.set_text(" ");
        m_infoBar.set_message_type(Gtk::MESSAGE_OTHER);
        m_infoBar.hide();
    }
}

Gtk::InfoBar &InfoBar::getInstance()
{
    return m_infoBar;
}
void InfoBar::setMessage(const std::string &p_label, MessageTypeInfoBar p_type)
{
    m_infoBar.show();
    m_messageLabel.set_text(p_label);
    m_infoBar.set_message_type((Gtk::MessageType)p_type);
}

    void InfoBar::setMessageInfo(const std::string &p_label)
    {
        setMessage(p_label, INFO);
    }

    void InfoBar::setMessageOther(const std::string &p_label)
    {
        setMessage(p_label, OTHER);
    }

    void InfoBar::setMessageWarning(const std::string &p_label)
    {
        setMessage(p_label, WARNING);
    }

    void InfoBar::setMessageError(const std::string &p_label)
    {
        setMessage(p_label, ERROR);
    }

    void InfoBar::setMessageQuestion(const std::string &p_label)
    {
        setMessage(p_label, QUESTION);
    }