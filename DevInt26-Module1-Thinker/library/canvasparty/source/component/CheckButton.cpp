#include "CheckButton.h"
#include <iostream>

CheckButton::CheckButton(const std::string &p_id, const std::string &p_label)
:m_id{p_id}, Gtk::CheckButton(p_label)
{
}

CheckButton::~CheckButton()
{
}

bool CheckButton::getState()
{
    return this->get_active();
}

const std::string &CheckButton::getId() const
{
    return m_id;
}

const std::string CheckButton::getDisplay() const
{
    return this->get_label();
}

Glib::RefPtr<CheckButton> CheckButton::create(const std::string &p_id,
    const std::string &p_label)
{
    return Glib::RefPtr<CheckButton>(new CheckButton(p_id, p_label));
}

CheckButton &CheckButton::getInstance()
{
    return *this;
}