#include "ToolDefinition.h"
#include <iostream>     

ToolDefinition::ToolDefinition(std::string p_toolName, std::string p_imageName, std::string p_tooltip, std::string p_toolId, int p_imageWidth, 
    int p_imageHeight)
    :Gtk::ToolButton(p_toolName),
    m_toolName{p_toolName},
    m_imageName{p_imageName},
    m_toolId{p_toolId},
    m_tooltip{p_tooltip},
    m_imageHeight{p_imageWidth},
    m_imageWidth{p_imageHeight},
    m_dragAndDropEvent{new DragAndDropEvent()}
{
    m_filepath = "../resources/"+m_imageName;
    try{
        m_imageResource = Gdk::Pixbuf::create_from_file(m_filepath,m_imageWidth
            ,m_imageHeight);
        m_image.set(m_imageResource);
        this->set_label(m_tooltip);
        this->set_icon_name(m_imageName);
        this->set_icon_widget(m_image);
    }
    catch(const std::exception &ex) 
    {
        std::cout << "An error occurred during image loading: " << 
            m_filepath << " traceback: " << ex.what() << "\n";
    }
}

ToolDefinition::ToolDefinition(std::string p_toolName, std::string p_imageName, std::string p_tooltip, std::string p_toolId)
    :Gtk::ToolButton(p_toolName),
    m_toolName{p_toolName},
    m_imageName{p_imageName},
    m_toolId{p_toolId},
    m_tooltip{p_tooltip},
    m_imageHeight{50},
    m_imageWidth{50},
    m_dragAndDropEvent{new DragAndDropEvent()}
{
    m_filepath = "../resources/"+m_imageName;
    try{
        m_imageResource = Gdk::Pixbuf::create_from_file(m_filepath,m_imageWidth
            ,m_imageHeight);
        m_image.set(m_imageResource);
        this->set_label(m_tooltip);
        this->set_icon_name(m_imageName);
        this->set_icon_widget(m_image);
    }
    catch(const std::exception &ex) 
    {
        std::cout << "An error occurred during image loading: " << 
            m_filepath << " traceback: " << ex.what() << "\n";
    }
}

ToolDefinition::~ToolDefinition()
{
    delete this;
}

std::string ToolDefinition::getToolName()const
{
    return m_toolName;
}

std::string ToolDefinition::getFilepath()const
{
    return m_filepath;
}

std::string ToolDefinition::getImageName()const
{
    return m_imageName;
}

std::string ToolDefinition::getToolId()const
{
    return m_toolId;
}

std::string ToolDefinition::getToolTip()const
{
    return m_tooltip;
}
Glib::RefPtr<Gdk::Pixbuf> ToolDefinition::getImage()
{
    return m_imageResource; 
}

void ToolDefinition::setImageWidth(const int p_imageWidth)
{
    m_imageWidth = p_imageWidth;
}

void ToolDefinition::setImageHeight(const int p_imageHeight)
{
    m_imageHeight = p_imageHeight;
}

void ToolDefinition::setToolName(const std::string p_toolName)
{
    m_toolName = p_toolName;
}

void ToolDefinition::setToolId(const std::string p_toolId)
{
    m_toolId = p_toolId;
}

void ToolDefinition::setImage(std::string p_imageName, int p_imageWidth, int p_imageHeight)
{
     ToolDefinition::setImageWidth(p_imageWidth);
     ToolDefinition::setImageHeight(p_imageHeight);
     m_imageName = p_imageName;
     m_filepath = "../resources/"+ m_imageName;
    try{
        m_imageResource = Gdk::Pixbuf::create_from_file(m_filepath,
            m_imageWidth,m_imageHeight);
        m_image.set(m_imageResource);
        this->set_icon_widget(m_image);
    }
    catch(const std::exception &ex) 
    {
        std::cout << "An error occurred during image loading: " << 
            m_filepath << " traceback: " << ex.what() << "\n";
    }
}

DragAndDropEvent *ToolDefinition::getDragAndDropEvent()
{
    return m_dragAndDropEvent;
}
