#include "ToolDefinitionGroup.h"
#include <string>
#include <iostream>

ToolDefinitionGroup::ToolDefinitionGroup(const std::string&
    p_nameToolDefinitionGroup)
    :m_mapToolDefinition{new std::map<std::string, ToolDefinition*>},
    Gtk::ToolItemGroup(p_nameToolDefinitionGroup)
{
}

ToolDefinitionGroup::~ToolDefinitionGroup()
{
}

ToolDefinition *ToolDefinitionGroup::addToolDefinition(
    const std::string &p_toolKey, 
    const std::string &p_toolName, 
    const std::string &p_imageName, 
    const std::string &p_tooltip)
{
    ToolDefinition *toolDefinition = new ToolDefinition(p_toolName,
        p_imageName, p_tooltip, p_toolKey);
    if (m_mapToolDefinition->insert(std::make_pair(p_toolKey, 
        toolDefinition)).second == false)
    {
        delete toolDefinition;
    }
    else
    {
        this->add(*toolDefinition);
    }
    return toolDefinition;
}