#include "ToolBox.h"
#include <iostream>

ToolBox::ToolBox()
    :m_mapToolDefinitionGroup 
    {new std::map<std::string, ToolDefinitionGroup *>}
{
}

ToolBox::~ToolBox()
{
}

ToolDefinitionGroup *ToolBox::addToolDefinitionGroup(
    const std::string &p_groupKey, const std::string &p_groupName)
{
    ToolDefinitionGroup *toolDefinitionGroup = 
        new ToolDefinitionGroup{p_groupName};

    if (m_mapToolDefinitionGroup->insert(std::make_pair
        (p_groupKey, toolDefinitionGroup)).second == false)
    {
        delete toolDefinitionGroup;
    }
    else
    {
        m_toolBox.add(*toolDefinitionGroup);
    }
    return toolDefinitionGroup;
}

Gtk::ToolPalette &ToolBox::getInstance()
{
    return m_toolBox;
}