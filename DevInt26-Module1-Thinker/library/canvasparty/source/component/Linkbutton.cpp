#include "Linkbutton.h"
#include <iostream>

LinkButton::LinkButton(const std::string &p_url, const std::string &p_label)
:m_linkButton{p_url, p_label}
{
}

LinkButton::~LinkButton()
{
}

void LinkButton::onLinkButtonClicked() const
{
    std::cout << "The " << m_linkButton.get_label() << " was clicked.\n";
}

Gtk::LinkButton &LinkButton::getInstance()
{
    return m_linkButton;
}