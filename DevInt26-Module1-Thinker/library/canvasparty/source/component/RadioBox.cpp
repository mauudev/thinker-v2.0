#include "RadioBox.h"
#include <iostream>

RadioBox::RadioBox(const std::string &p_id, const std::string &p_display)
:m_id{p_id}, m_display{p_display}, Gtk::ButtonBox(Gtk::ORIENTATION_VERTICAL)
{
}

RadioBox::RadioBox(const std::string &p_id, const std::string &p_display,
    const Gtk::Orientation &p_orientation)
:m_id{p_id}, m_display{p_display}, Gtk::ButtonBox(p_orientation)
{
}

RadioBox::~RadioBox()
{
}

void RadioBox::joinInBox(const Gtk::PackOptions &p_option, int p_padding,
    RadioButton &p_child)
{
    this->pack_start(p_child, p_option, p_padding);
}

void RadioBox::joinInGroup(RadioButton &p_radio)
{
    std::vector<Gtk::Widget *> child = this->get_children();
    RadioButton *firstButton = static_cast<RadioButton *>(child[0]);
    firstButton->set_active(true);
}

RadioButton *RadioBox::getSelected()
{
    std::vector<Gtk::Widget *> child = this->get_children();
    for(int i = 0; i < child.size(); ++i)
    {
        RadioButton *iteration = static_cast<RadioButton *>(child[i]);
        if(iteration->get_active())
            return iteration;
    }
}

const std::string &RadioBox::getId() const
{
    return m_id;
}

const std::string &RadioBox::getDisplay() const
{
    return m_display;
}

std::string RadioBox::getValue()
{
    std::string aux = getSelected()->getId();
    return aux;
}

Gtk::ButtonBox &RadioBox::get()
{
    return *this;
}