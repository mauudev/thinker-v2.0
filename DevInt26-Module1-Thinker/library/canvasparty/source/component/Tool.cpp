#include "Tool.h"
#include <string>


Tool::Tool(Gtk::Widget *p_canvas, ToolData *p_toolData,
    double p_x,
    double p_y)
{
    Glib::RefPtr<Gtk::IconTheme> iconTheme = 
        Gtk::IconTheme::get_for_screen(p_canvas->get_screen());
    int width = 0;
    int heigth = 0;
    Gtk::IconSize::lookup(Gtk::ICON_SIZE_DIALOG, width, heigth);

    m_toolData = p_toolData;
    this->m_pixbuf = p_toolData->getToolImage();
    this->m_x = p_x;
    this->m_y = p_y;
    this->m_heigth=p_y+25;
    this->m_width=p_x+25;


    m_coordinates.setCoorX(p_x);
    m_coordinates.setCoorY(p_y);
    m_flagSelected = false;
    m_flagMoving = false;
    m_isLinked = false;
    m_toolDeleteEvent = new ToolDeleteEvent();
    m_toolCopyEvent = new ToolCopyEvent();
}

ToolData *Tool::getToolData()
{
    return m_toolData;
}

bool Tool::isLinked()
{
    return m_isLinked;
}

void Tool::setLink(bool p_link)
{
    m_isLinked = p_link;
}

Glib::ustring Tool::getToolName()
{
    return m_toolData->getToolName();
}

ToolDeleteEvent *Tool::getToolDeleteEvent()
{
    return m_toolDeleteEvent;
}

ToolCopyEvent *Tool::getToolCopyEvent()
{
    return m_toolCopyEvent;
}

Glib::ustring Tool::getToolId()
{
    return m_toolData->getToolId();
}

void Tool::setToolCopyEvent(ToolCopyEvent *p_event)
{
    m_toolCopyEvent = p_event;
}

int Tool::getAxisX()
{
    return this->m_x;
}
int Tool::getAxisY()
{
    return this->m_y;
}
 Glib::RefPtr<Gdk::Pixbuf> Tool::getToolImage()
 {
    return this->m_pixbuf;
 }
int Tool::getWidth()
 {
    return this->m_width;
 }
int Tool::getHeigth()
{
    return this->m_heigth;
}

bool Tool::pointIsInside(Point p_point)
{
    if(p_point.getCoorX() >= m_coordinates.getCoorX() &&
        p_point.getCoorY() >= m_coordinates.getCoorY() &&
        p_point.getCoorX() <= (m_coordinates.getCoorX()+50) &&
        p_point.getCoorY() <= (m_coordinates.getCoorY()+50))
    {
        return true;
    }
    return false;
}

Point& Tool::getPoint()
{
    return m_coordinates;
}

bool Tool::isSelected()
{
    return m_flagSelected;
}

bool Tool::isMoving()
{
    return m_flagMoving;
}

void Tool::activateFlagSelect()
{
    m_flagSelected=true;
}

void Tool::deactivateFlagSelect()
{
    m_flagSelected=false;
}

void Tool::activateFlagMoving()
{
    m_flagMoving=true;
}

void Tool::deactivateFlagMoving()
{
    m_flagMoving=false;
}
std::string Tool::getId()
{
    return m_toolData->getToolId();
}
void Tool::destroy()
{
    m_destroy = true;

}
bool Tool::getDestroy()
{
    return m_destroy;

}