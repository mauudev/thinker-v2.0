#include "TextArea.h"
#include <string>

using namespace std;

TextArea::TextArea(const string &p_id, const string &p_display)
:Gtk::Box(Gtk::ORIENTATION_VERTICAL), m_id{p_id}, m_display{p_display}
{
    this->pack_start(m_scrolledWindow);
    m_scrolledWindow.add(m_textView);
    m_scrolledWindow.set_policy(Gtk::POLICY_AUTOMATIC, Gtk::POLICY_AUTOMATIC);
    m_scrolledWindow.set_hexpand(true);
    m_scrolledWindow.set_vexpand(true);
    m_refTextBuffer = Gtk::TextBuffer::create();
    m_textView.set_buffer(m_refTextBuffer);
}

TextArea::TextArea(const string &p_id, const string &p_display, 
    Gtk::Orientation &p_orientation)
:Gtk::Box(p_orientation), m_id{p_id}, m_display{p_display}
{
    this->pack_start(m_scrolledWindow);
    m_scrolledWindow.add(m_textView);
    m_scrolledWindow.set_policy(Gtk::POLICY_AUTOMATIC, Gtk::POLICY_AUTOMATIC);
    m_scrolledWindow.set_hexpand(true);
    m_scrolledWindow.set_vexpand(true);
    m_refTextBuffer = Gtk::TextBuffer::create();
    m_textView.set_buffer(m_refTextBuffer);
}

TextArea::~TextArea()
{
}

const string &TextArea::getId() const 
{
    return m_id;
}

const string &TextArea::getDisplay() const
{
    return m_display;
}

std::string TextArea::getValue()
{
    return m_refTextBuffer->get_text(false);
}

Gtk::Box &TextArea::get()
{
    return *this;
}