#include "RadioButton.h"
#include <iostream>

RadioButton::RadioButton(const std::string &p_id, const std::string &p_label)
:m_id{p_id}, Gtk::RadioButton(p_label)
{
}

RadioButton::~RadioButton()
{
}

void RadioButton::joinGroup(RadioButton &p_radioButton)
{
    this->join_group(p_radioButton);
}

bool RadioButton::getState()
{
    return this->get_active();
}

const std::string &RadioButton::getId() const
{
    return m_id;
}

const std::string RadioButton::getDisplay() const
{
    return this->get_label();
}

Glib::RefPtr<RadioButton> RadioButton::create(const std::string &p_id,
    const std::string &p_label)
{
    return Glib::RefPtr<RadioButton>(new RadioButton(p_id, p_label));
}

RadioButton &RadioButton::getInstance()
{
    return *this;
}