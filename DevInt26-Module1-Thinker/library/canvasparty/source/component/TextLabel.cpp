#include "TextLabel.h"
#include <string>

using namespace std;

TextLabel::TextLabel(const string &p_id, const string &p_display)
:Gtk::Entry(), m_id{p_id}, m_display{p_display}
{
}

TextLabel::~TextLabel()
{
}

const std::string &TextLabel::getId() const
{
    return m_id;
}

const std::string &TextLabel::getDisplay() const
{
    return m_display;
}

std::string TextLabel::getValue()
{
    return this->get_text();
}

Gtk::Entry &TextLabel::get()
{
    return *this;
}