#include "IntegerLabel.h"
#include <cstdio>
#include <string>
using namespace std;

IntegerLabel::IntegerLabel(const string &p_id, const string &p_display)
:Gtk::SpinButton(Gtk::Adjustment::create(0.0, -10000.0, 10000.0), 2, 0),
m_id{p_id}, m_display{p_display}
{
    this->set_wrap();
    this->set_size_request(100, -1);
}

IntegerLabel::~IntegerLabel()
{
}

const std::string &IntegerLabel::getId() const
{
    return m_id;
}

const std::string &IntegerLabel::getDisplay() const
{
    return m_display;
}

std::string IntegerLabel::getValue()
{
    return std::to_string(this->get_value());
}

Gtk::SpinButton &IntegerLabel::get()
{
    return *this;
}