#include <iostream>
#include "Canvas.h"
#include "DataCollection.h"
#include "ToolData.h"
#include "ToolDefinition.h"
#include "ToolDeleteEvent.h"
#include <string>

Canvas::Canvas()
:m_dragDataRequestedForDrop(false), 
m_dropItem(nullptr),
m_flag(false),
m_idToolCount{0},
m_isClickReleasedForInsert{true},
m_areaStart{0,0},
m_areaEnd{0,0},
m_flagArea{false},
m_widthArea{0},
m_heigthArea{0},
m_flagLink{false},
m_pointLink{50,50},
m_widthLink{0},
m_link{nullptr},
m_canvasToggleToolBox{nullptr},
m_tool1{nullptr},
m_tool2{nullptr}
{
    add_events(Gdk::BUTTON_PRESS_MASK | Gdk::BUTTON_RELEASE_MASK | 
        Gdk::POINTER_MOTION_MASK | Gdk::KEY_PRESS_MASK | 
        Gdk::KEY_RELEASE_MASK);
    m_dataCollection = new DataCollection();
    set_app_paintable();
    this->set_can_focus(true);
}

Canvas::~Canvas()
{
    while(!m_canvasTool.empty())
    {
        std::vector<Tool*>::iterator iter = m_canvasTool.begin();
        Tool *item = *iter;
        delete item;
        m_canvasTool.erase(iter);
    }
    delete m_dropItem;
}

size_t Canvas::getIdToolCount()
{
    return m_idToolCount;
}

size_t Canvas::getCanvasToolSize()
{
    return m_canvasTool.size();
}

size_t Canvas::getCanvasLinkedSize()
{
    return m_links.size();
}

void Canvas::setIdToolCount(const size_t &p_count)
{
    m_idToolCount = p_count;
}

std::vector<Tool*> *Canvas::getToolVector()
{
    return &m_canvasTool;
}

DataCollection* Canvas::getDataCollection()
{
    return m_dataCollection;
}

Linked *Canvas::getToolLink(const Glib::ustring &p_toolId)
{
    Linked *target = nullptr;
    for(size_t i = 0; i < m_links.size(); i++)
    {
        Tool *origin = m_links.at(i)->getOriginTool();
        Tool *destination = m_links.at(i)->getDestinationTool();
        if(origin->getToolId() == p_toolId || 
            destination->getToolId() == p_toolId)
        {
            target = m_links.at(i);
            break;
        }
    }
    return target;
}

void Canvas::removeLink(const Glib::ustring &p_toolId)
{
    size_t i = 0U;
    while(i < m_links.size())
    {
        Tool *origin = m_links.at(i)->getOriginTool();
        Tool *destination = m_links.at(i)->getDestinationTool();
        if(origin->getToolId() == p_toolId || 
            destination->getToolId() == p_toolId)
        {
            origin->setLink(false);
            destination->setLink(false);
            m_links.erase(m_links.begin()+i);
            i = 0U;
            continue;
        }
        i++;
    }
}

void Canvas::queueDraw()
{
    queue_draw();
}

void Canvas::itemDraw(const Tool *p_item,
    const Cairo::RefPtr<Cairo::Context> &p_cairo,
    bool p_preview)
{
    // std::cout << "itemDraw\n";
    // if(!p_item || !p_item->m_pixbuf)
    //     return;

    // const double cairoX = p_item->m_pixbuf->get_width();
    // const double cairoY = p_item->m_pixbuf->get_height();
    
    // Gdk::Cairo::set_source_pixbuf(p_cairo, p_item->m_pixbuf,
    // p_item->m_x - cairoX * 0.5, p_item->m_y - cairoY * 0.5);    
    // if(p_preview)
    // {
    //     p_cairo->paint_with_alpha(0.6);
    // }
    // else
    // {
    //     p_cairo->paint();
    // }
}

bool Canvas::on_draw(const Cairo::RefPtr<Cairo::Context> &p_cairo)
{
    p_cairo->set_source_rgb(1, 1, 1);
    const Gtk::Allocation allocation = get_allocation();
    p_cairo->rectangle(0, 0, allocation.get_width(), allocation.get_height());
    p_cairo->fill();

    if(m_dropItem)
    {
        itemDraw (m_dropItem, p_cairo, false);
    }
    drawLink(p_cairo);

    std::vector<Tool*>::iterator it = m_canvasTool.begin();
    for (it=m_canvasTool.begin(); it!=m_canvasTool.end(); ++it)
    {
        if((*it)->isSelected())
        {
            drawRoundedPath(p_cairo,(*it)->getPoint().getCoorX(),(*it)
                ->getPoint().getCoorY(),50,50,8);
        }
        Gdk::Cairo::set_source_pixbuf(p_cairo,
            (*it)->getToolImage(),
            (*it)->getPoint().getCoorX(),
            (*it)->getPoint().getCoorY());
        p_cairo->paint();
    }

    std::vector<Linked*>::iterator itl = m_links.begin();
    for (itl=m_links.begin(); itl!=m_links.end(); ++itl)
    {
        (*itl)->calculateCoordinatesA();
        (*itl)->calculateCoordinatesB();
        (*itl)->calculateCoordinatesC();
        p_cairo->set_source_rgb(0.0,1,0.0);
        p_cairo->set_line_width(3);       
        p_cairo->move_to((*itl)->getPointA()->getCoorX(),(*itl)->getPointA()->getCoorY());
        p_cairo->line_to((*itl)->getPointB()->getCoorX(),(*itl)->getPointB()->getCoorY());
        p_cairo->stroke();
    }

    p_cairo->set_source_rgba(0.0,0.0,0.9,0.1);
    p_cairo->set_line_width(2.0);
    p_cairo->rectangle(m_initXArea,m_initYArea,m_widthArea,m_heigthArea);
    p_cairo->fill_preserve();
    p_cairo->stroke();

    return true;
}

bool Canvas::on_drag_motion(const Glib::RefPtr<Gdk::DragContext> &p_context,
    int p_x,
    int p_y,
    guint p_time)
{
    m_dragDataRequestedForDrop = false;

    if(m_dropItem)
    {
        m_dropItem->m_x = p_x;
        m_dropItem->m_y = p_y;
        queueDraw();
        p_context->drag_status(Gdk::ACTION_COPY, p_time);

    }
    else
    {
        const Glib::ustring target = drag_dest_find_target(p_context);
        if (target.empty())
        {
            return false;
        }

        drag_get_data(p_context, target, p_time);
    }

    Gtk::DrawingArea::on_drag_motion(p_context, p_x, p_y, p_time);

    return true;
}

void Canvas::on_drag_data_received(
    const Glib::RefPtr<Gdk::DragContext> &p_context, 
    int p_x, 
    int p_y, 
    const Gtk::SelectionData &p_selectionData,
    guint p_info, guint p_time)
{
    std::cout << "on_drag_data_received\n";
    Gtk::Widget* widget =nullptr;
    widget = drag_get_source_widget(p_context);

    Gtk::ToolPalette *drag_palette = dynamic_cast<Gtk::ToolPalette*>(widget);
    while(widget && !drag_palette)
    {
        widget = widget->get_parent();
        drag_palette = dynamic_cast<Gtk::ToolPalette*>(widget);
    }

    Gtk::ToolItem *drag_item = nullptr;
    if(drag_palette)
    {
        drag_item = drag_palette->get_drag_item(p_selectionData);
    }

    ToolDefinition *button = dynamic_cast<ToolDefinition*>(drag_item);
    if(button)
    {
        delete m_dropItem;
        m_dropItem = nullptr;
    }
    try 
    {
        ToolData *tooldata = new ToolData(button->getToolId()+
            std::to_string(m_idToolCount),
            button->getToolName(),button->getImage());

        if(m_isClickReleasedForInsert)
        {
            m_dataCollection->insert(tooldata->getToolId()
                ,tooldata->getToolName());
            m_isClickReleasedForInsert = false;
        }

        m_idToolCount++;
        Tool *item = new Tool(this, tooldata, p_x-25, p_y-25);

        if(m_dragDataRequestedForDrop)
        {
            button->getDragAndDropEvent()->trigger(item);
            if (item->getDestroy()== true)
            {
                delete item;
                return;
            }
            else
            {
                m_canvasTool.push_back(item);
                p_context->drag_finish(true, false, p_time);
            }
        }
        else
        {
            m_dropItem = item;
            p_context->drag_status(Gdk::ACTION_COPY, p_time);
        }
        queueDraw();
    }
    catch (const Gtk::IconThemeError &errorX)
    {
        std::cerr << "IconThemeError: " << errorX.what() << std::endl;
    }

    Gtk::DrawingArea::on_drag_data_received(p_context, p_x, p_y,
        p_selectionData, p_info, p_time);
}

bool Canvas::on_drag_drop(const Glib::RefPtr<Gdk::DragContext> &p_context, 
    int p_x,
    int p_y,
    guint p_time)
{
    std::cout << "on_drag_drop\n";
    const Glib::ustring p_target = drag_dest_find_target(p_context);

    if (p_target.empty())
    {
        return false;
    }
    m_dragDataRequestedForDrop = true;
    drag_get_data(p_context, p_target, p_time);
    m_isClickReleasedForInsert = true;
    return true;
}


bool Canvas::on_button_press_event(GdkEventButton *p_event)
{
    if (p_event->button == 1)
    {
        if(m_canvasToggleToolBox!=nullptr)
        {
            if(m_canvasToggleToolBox->whichIsActive()==StatusCanvasToolBox::ACTION_TOOL_DEFAULT)
            {
                functionSelect(p_event->x,p_event->y);
            }
            else if(m_canvasToggleToolBox->whichIsActive()==StatusCanvasToolBox::ACTION_TOOL_SELECT_AREA)
            {
                functionSelectArea(p_event->x,p_event->y);
            }
            else if(m_canvasToggleToolBox->whichIsActive()==StatusCanvasToolBox::ACTION_TOOL_LINK)
            {
                functionLink(p_event->x,p_event->y);
            }
        }
    }
    return true;
}

bool Canvas::on_motion_notify_event(GdkEventMotion*p_event)
{
    if(m_canvasToggleToolBox!=nullptr)
    {
        if(m_canvasToggleToolBox->whichIsActive()==StatusCanvasToolBox::ACTION_TOOL_DEFAULT)
        {
            functionMovingCursorSelect(p_event->x,p_event->y);
        }
        else if(m_canvasToggleToolBox->whichIsActive()==StatusCanvasToolBox::ACTION_TOOL_SELECT_AREA)
        {
            functionMovingCursorSelectArea(p_event->x,p_event->y);
        }
        else if(m_canvasToggleToolBox->whichIsActive()==StatusCanvasToolBox::ACTION_TOOL_LINK)
        {
            functionMovingCursorLink(p_event->x,p_event->y);
        }
    }
    return true;
}

bool Canvas::on_button_release_event(GdkEventButton *p_event)
{   
    if(m_canvasToggleToolBox!=nullptr)
    {
        if(m_canvasToggleToolBox->whichIsActive()==StatusCanvasToolBox::ACTION_TOOL_DEFAULT)
        {
            functionReleaseClickSelect();
        }
        else if(m_canvasToggleToolBox->whichIsActive()==StatusCanvasToolBox::ACTION_TOOL_SELECT_AREA)
        {
            functionReleaseClickSelectArea();
        }
        else if(m_canvasToggleToolBox->whichIsActive()==StatusCanvasToolBox::ACTION_TOOL_LINK)
        {
            functionReleaseClickLink(p_event->x,p_event->y);
        }
    }
    return true;
}

void Canvas::functionSelect(int p_coorX,int p_coorY)
{

    std::vector<Tool*>::iterator it1 = m_canvasTool.begin();
    for (it1=m_canvasTool.begin(); it1!=m_canvasTool.end(); ++it1)
    {
        (*it1)->deactivateFlagSelect();
    }

    std::vector<Tool*>::iterator it = m_canvasTool.begin();
    for (it=m_canvasTool.begin(); it!=m_canvasTool.end(); ++it)
    {
        if((*it)->pointIsInside(Point{p_coorX,p_coorY}))
        {
            (*it)->activateFlagSelect();
            (*it)->activateFlagMoving();
            break;
        }
    }
}

Tool* Canvas::getSelectedTool(int p_coorX, int p_coorY)
{
    Tool *tool = nullptr;
    int index = 0;
    std::vector<Tool*>::iterator it = m_canvasTool.begin();
    for (it=m_canvasTool.begin(); it!=m_canvasTool.end(); ++it)
    {
        if((*it)->pointIsInside(Point{p_coorX,p_coorY}))
        {
            tool = m_canvasTool.at(index);
            break;
        }
        index++;
    }
    return tool; 
}

void Canvas::deleteToolFromCanvas(Tool *tool)
{
    int index = 0;
    std::vector<Tool*>::iterator it = m_canvasTool.begin();
    for (it=m_canvasTool.begin(); it!=m_canvasTool.end(); ++it)
    {
        if((*it)->getToolId() == tool->getToolId())
        {
            m_canvasTool.erase(m_canvasTool.begin()+index);
            break;
        }
        index++;
    }
    queueDraw();
}

void Canvas::copyToolFromCanvas(Tool* tool)
{
    ToolData *tooldata = tool->getToolData();

    m_idToolCount++;
    Glib::ustring toolId = tooldata->getToolId() + 
        std::to_string(m_idToolCount);

    ToolData *new_tooldata = new ToolData(toolId,tooldata->getToolName(),
        tooldata->getToolImage());

    Tool *new_tool = new Tool(this,new_tooldata,tool->getAxisX()+15, 
        tool->getAxisY()+15);

    new_tool->setToolCopyEvent(tool->getToolCopyEvent());
    m_canvasTool.push_back(new_tool);
    m_dataCollection->insert(tooldata->getToolId(),tooldata->getToolName());
    queueDraw();
}

void Canvas::functionSelectArea(int p_coorX,int p_coorY)
{
    m_flagArea = true;
    m_areaStart.setCoorX(p_coorX);
    m_areaStart.setCoorY(p_coorY);
}

void Canvas::functionLink(int p_coorX,int p_coorY)
{

    std::vector<Tool*>::iterator it = m_canvasTool.begin();
    for (it=m_canvasTool.begin(); it!=m_canvasTool.end(); ++it)
    {
        if((*it)->pointIsInside(Point{p_coorX,p_coorY}))
        {
           m_flagLink = true;
           m_linkStart.setCoorX((*it)->getPoint().getCoorX()+25);
           m_linkStart.setCoorY((*it)->getPoint().getCoorY()+25);
           m_linkEnd.setCoorX(m_linkStart.getCoorX());
           m_linkEnd.setCoorY(m_linkStart.getCoorY());
           m_tool1 = *it;
           break;
        }
    }
}

void Canvas::functionMovingCursorSelect(int p_coorX,int p_coorY)
{
    int deltaX=p_coorX-m_focusX;
    int deltaY=p_coorY-m_focusY;
    std::vector<Tool*>::iterator it = m_canvasTool.begin();
    for (it=m_canvasTool.begin(); it!=m_canvasTool.end(); ++it)
    {
        if((*it)->isMoving())
        {
            (*it)->getPoint().setCoorX((*it)->getPoint().getCoorX()+deltaX);
            (*it)->getPoint().setCoorY((*it)->getPoint().getCoorY()+deltaY);
        }
    }
    queueDraw();
    m_focusX=p_coorX;
    m_focusY=p_coorY;
}

void Canvas::functionMovingCursorDelete(int p_coorX,int p_coorY)
{

}

void Canvas::functionMovingCursorSelectArea(int p_coorX,int p_coorY)
{
    if(m_flagArea)
    {
        m_areaEnd.setCoorX(p_coorX);
        m_areaEnd.setCoorY(p_coorY);
        if(m_areaEnd.getCoorX()>=m_areaStart.getCoorX() &&
            m_areaEnd.getCoorY()>=m_areaStart.getCoorY())
        {
            m_initXArea = m_areaStart.getCoorX();
            m_initYArea = m_areaStart.getCoorY();
            m_widthArea = m_areaEnd.getCoorX()-m_areaStart.getCoorX();
            m_heigthArea = m_areaEnd.getCoorY()-m_areaStart.getCoorY();
        }
        else if(m_areaEnd.getCoorX()>=m_areaStart.getCoorX() && 
            m_areaEnd.getCoorY()<=m_areaStart.getCoorY())
        {
            m_initXArea = m_areaStart.getCoorX();
            m_initYArea = m_areaEnd.getCoorY();
            m_widthArea = m_areaEnd.getCoorX()-m_areaStart.getCoorX();
            m_heigthArea = m_areaStart.getCoorY()-m_areaEnd.getCoorY();
        }
        else if(m_areaEnd.getCoorX()<=m_areaStart.getCoorX() && 
            m_areaEnd.getCoorY()<=m_areaStart.getCoorY())
        {
            m_initXArea = m_areaEnd.getCoorX();
            m_initYArea = m_areaEnd.getCoorY();
            m_widthArea = m_areaStart.getCoorX()-m_areaEnd.getCoorX();
            m_heigthArea = m_areaStart.getCoorY()-m_areaEnd.getCoorY(); 
        }
        else if(m_areaEnd.getCoorX()<=m_areaStart.getCoorX() && 
            m_areaEnd.getCoorY()>=m_areaStart.getCoorY())
        {
            m_initXArea = m_areaEnd.getCoorX();
            m_initYArea = m_areaStart.getCoorY();
            m_widthArea = m_areaStart.getCoorX()-m_areaEnd.getCoorX();
            m_heigthArea = m_areaEnd.getCoorY()-m_areaStart.getCoorY();
        }
    }
    queueDraw();
}


void Canvas::functionMovingCursorLink(int p_coorX,int p_coorY)
{
    if(m_flagLink)
    {
        m_linkEnd.setCoorX(p_coorX);
        m_linkEnd.setCoorY(p_coorY);
    }
    queueDraw();
}

void Canvas::drawLink(const Cairo::RefPtr<Cairo::Context>& p_cr)
{
    p_cr->set_source_rgb (0.1, 0.2, 0.3);
    p_cr->set_line_width (4);
    p_cr->move_to(m_linkStart.getCoorX(),m_linkStart.getCoorY());
    p_cr->line_to(m_linkEnd.getCoorX(),m_linkEnd.getCoorY());
    p_cr->stroke();
}

void Canvas::drawRoundedPath(const Cairo::RefPtr<Cairo::Context>& p_cr, 
    double p_x, 
    double p_y,
    double p_width, 
    double p_height, 
    double p_radius) 
{

    double degrees = M_PI / 180.0;

    p_cr->arc(p_x + p_width - p_radius, p_y + p_radius, p_radius, -
        90 * degrees,0 * degrees);
    p_cr->arc(p_x + p_width - p_radius, p_y + p_height - p_radius, p_radius, 
        0 * degrees, 90 * degrees);
    p_cr->arc(p_x + p_radius, p_y + p_height - p_radius, p_radius, 
        90 * degrees, 180 * degrees);
    p_cr->arc(p_x + p_radius, p_y + p_radius, p_radius, 180 * degrees, 
        270 * degrees);
    p_cr->set_source_rgb (0.5, 0.5, 0.5);
    p_cr->fill_preserve ();
    p_cr->set_line_width (0);
    p_cr->stroke ();
    queueDraw();
}

void Canvas::drawSelectArea(int p_initXArea,int p_initYArea,int p_widthArea,int p_heithArea)
{
    m_initXArea = p_initXArea;
    m_initYArea = p_initYArea;
    m_widthArea = p_widthArea;
    m_heigthArea = p_heithArea;
    queueDraw();
}

void Canvas::functionReleaseClickSelect()
{
    std::vector<Tool*>::iterator it = m_canvasTool.begin();
    for (it=m_canvasTool.begin(); it!=m_canvasTool.end(); ++it)
    {
            (*it)->deactivateFlagMoving();
    }
}

void Canvas::functionReleaseClickSelectArea()
{

    std::vector<Tool*>::iterator it1 = m_canvasTool.begin();
    for (it1=m_canvasTool.begin(); it1!=m_canvasTool.end(); ++it1)
    {
        (*it1)->deactivateFlagSelect();
    }

    int c = 0;
    std::vector<Tool*>::iterator it = m_canvasTool.begin();
    for (it=m_canvasTool.begin(); it!=m_canvasTool.end(); ++it)
    {
        int x=(*it)->getPoint().getCoorX();
        int y=(*it)->getPoint().getCoorY();
        if(x>m_initXArea 
            && y>m_initYArea 
            && x<(m_initXArea+m_widthArea) 
            && y<(m_initYArea+m_heigthArea))
        {
            (*it)->activateFlagSelect();
            c++;
            continue;
        }
        else if(x+50>m_initXArea 
            && y>m_initYArea 
            && x+50<(m_initXArea+m_widthArea) 
            && y<(m_initYArea+m_heigthArea))
        {
            (*it)->activateFlagSelect();
            c++;
            continue;
        }
        else if(x+50>m_initXArea 
            && y+50>m_initYArea 
            && x+50<(m_initXArea+m_widthArea) 
            && y+50<(m_initYArea+m_heigthArea))
        {
            (*it)->activateFlagSelect();
            c++;
            continue;
        }
        else if(x>m_initXArea 
            && y+50>m_initYArea 
            && x<(m_initXArea+m_widthArea) 
            && y+50<(m_initYArea+m_heigthArea))
        {
            (*it)->activateFlagSelect();
            c++;
            continue;
        }
    }
    m_flagArea = false;
    m_areaStart.setCoorX(0);
    m_areaStart.setCoorY(0);
    m_areaEnd.setCoorX(0);
    m_areaEnd.setCoorY(0);
    m_initXArea = 0;
    m_initYArea = 0;
    m_widthArea = 0;
    m_heigthArea = 0;
    queueDraw();
}

void Canvas::functionReleaseClickLink(int p_coorX,int p_coorY)
{
    std::vector<Tool*>::iterator it = m_canvasTool.begin();
    for (it=m_canvasTool.begin(); it!=m_canvasTool.end(); ++it)
    {
        if((*it)->pointIsInside(Point{p_coorX,p_coorY}))
        {
            m_linkEnd.setCoorX((*it)->getPoint().getCoorX()+25);
            m_linkEnd.setCoorY((*it)->getPoint().getCoorY()+25);
            if(m_linkStart.getCoorX()!=m_linkEnd.getCoorX() &&
                m_linkStart.getCoorY()!=m_linkEnd.getCoorY())
            {
                m_tool2 = *it;
                m_tool1->setLink(true);
                m_tool2->setLink(true);
                m_links.push_back(new Linked(m_tool1,m_tool2));
            }
        }
    }
    m_tool1=nullptr;
    m_tool2=nullptr;
    m_flagLink = false;
    m_linkStart.setCoorX(0);
    m_linkStart.setCoorY(0);
    m_linkEnd.setCoorX(0);
    m_linkEnd.setCoorY(0);
    queueDraw();
}


void Canvas::connectWithActionBar(CanvasToggleToolBox* p_canvasToolBox)
{
    m_canvasToggleToolBox = p_canvasToolBox;
}

void Canvas::functionClear()
{
    m_canvasTool.clear();
    m_links.clear();
    setIdToolCount(0);
    queueDraw();
}

void Canvas::sendFunctionClear()
{
    //m_canvasToggleToolBox->actionCanvasClear([this](){this->functionClear();});
}