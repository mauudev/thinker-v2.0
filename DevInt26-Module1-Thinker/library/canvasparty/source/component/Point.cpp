#include "Point.h"

Point::Point()
:m_coorX{0},m_coorY{0}
{

}

Point::Point(int p_coorX, int p_coorY)
:m_coorX{p_coorX},m_coorY{p_coorY}
{
}

int Point::getCoorX()
{
    return m_coorX;
}

int Point::getCoorY()
{
    return m_coorY;
}

void  Point::setCoorX(int p_coorX)
{
    m_coorX=p_coorX;
}

void  Point::setCoorY(int p_coorY)
{
    m_coorY=p_coorY;
}