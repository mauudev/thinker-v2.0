#include "Menubar.h"

MenuBar::MenuBar()
{
    m_menuBar = new menuBar();
    m_menuItemDefault= new menuItem();
    m_menuDefault= new menu();
    m_subMenu = new menu();
    m_nsubMenu = new menu();
    m_menuNivel = 0;
    m_item = nullptr;
}

menuBar *MenuBar::buildMenuBar()
{
    m_menuBar = Gtk::manage(new menuBar());
    return m_menuBar;
}

menu *MenuBar::addMenu(const string &p_label)
{
    m_menuItemDefault = Gtk::manage(new menuItem(p_label, true));
    m_menuBar->append(*m_menuItemDefault);
    m_menuDefault = Gtk::manage(new menu());
    m_menuItemDefault->set_submenu(*m_menuDefault);
    return m_menuDefault;
}

menuItem *MenuBar::addSubMenu(menu *p_menu, const string &p_label)
{
    m_menuItemDefault = Gtk::manage(new menuItem(p_label, true));
    menuList.push_back(m_menuItemDefault);
    p_menu->append(*(menuList.iterate()));
    return m_menuItemDefault;
}

menuItem *MenuBar::addNestedSubMenu(menuItem *p_Item, const string &p_label)
{
    m_menuItemDefault = Gtk::manage(new menuItem(p_label, true));
    menuList.push_back(m_menuItemDefault);
    if(m_item!=p_Item)
    {
        m_nsubMenu = new menu();
        p_Item->set_submenu(*m_nsubMenu);
        m_nsubMenu->append(*(menuList.iterate()));
        m_item=p_Item;
    }
    else if(m_item==p_Item && m_menuNivel!=1 )
    {
        p_Item->set_submenu(*m_subMenu);
        m_subMenu->append(*(menuList.iterate()));
        m_menuNivel++;
    }
    else
    {
        p_Item->set_submenu(*m_nsubMenu);
        m_nsubMenu->append(*(menuList.iterate()));
    }
    return m_menuItemDefault;
}

menuBar *MenuBar::getInstance()
{
    return m_menuBar;
}

MenuBar::~MenuBar()
{
    delete m_menuBar;
    delete m_menuItemDefault;
    delete m_menuDefault;
    delete m_subMenu;
    delete m_nsubMenu;
}
