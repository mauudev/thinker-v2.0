#include "PointDouble.h"

PointDouble::PointDouble()
:m_coorX{0},m_coorY{0}
{
}

PointDouble::PointDouble(double p_coorX, double p_coorY)
:m_coorX{p_coorX},m_coorY{p_coorY}
{
}

double PointDouble::getCoorX()
{
    return m_coorX;
}

double PointDouble::getCoorY()
{
    return m_coorY;
}

void  PointDouble::setCoorX(double p_coorX)
{
    m_coorX=p_coorX;
}

void  PointDouble::setCoorY(double p_coorY)
{
    m_coorY=p_coorY;
}
