#include "ToolData.h"

ToolData::ToolData(Glib::ustring p_toolId, Glib::ustring p_toolName,
    Glib::RefPtr<Gdk::Pixbuf> p_toolImage):
    m_toolName{p_toolName},
    m_toolId{p_toolId},
    m_toolImage{p_toolImage}
{

}

ToolData::~ToolData()
{
    delete this;
}

const Glib::ustring ToolData::getToolName()
{
    return m_toolName;
}

const Glib::ustring ToolData::getToolId()
{
    return m_toolId;
}

const Glib::RefPtr<Gdk::Pixbuf> ToolData::getToolImage()
{
    return m_toolImage;
}

void ToolData::setToolName(const Glib::ustring &p_toolName)
{
    m_toolName = p_toolName;
}

void ToolData::setToolId(const Glib::ustring &p_toolId)
{
    m_toolId = p_toolId;
}

void ToolData::setToolImage(const Glib::RefPtr<Gdk::Pixbuf> &p_toolImage)
{
    m_toolImage = p_toolImage;
}