#include "DataCollection.h"
#include <iostream>

DataCollection::DataCollection()
{
    m_dataCollection = new std::map<Glib::ustring,Glib::ustring>();
}

DataCollection::~DataCollection()
{
    delete m_dataCollection;
    delete this;
}

void DataCollection::insert(const Glib::ustring &p_id,
    const Glib::ustring &p_obj)
{
    m_dataCollection->insert (
        std::pair<Glib::ustring,Glib::ustring>(p_id, p_obj));
}

void DataCollection::remove(const Glib::ustring &p_id)
{
    try
    {
        m_dataCollection->erase(p_id);
    }
    catch(const std::exception &ex)
    {
        std::cout << "The element to remove doesn't exists: " 
            << ex.what() << "\n";
    }
}

const Glib::ustring DataCollection::find(const Glib::ustring &p_id)
{
    if(m_dataCollection->find(p_id)->second.empty()) 
    {
        return ""; 
    }
    else 
    {
        return m_dataCollection->find(p_id)->second;
    }
}

void DataCollection::clear()
{
    m_dataCollection->clear();
}

const size_t DataCollection::getSize() 
{
    return m_dataCollection->size();
}

std::map<Glib::ustring,Glib::ustring> *DataCollection::getDataCollection()
{
    return m_dataCollection;
}

void DataCollection::iterate()
{
    std::map<Glib::ustring,Glib::ustring>::iterator it;
    std::cout << "***********************" << "\n";
    for(it = m_dataCollection->begin(); it != m_dataCollection->end(); it++)
    {
        std::cout << "Key: " << it->first << " Value: " << it->second << "\n"; 
    }
    std::cout << "***********************" << "\n";
}