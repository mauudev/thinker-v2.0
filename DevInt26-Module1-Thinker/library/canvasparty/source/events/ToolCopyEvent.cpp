#include "ToolCopyEvent.h"
using callback = function<void (Tool*)>;

ToolCopyEvent::ToolCopyEvent()
{

}

ToolCopyEvent::~ToolCopyEvent()
{
	delete this;
}

std::vector<callback> &ToolCopyEvent::getCallbacksVector()
{
	return m_callbacksVector;
}

size_t ToolCopyEvent::getCallbacksSize()
{
	return m_callbacksVector.size();
}

void ToolCopyEvent::sayHello()
{
	std::cout << "Hello from event !" << "\n";
}
void ToolCopyEvent::trigger(Tool *tool)
{
    for(auto& p_dispatch_event : m_callbacksVector)
    {
        p_dispatch_event(tool);
    }
    m_callbacksVector.clear();
}