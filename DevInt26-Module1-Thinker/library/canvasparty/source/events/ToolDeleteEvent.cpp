#include "ToolDeleteEvent.h"
using callback = function<void (Tool*)>; 

ToolDeleteEvent::ToolDeleteEvent()
{

}

ToolDeleteEvent::~ToolDeleteEvent()
{
	delete this;
}

void ToolDeleteEvent::sayHello()
{
	std::cout << "Hello from delete event !" << "\n";
}

std::vector<callback> &ToolDeleteEvent::getCallbacksVector()
{
	return m_callbacksVector;
}

size_t ToolDeleteEvent::getCallbacksSize()
{
	return m_callbacksVector.size();
}

void ToolDeleteEvent::trigger(Tool *tool)
{
    for(auto& p_dispatch_event : m_callbacksVector)
    {
        p_dispatch_event(tool);
    }
}