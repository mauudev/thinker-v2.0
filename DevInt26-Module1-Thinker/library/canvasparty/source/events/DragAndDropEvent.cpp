#include "DragAndDropEvent.h"
#include <string>
using namespace std;
DragAndDropEvent::DragAndDropEvent()
{ 
}
DragAndDropEvent::~DragAndDropEvent()
{
}
void DragAndDropEvent::trigger(Tool *p_tool)
{
    for(auto& p_trigger : m_vectorFunction)
    {
        p_trigger(p_tool);
    }
}