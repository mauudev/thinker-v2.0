#ifndef DATA_COLLECTION_H
#define DATA_COLLECTION_H

#include <glibmm.h>
#include <gtkmm.h>
 
class DataCollection
{
protected:
    std::map<Glib::ustring,Glib::ustring> *m_dataCollection;

public:

    DataCollection();
    virtual ~DataCollection();
    void insert(const Glib::ustring &, const Glib::ustring &);
    void remove(const Glib::ustring &);
    const Glib::ustring find(const Glib::ustring &);
    void clear();
    const size_t getSize(); 
    std::map<Glib::ustring, Glib::ustring> *getDataCollection();

    void iterate(); 
};

#endif