#ifndef TOOL_DATA_H
#define TOOL_DATA_H

#include <gtkmm.h>
#include <glibmm.h>

class ToolData
{
    Glib::ustring m_toolId;
    Glib::ustring m_toolName;
    Glib::RefPtr<Gdk::Pixbuf> m_toolImage;

public:
    ToolData(Glib::ustring, Glib::ustring, Glib::RefPtr<Gdk::Pixbuf>);
    ~ToolData();
    const Glib::ustring getToolName();
    const Glib::ustring getToolId();
    const Glib::RefPtr<Gdk::Pixbuf> getToolImage();
    void setToolName(const Glib::ustring&);
    void setToolId(const Glib::ustring&);
    void setToolImage(const Glib::RefPtr<Gdk::Pixbuf>&);
};
#endif
