#ifndef TOOL_COPY_EVENT_H
#define TOOL_COPY_EVENT_H 
#include <vector>
#include <string>
#include <functional> 
#include <iostream>
#include <glibmm.h>

class Canvas;
class Tool;

using namespace std;

class ToolCopyEvent
{
    using callback = function<void (Tool*)>; // function lambda
    std::vector<callback> m_callbacksVector;    

public:
    ToolCopyEvent();
    ~ToolCopyEvent();

    std::vector<callback> &getCallbacksVector();
    void sayHello();
    size_t getCallbacksSize();
    template <typename FUNC>
    void addCallback(FUNC func)
    {
        m_callbacksVector.push_back(func);
    }
    void trigger(Tool*);
};
#endif 