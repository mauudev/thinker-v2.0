#ifndef DRAG_AND_DROP_EVENT_H
#define DRAG_AND_DROP_EVENT_H
#include <vector>
#include <string>
#include <functional>
#include "Tool.h"
using namespace std;
class DragAndDropEvent
{
protected:
    using Function = function<void(Tool *)>;
    std::vector<Function> m_vectorFunction;
public:
    DragAndDropEvent();
    ~DragAndDropEvent();
    template<typename FUNC>
    void addFunction(FUNC p)
    {
           m_vectorFunction.push_back(p);
    }
    void trigger(Tool *);

};

#endif