#ifndef TOOL_DELETE_EVENT_H
#define TOOL_DELETE_EVENT_H 
#include <vector>
#include <string>
#include <functional> 
#include <iostream>
#include <glibmm.h>

class Canvas;
class Tool;

using namespace std;

class ToolDeleteEvent
{
    using callback = function<void (Tool*)>; // function lambda
    std::vector<callback> m_callbacksVector;    
public:
    ToolDeleteEvent();
    ~ToolDeleteEvent();

    void sayHello();
    size_t getCallbacksSize();
    std::vector<callback> &getCallbacksVector();
    template <typename FUNC>
    void addCallback(FUNC func)
    {
        m_callbacksVector.push_back(func);
    }
    void trigger(Tool*);
};
#endif 