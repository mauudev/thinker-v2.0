#ifndef MESSAGE_TYPE_H
#define MESSAGE_TYPE_H
enum MessageTypeInfoBar 
{
    INFO, 
    WARNING, 
    QUESTION,
    ERROR,
    OTHER 
};
#endif
