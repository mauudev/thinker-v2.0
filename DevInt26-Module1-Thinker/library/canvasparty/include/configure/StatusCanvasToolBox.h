#ifndef STATUS_CANVAS_TOOL_BOX_H
#define STATUS_CANVAS_TOOL_BOX_H
enum StatusCanvasToolBox
{
    ACTION_TOOL_SELECT_AREA = 1,
    ACTION_TOOL_LINK = 2,
    ACTION_TOOL_DEFAULT = 3
};
#endif