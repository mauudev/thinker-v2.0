#ifndef RADIO_BUTTON_H
#define RADIO_BUTTON_H

#include <gtkmm/radiobutton.h>
#include <string>

class RadioButton: public Gtk::RadioButton
{
    std::string m_id;
public:
    RadioButton(const std::string &, const std::string &);
    ~RadioButton();
    template<typename...ARGS>
    void joinGroup(RadioButton &p_radioButton, ARGS&...p_args)
    {
        this->join_group(p_radioButton);
        joinGroup(p_args...);
    }
    void joinGroup(RadioButton &p_radioButton);
    bool getState();
    const std::string &getId() const;
    const std::string getDisplay() const;
    static Glib::RefPtr<RadioButton> create(const std::string &,
        const std::string &);
    RadioButton &getInstance();
};

#endif