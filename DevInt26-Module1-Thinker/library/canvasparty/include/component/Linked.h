#ifndef _LINKED_H
#define _LINKED_H

#include <cmath>
#include "PointDouble.h"
#include "Tool.h"
class Linked
{
private:
    PointDouble m_a;
    PointDouble m_b;
    PointDouble m_c;
    Tool* m_tool1;
    Tool* m_tool2;
    double m_widthArrow;
    double m_widthLine;
    char m_direccion;

public:
    Linked();
    ~Linked();
    Linked(Tool* p_tool1, Tool* p_tool2);
    
    void link(Tool* p_tool1,Tool* p_tool2);
    static double calculateDistance(PointDouble& p_a,PointDouble& p_b);

    Tool *getOriginTool();
    Tool *getDestinationTool();

    void calculateCoordinatesA();
    void calculateCoordinatesB();
    void calculateCoordinatesC();
    PointDouble* getPointA();
    PointDouble* getPointB();
    PointDouble* getPointC();
};

#endif