#ifndef ACTION_TOOL_TOGGLE_H
#define ACTION_TOOL_TOGGLE_H
#include <glibmm.h>
#include <gtkmm/togglebutton.h>
#include <gtkmm/image.h>
#include <string>

class ActionToolToggle:public Gtk::ToggleButton
{
private:
    Gtk::Image m_img;

public:
    ActionToolToggle();
    ActionToolToggle(const std::string& p_file);
    bool isActive();
    void deactivate();
    void activate();
    void enable();
    void disable();
};

#endif