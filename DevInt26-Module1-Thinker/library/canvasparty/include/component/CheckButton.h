#ifndef CHECK_BUTTON_H
#define CHECK_BUTTON_H

#include <gtkmm/checkbutton.h>
#include <string>

class CheckButton: public Gtk::CheckButton
{
    std::string m_id;
public:
    CheckButton(const std::string &, const std::string &);
    ~CheckButton();
    bool getState();
    const std::string &getId() const;
    const std::string getDisplay() const;
    static Glib::RefPtr<CheckButton> create(const std::string &,
        const std::string &);
    CheckButton &getInstance();
};

#endif