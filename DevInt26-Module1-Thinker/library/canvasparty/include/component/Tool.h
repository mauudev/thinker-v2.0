#ifndef GTKMM_TOOL_H
#define GTKMM_TOOL_H

#include <gtkmm.h>
#include "ToolData.h"
#include "Point.h"
#include "ToolDeleteEvent.h"
#include "ToolCopyEvent.h"
#include <string>

class Tool
{

public:
    Glib::RefPtr<Gdk::Pixbuf> m_pixbuf;
    int m_x;
    int m_y;
    ToolData *m_toolData;
    ToolDeleteEvent *m_toolDeleteEvent;
    ToolCopyEvent *m_toolCopyEvent;
    int m_width;
    int m_heigth;

    //**************************
    Point m_coordinates;
    bool m_flagSelected;
    bool m_flagMoving;
    bool m_isLinked;
    //***********************
    bool m_destroy = false;

    Tool(Gtk::Widget *p_canvas, 
        ToolData *p_toolData,
        double p_x,
        double p_y);

    Glib::ustring getToolName();

    Glib::ustring getToolId();
    
    Glib::RefPtr<Gdk::Pixbuf> getToolImage();
    
    ToolData *getToolData();

    ToolDeleteEvent* getToolDeleteEvent();

    ToolCopyEvent* getToolCopyEvent();

    void setToolCopyEvent(ToolCopyEvent*);
    
    int getAxisX();

    int getAxisY();


    int getWidth();

    int getHeigth();

    bool isSelectedForEvent();


    bool pointIsInside(Point p_point);
    Point& getPoint();
    bool isSelected();
    bool isMoving();
    bool isLinked();
    void activateFlagSelect();
    void deactivateFlagSelect();
    void deactivateFlagMoving();
    void activateFlagMoving();
    void setLink(bool);
    std::string getId();
    void destroy();
    bool getDestroy();
};
#endif