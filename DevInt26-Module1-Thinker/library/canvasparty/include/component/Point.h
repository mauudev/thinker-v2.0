#ifndef _POINT_H
#define _POINT_H

class Point
{
private:
    int m_coorX,m_coorY;
public:
    Point();
    Point(int coorX,int coorY);
    int getCoorX();
    int getCoorY();
    void setCoorX(int p_coorX);
    void setCoorY(int p_coorY);
};

#endif