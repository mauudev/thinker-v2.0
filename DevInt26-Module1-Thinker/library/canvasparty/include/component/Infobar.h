#ifndef INFOBAR_H
#define INFOBAR_H
#include <gtkmm/infobar.h>
#include <gtkmm/label.h>
#include <string>
#include "MessageType.h"

class InfoBar
{
private:
    Gtk::InfoBar m_infoBar;
    Gtk::Label m_messageLabel;
public:
    InfoBar();
    virtual ~InfoBar();
    void onInfoBarResponse(int);
    Gtk::InfoBar &getInstance();
    void setMessage(const std::string &, MessageTypeInfoBar);
    void setMessageInfo(const std::string &);
    void setMessageOther(const std::string &);
    void setMessageWarning(const std::string &);
    void setMessageError(const std::string &);
    void setMessageQuestion(const std::string &);
};

#endif