#ifndef CHECK_BOX_H
#define CHECK_BOX_H

#include <gtkmm/buttonbox.h>
#include "CheckButton.h"
#include "GenericInput.h"

class CheckBox: public GenericInput, public Gtk::ButtonBox
{
    std::string m_id;
    std::string m_display;
public:
    CheckBox(const std::string &, const std::string &);
    CheckBox(const std::string &, const std::string &,
        const Gtk::Orientation &);
    ~CheckBox();
    template<typename...ARGS>
    void joinInBox(const Gtk::PackOptions &p_option,
        const int p_padding, CheckButton &p_child, ARGS&...args)
    {
        this->pack_start(p_child, p_option, p_padding);
        joinInBox(p_option, p_padding, args...);
    }
    template<typename...ARGS>
    void joinInBox(CheckButton &p_child, ARGS&...args)
    {
        this->pack_start(p_child, Gtk::PACK_SHRINK, 5);
        joinInBox(Gtk::PACK_SHRINK, 5, args...);
    }
    void joinInBox(const Gtk::PackOptions &, const int, CheckButton &);
    std::vector<CheckButton *> getSelected();
    const std::string &getId() const;
    const std::string &getDisplay() const;
    std::string getValue();
    Gtk::ButtonBox &get();
};

#endif