#ifndef TOOLDEFINITION_H
#define TOOLDEFINITION_H

#include <string>
#include <gtkmm.h>
#include "DragAndDropEvent.h"

class ToolDefinition : public Gtk::ToolButton
{
    std::string m_toolName;
    std::string m_imageName;
    std::string m_tooltip;
    std::string m_filepath;
    std::string m_toolId;
    Gtk::Image m_image;
    Glib::RefPtr<Gdk::Pixbuf> m_imageResource;
    Gtk::Menu m_menuPopup;
    int m_imageHeight;
    int m_imageWidth;
    DragAndDropEvent *m_dragAndDropEvent;


public:
    ToolDefinition(std::string, std::string, std::string, std::string);
    ToolDefinition(std::string, std::string, std::string, std::string, 
        int, int);
    virtual ~ToolDefinition();

    std::string getToolName()const;
    std::string getImageName()const;
    std::string getFilepath()const;
    std::string getToolTip()const;
    std::string getToolId()const;
    int getImageWidth()const;
    int getimageHight()const;
    void setToolId(const std::string);
    void setImageWidth(const int);
    void setImageHeight(const int);
    void setToolName(const std::string);
    Glib::RefPtr<Gdk::Pixbuf> getImage(); 
    void setImage(std::string, int, int);
    DragAndDropEvent *getDragAndDropEvent();
};
#endif
