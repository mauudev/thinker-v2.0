#ifndef TOGGLE_BUTTON_H
#define TOGGLE_BUTTON_H

#include <gtkmm/togglebutton.h>
#include <string>

class ToggleButton
{
    Gtk::ToggleButton m_toggleButton;
public:
    ToggleButton(const std::string &);
    ~ToggleButton();
    void onToggleButtonClicked() const;
    Gtk::ToggleButton &getInstance();
};

#endif