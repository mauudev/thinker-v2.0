#ifndef GENERICINPUT_H
#define GENERICINPUT_H

#include <gtkmm.h>
#include <string>

class GenericInput
{
public:
    GenericInput();
    virtual ~GenericInput();
    virtual const std::string &getId() const;
    virtual const std::string &getDisplay() const;
    virtual std::string getValue();
    virtual Gtk::Widget &get() = 0;
};

#endif