#ifndef GTK_TOOLDEFINITIONGROUP_H
#define GTK_TOOLDEFINITIONGROUP_H

#include <glibmm.h>
#include "ToolDefinition.h"
#include <gtkmm/toolitemgroup.h>
#include <string>
#include <map>

class ToolDefinitionGroup : public Gtk::ToolItemGroup
{
    std::map<std::string, ToolDefinition*> *m_mapToolDefinition;
public:
    ToolDefinitionGroup(const std::string &p_nameToolDefinitionGroup);
    ~ToolDefinitionGroup();
    ToolDefinition *addToolDefinition(const std::string &p_toolKey, 
        const std::string &p_toolName, const std::string &p_imageName,
        const std::string &p_tooltip);
};

#endif