#ifndef GTKMM_TEXTLABEL_H
#define GTKMM_TEXTLABEL_H

#include <gtkmm/entry.h>
#include <string>
#include "GenericInput.h"


class TextLabel : public GenericInput, public Gtk::Entry
{
private:
    std::string m_id;
    std::string m_display;
public:
    TextLabel(const std::string &, const std::string &);
    ~TextLabel();
    const std::string &getId()const;
    const std::string &getDisplay()const;
    std::string getValue();
    Gtk::Entry &get();
};

#endif