#ifndef RADIO_BOX_H
#define RADIO_BOX_H

#include <gtkmm/buttonbox.h>
#include "RadioButton.h"
#include "GenericInput.h"

class RadioBox: public GenericInput, public Gtk::ButtonBox
{
    std::string m_id;
    std::string m_display;
public:
    RadioBox(const std::string &, const std::string &);
    RadioBox(const std::string &, const std::string &,
        const Gtk::Orientation &);
    ~RadioBox();
    template<typename...ARGS>
    void joinInBox(const Gtk::PackOptions &p_option,
        const int p_padding, RadioButton &p_child, ARGS&...args)
    {
        this->pack_start(p_child, p_option, p_padding);
        joinInBox(p_option, p_padding, args...);
        joinInGroup(p_child, args...);
    }
    template<typename...ARGS>
    void joinInBox(RadioButton &p_child, ARGS&...args)
    {
        this->pack_start(p_child, Gtk::PACK_SHRINK, 5);
        joinInBox(Gtk::PACK_SHRINK, 5, args...);
        joinInGroup(p_child, args...);
    }
    void joinInBox(const Gtk::PackOptions &, const int, RadioButton &);
    template<typename...ARGS>
    void joinInGroup(RadioButton &p_radio, ARGS&...args)
    {
        p_radio.joinGroup(args...);
        joinInGroup(args...);
    }
    void joinInGroup(RadioButton &);
    RadioButton *getSelected();
    const std::string &getId() const;
    const std::string &getDisplay() const;
    std::string getValue();
    Gtk::ButtonBox &get();
};

#endif