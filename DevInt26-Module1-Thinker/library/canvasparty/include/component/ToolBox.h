#ifndef GTKMM_TOOLBOX_H
#define GTKMM_TOOLBOX_H
#include <iostream>
#include <string>
#include <map>
#include "ToolDefinitionGroup.h"
#include <gtkmm/toolpalette.h>

class ToolBox 
{
private:
    std::map<std::string, ToolDefinitionGroup*> *m_mapToolDefinitionGroup;
    Gtk::ToolPalette m_toolBox;
public:
    ToolBox();
    ~ToolBox();
    ToolDefinitionGroup *addToolDefinitionGroup(const std::string& p_groupKey, 
        const std::string& p_groupName);
    Gtk::ToolPalette &getInstance();
};
#endif