#ifndef GTKMM_TEXTAREA_H
#define GTKMM_TEXTAREA_H

#include <gtkmm/box.h>
#include <gtkmm/scrolledwindow.h>
#include <gtkmm/textview.h>
#include <gtkmm/textbuffer.h>
#include <string>
#include "GenericInput.h"


class TextArea : public GenericInput, public Gtk::Box
{
private:
    std::string m_id;
    std::string m_display;
    Gtk::ScrolledWindow m_scrolledWindow;
    Gtk::TextView m_textView;
    Glib::RefPtr<Gtk::TextBuffer> m_refTextBuffer;

public:
    TextArea(const std::string &,const std::string &);
    TextArea(const std::string &, const std::string &, Gtk::Orientation &);
    virtual ~TextArea();
    const std::string &getId() const;
    const std::string &getDisplay() const;
    std::string getValue();
    Gtk::Box &get();
};

#endif