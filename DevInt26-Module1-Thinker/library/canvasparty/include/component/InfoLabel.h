#ifndef GTKMM_INFOLABEL_H
#define GTKMM_INFOLABEL_H

#include <gtkmm/entry.h>
#include <string>
#include "GenericInput.h"


class InfoLabel : public Gtk::Label
{
private:
    std::string m_message;
public:
    InfoLabel(const std::string &);
    ~InfoLabel();
    const std::string getId() const;
    const std::string getDisplay() const;
    std::string getValue();
    Gtk::Label &get();

};

#endif
