#ifndef _POINT_DOUBLE_H
#define _POINT_DOUBLE_H

class PointDouble
{
private:
    double m_coorX;
    double m_coorY;
public:
    PointDouble();
    PointDouble(double p_coorX,double p_coorY);
    double getCoorX();
    double getCoorY();
    void setCoorX(double p_coorX);
    void setCoorY(double p_coorY);
};

#endif