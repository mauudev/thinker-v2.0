#ifndef LINK_BUTTON_H
#define LINK_BUTTON_H

#include <gtkmm/linkbutton.h>
#include <string>

class LinkButton
{
    Gtk::LinkButton m_linkButton;
public:
    LinkButton(const std::string &, const std::string &);
    ~LinkButton();
    void onLinkButtonClicked() const;
    Gtk::LinkButton &getInstance();
};

#endif