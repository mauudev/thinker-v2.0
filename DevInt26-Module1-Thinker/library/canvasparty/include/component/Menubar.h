#ifndef MENUBAR_H
#define MENUBAR_H
#include <gtkmm.h>
#define menuBar Gtk::MenuBar
#define menuItem Gtk::MenuItem
#define menu Gtk::Menu
#include <iostream>
#include <string>
#include <functional>
using namespace std;

template<typename U>
struct Node
{
    U m_item;
    Node<U> *m_next;
};

template<typename T>
class linkedMenuItemList
{
    Node<T> *m_first;
    Node<T> *m_last;
public:
    linkedMenuItemList()
    : m_first{nullptr}, m_last{nullptr}
    {
    }
    void push_back(const T &obj)
    {
        Node<T> *nn = new Node<T>{obj,nullptr};
        if(m_first == nullptr)
        {
            m_first = m_last = nn;
            return;
        }
        m_last->m_next = nn;
        m_last = nn;
    }
    T iterate()
    {
        T value;
        Node<T> *aux = m_first;
        while (aux)
        {
            value = aux->m_item;
            aux = aux->m_next;
        }
        return value;
    }
    ~linkedMenuItemList()
    {
        Node<T> *aux = m_first;
        while(aux)
        {
            Node<T> *next = aux->m_next;
            delete aux;
            aux = next;
        }
    }
};

class MenuBar :public Gtk::MenuBar
{
private:
    linkedMenuItemList<Gtk::MenuItem *> menuList;
protected:
    menuBar *m_menuBar;
    menuItem *m_menuItemDefault;
    menu *m_menuDefault;
    menu *m_subMenu;
    menu *m_nsubMenu;
    menu *m_nnsubMenu;
    menuItem *m_item;
    int m_menuNivel;
public:
    using Function = function<void()>;
    MenuBar();
    ~MenuBar();
    menuBar *buildMenuBar();
    menu *addMenu(const string &);
    menuItem *addSubMenu(menu*, const string &);
    menuItem *addNestedSubMenu(menuItem*, const string &);
    menuBar *getInstance();
    // void addEvent(menuItem *p_item, Function &p);
    template<typename FUNC>
    void addEvent(menuItem *p_item, FUNC p)
    {
        p_item->signal_activate().connect(p);
    }
};
#endif
