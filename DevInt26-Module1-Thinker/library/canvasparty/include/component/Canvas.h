#ifndef GTKMM_EXAMPLE_CANVAS_H
#define GTKMM_EXAMPLE_CANVAS_H

#include <gtkmm.h>
#include <gdkmm/pixbuf.h>
#include <functional>
#include "Tool.h" 
#include "DataCollection.h"
#include "ToolDefinition.h"
#include "Linked.h"
#include "Point.h"
#include "CanvasToggleToolBox.h"

class Canvas : public Gtk::DrawingArea
{
private:
    typedef std::vector<Tool*> typeVecItems;
    typedef std::vector<Linked*> typeVecLinks;;
    bool m_flag;
    bool m_dragDataRequestedForDrop;
    bool m_isClickReleasedForInsert;
    int m_idToolCount;
    Tool *m_dropItem;
    ToolDefinition *m_toolDefinition;
    DataCollection *m_dataCollection;
    typeVecItems m_canvasTool;
    typeVecLinks m_links;

    //select
    int m_focusX;
    int m_focusY;

    //select area
    Point m_areaStart;
    Point m_areaEnd;
    bool m_flagArea;
    int m_initXArea;
    int m_initYArea;
    int m_widthArea;
    int m_heigthArea;

    //link
    bool m_flagLink;
    Point m_linkStart;
    Point m_linkEnd;
    double m_widthLink;
    Tool *m_tool1;
    Tool *m_tool2;

    Point m_pointLink;
    Linked *m_link;
    CanvasToggleToolBox *m_canvasToggleToolBox;


public:
    Canvas();

    virtual ~Canvas();

    size_t getCanvasToolSize();
    size_t getCanvasLinkedSize();
    size_t getIdToolCount();
    std::vector<Tool*> *getToolVector();
    DataCollection *getDataCollection();
    bool onDeleteKeyPress(GdkEventKey*);
    Linked *getToolLink(const Glib::ustring&);
    void removeLink(const Glib::ustring&);
    void queueDraw();
    void setIdToolCount(const size_t&);

    Tool* getSelectedTool(int,int);
    void deleteToolFromCanvas(Tool*);
    void copyToolFromCanvas(Tool*);
    void itemDraw(const Tool *p_item,
        const Cairo::RefPtr<Cairo::Context> &p_cairo,
        bool p_preview);

    bool on_draw(const Cairo::RefPtr<Cairo::Context> &p_cairo) override;

    void on_drag_data_received(const Glib::RefPtr<Gdk::DragContext> &p_context,
        int p_x,
        int p_y,
        const Gtk::SelectionData &p_selectionData,
        guint p_info,
        guint p_time) override;

    bool on_drag_motion(const Glib::RefPtr<Gdk::DragContext> &p_context, 
        int p_x,
        int p_y,
        guint p_time) override;

    bool on_drag_drop(const Glib::RefPtr<Gdk::DragContext> &p_context,
        int p_x,
        int p_y,
        guint p_time) override;

    bool on_button_press_event(GdkEventButton *p_event);
    bool on_button_release_event(GdkEventButton *p_event);
    bool on_motion_notify_event(GdkEventMotion*p_event);

    void connectWithActionBar(CanvasToggleToolBox* p_canvasToggleToolBox);

    void functionSelect(int p_coorX,int p_coorY);
    void functionDelete(int p_coorX,int p_coorY);
    void functionSelectArea(int p_coorX,int p_coorY);
    void functionLink(int p_coorX,int p_coorY);

    void functionMovingCursorSelect(int p_coorX,int p_coorY);
    void functionMovingCursorDelete(int p_coorX,int p_coorY);
    void functionMovingCursorSelectArea(int p_coorX,int p_coorY);
    void functionMovingCursorLink(int p_coorX,int p_coorY);

    void functionReleaseClickSelect();
    void functionReleaseClickSelectArea();
    void functionReleaseClickLink(int p_coorX,int p_coorY);

    void drawRoundedPath(const Cairo::RefPtr<Cairo::Context>& p_cairo, 
        double p_x,
        double p_y,
        double p_width,
        double p_height,
        double p_radius);
    void drawLink(const Cairo::RefPtr<Cairo::Context>& p_cairo);
    void drawSelectArea(int p_initXArea,
        int p_initYArea,
        int p_widthArea,
        int p_heithArea);

    void functionClear();
    void sendFunctionClear();
};

#endif