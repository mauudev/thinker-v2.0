#ifndef GTKMM_INTEGERLABEL_H
#define GTKMM_INTEGERLABEL_H

#include <gtkmm.h>
#include <string>
#include "GenericInput.h"

class IntegerLabel : public GenericInput, public Gtk::SpinButton
{
private:
    std::string m_id;
    std::string m_display;
public:

    IntegerLabel(const std::string &, const std::string &);
    ~IntegerLabel();
    const std::string &getId() const;
    const std::string &getDisplay() const;
    std::string getValue();
    Gtk::SpinButton &get();
};

#endif
