#ifndef NEW_WINDOW_H
#define NEW_WINDOW_H
#include <gtkmm.h>
#include <string>
#include "GenericInput.h"
class NewForm : public Gtk::Window
{
private:
    Gtk::Button m_buttonCancel;
    Gtk::Button m_buttonAccept; 
    Gtk::Box m_boxWindow;
    Gtk::Box m_boxComponents;
    Gtk::Box m_boxButtons;
    std::vector<Gtk::Box *> m_boxComponent;
    std::vector<Gtk::Label *> m_label;
    Gtk::ScrolledWindow m_scrolledWindow;
public:
    NewForm(const std::string &);
    virtual ~NewForm();
    void buttonsEvents();
    void onButtonCancelClicked();
    void onButtonOkClicked();
    void addGenericInput(GenericInput &);
    NewForm &getInstance();
    void builder();
};
#endif
