#ifndef GTKMM_WINDOWUI_H
#define GTKMM_WINDOWUI_H

#include <gtkmm.h>
#include "CanvasToggleToolBox.h"
#include "CanvasButtonToolBox.h"
#include "Menubar.h"
#include "Infobar.h"
#include "Buttonbox.h"
#include "Linkbutton.h"
#include "Togglebutton.h"
#include "MainWindowLayout.h"
#include "ToolBox.h"
#include "ToolDefinition.h"
#include "ToolDefinitionGroup.h"
#include "Canvas.h"

class WindowUI : public Gtk::Window
{
private:
    MainWindowLayout *m_layout;
public:
    WindowUI();
    WindowUI(ToolBox &);
    virtual ~WindowUI();
    void addLayout(MainWindowLayout *p_layout);
};

#endif
