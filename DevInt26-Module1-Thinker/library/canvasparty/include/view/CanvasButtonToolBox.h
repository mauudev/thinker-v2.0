#ifndef CANVAS_BUTTON_TOOL_BOX_H
#define CANVAS_BUTTON_TOOL_BOX_H
#include <gtkmm/box.h>
#include "ActionToolButton.h"
#include <iostream>
#include <functional>
#include "Canvas.h"

class CanvasButtonToolBox
{
private:
    Gtk::Box m_layout;
    ActionToolButton m_actionToolCopy;
    ActionToolButton m_actionToolDelete;
    ActionToolButton m_actionToolClear;
    Canvas *m_canvas;

public:
    CanvasButtonToolBox();
    ~CanvasButtonToolBox();
    Gtk::Box &getInstance();

    void eventsButton();

    void actionCanvasDelete();
    void actionCanvasCopy();
    void actionCanvasClear();
    void connectWithCanvas(Canvas *);
};

#endif