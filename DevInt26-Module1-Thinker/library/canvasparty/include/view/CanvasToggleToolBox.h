#ifndef CANVAS_TOGGLE_TOOL_BOX_H
#define CANVAS_TOGGLE_TOOL_BOX_H
#include <gtkmm/box.h>
#include "ActionToolToggle.h"
#include "StatusCanvasToolBox.h"
#include <iostream>
#include <functional>

class CanvasToggleToolBox
{
public:
    // using func = function<void()>;
private:
    Gtk::Box m_layout;
    ActionToolToggle m_actionToolSelectArea;
    ActionToolToggle m_actionToolLink;

public:
    CanvasToggleToolBox();
    ~CanvasToggleToolBox();
    Gtk::Box &getInstance();

    void eventsToggle();
    void mutuallyExclusiveSelectArea();
    void mutuallyExclusiveLink();
    StatusCanvasToolBox whichIsActive();

};

#endif