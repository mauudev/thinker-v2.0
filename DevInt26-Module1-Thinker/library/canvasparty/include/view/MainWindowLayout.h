#ifndef GTKMM_COMPONENT_WINDOW_LAYOUT_H
#define GTKMM_COMPONENT_WINDOW_LAYOUT_H
#include <gtkmm.h>
#include <map>
#include "Canvas.h"
#include "CanvasToggleToolBox.h"
#include "CanvasButtonToolBox.h"
#include "Infobar.h"
#include "Menubar.h"
#include "ToolBox.h"
#include "MessageType.h"

class MainWindowLayout
{
private:

    Canvas *m_canvas;
    CanvasToggleToolBox *m_canvasToggleToolBox;
    CanvasButtonToolBox *m_canvasButtonToolBox;
    InfoBar *m_infobar;
    MenuBar *m_menubar;
    ToolBox *m_toolBox;

public:
    Gtk::Box m_verticalLayout;
    Gtk::Box m_horizontalLayout;

    MainWindowLayout();
    ~MainWindowLayout();
    Canvas *getCanvas();
    CanvasToggleToolBox *getCanvasToggleToolBox();
    CanvasButtonToolBox *getCanvasButtonToolBox();
    InfoBar *getInfobar();
    MenuBar *getMenubar();
    ToolBox *getToolBox();
    Gtk::Box &getInstance();
    void build();
    void connect();
};
#endif