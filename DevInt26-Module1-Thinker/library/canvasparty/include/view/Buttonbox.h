#ifndef GTKMM_BUTTONBOX_H
#define GTKMM_BUTTONBOX_H

#include <gtkmm/buttonbox.h>

class ButtonBox
{
    Gtk::ButtonBox m_buttonBox;
public:
    ButtonBox(bool, gint, Gtk::ButtonBoxStyle);
    Gtk::ButtonBox &getInstance();
    void packStart(Gtk::Widget &, const Gtk::PackOptions &, int);
};

#endif