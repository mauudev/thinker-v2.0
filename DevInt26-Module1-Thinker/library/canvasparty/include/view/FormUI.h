#ifndef FORM_UI_H
#define FORM_UI_H
#include <gtkmm.h>
#include <string>
#include <iostream>
#include "FormLayout.h"
#include "Input.h"

class FormUI
{
private:
    Gtk::Separator m_separator;
    Gtk::Button m_buttonCancel;
    Gtk::Button m_buttonSubmit;
    Gtk::Box m_box;
    Gtk::Box m_boxButtons;
    Gtk::Window m_window;
    Gtk::ScrolledWindow m_scroll;
    FormLayout m_layout;
    Glib::ustring m_windowName;

public:
    FormUI(const Glib::ustring &p_nameForm);
    ~FormUI();
    void loadListeners();
    void onCancelEvent();
    void onSubmitEvent();
    void addInput(Input &p_input);
    void build();
    Gtk::Window &getInstance();

};
#endif
