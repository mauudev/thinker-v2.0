#ifndef FORM_LAYOUT_H
#define FORM_LAYOUT_H

#include <gtkmm.h>
#include <vector>
#include "Input.h"

class FormLayout : public Gtk::Box
{
private:
    std::vector<Input*> items;
public:
    FormLayout();
    void build();
    void attachItem(Input &p_input);
};

#endif