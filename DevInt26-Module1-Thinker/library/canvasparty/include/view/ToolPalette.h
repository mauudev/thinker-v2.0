#ifndef GTKMM_TOOLPALETTE_H
#define GTKMM_TOOLPALETTE_H
#include <gtkmm.h>

class ToolPalette
{
private:
    Gtk::ToolPalette m_toolPalette;

public:
    ToolPalette();
    ~ToolPalette();
    void loadItems();
    Gtk::ToolPalette &getInstance();
};

#endif