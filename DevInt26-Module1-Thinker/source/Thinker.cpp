#include "Thinker.h"

Thinker::Thinker()
:m_projectToolbox{make_unique<ProjectToolbox>()},
    m_agentToolbox{make_unique<AgentToolbox>()},
    m_project{make_unique<Project>()}
{
}

Thinker::~Thinker()
{
}

Thinker &Thinker::getInstance()
{
    static Thinker instance;
    return instance;
}

Project &Thinker::newProject()
{
    m_project.reset();
    m_project = make_unique<Project>();
    return *m_project;
}

ProjectToolbox &Thinker::getProjectToolbox() const
{
    return *m_projectToolbox;
}

AgentToolbox &Thinker::getAgentToolbox() const
{
    return *m_agentToolbox;
}

Project &Thinker::getProject() const
{
    return *m_project;
}