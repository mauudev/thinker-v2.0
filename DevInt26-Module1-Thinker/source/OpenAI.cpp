#include "OpenAI.h"
#include "ExceptionNode.h"

const int DEFAULT_NUMBER_COINS = 3;

OpenAI::OpenAI(const string &p_openAIName)
:m_openAIName{p_openAIName}, m_numberCoins{DEFAULT_NUMBER_COINS}
{
    gameInitializer();
}

vector<string> OpenAI::getVectorGame()
{
    return m_gamesOfOpenAI;
}

ToolType OpenAI::getClassName() const
{
    return e_openAI;
}

const string OpenAI::getIToolName() const
{
    return m_openAIName;
}

void OpenAI::link(shared_ptr<ITool> p_link)
{
    switch(p_link->getClassName())
    {
        case e_screenHandler: m_screenHandler = 
            static_pointer_cast<ScreenHandler>(p_link);break;
        case e_keyboardHandler: m_aStep = 
            static_pointer_cast<KeyboardHandler>(p_link); break;
        case e_tensorFlow: m_aStep = 
            static_pointer_cast<TensorFlow>(p_link); break;
        case e_randomHandler: m_aStep = 
            static_pointer_cast<RandomHandler>(p_link); break;
        default: throw ExceptionNode{"The tool don't find."};
    }
}

Json OpenAI::serialize()
{
    Json jsonAnswer;
    jsonAnswer["openAIName"] = m_openAIName;
    size_t linkCount = 0;
    if(m_screenHandler.lock() != nullptr)
    {
        jsonAnswer["Links"][linkCount++] = m_screenHandler.lock()->getIToolName();
    }

    if(m_aStep.lock() != nullptr)
    {
        jsonAnswer["Links"][linkCount++] = m_aStep.lock()->getIToolName();
    }

    return jsonAnswer;
}

void OpenAI::deserialize(Json &p_json)
{
    m_openAIName = p_json["openAIName"];
    
}


Statement &OpenAI::generateASTInitNode(BuilderCodePy &py)
{
    return
    py.functionDef("__init__")
        .addParameter(&py.name("self"))
        .addBodyNode(&initOpenAINameASTNode(py))
        .addBodyNode(&initNumberCoinsOpenAIASTNode(py))
        .addBodyNode(&initCounterGamesOpenAIASTNode(py))
        .addBodyNode(&initDoneOpenAIASTNode(py))
        .addBodyNode(&initRGBArrayOpenAIASTNode(py))
        .addBodyNode(&initRewardOpenAIASTNode(py))
        .addBodyNode(&initObservationsOpenAIASTNode(py))
        .addBodyNode(&initEnvironmentOpenAIASTNode(py))
        .addBodyNode(&initEnvironmentActionSpaceOpenAIASTNode(py))
        .addBodyNode(&initResetEnvironmentOpenAIASTNode(py));
}

Statement &OpenAI::generateASTBodyNode(BuilderCodePy &py)
{
    return
    py.concat()
        .addBodyNode(&getStatusOpenAIASTNode(py))
        .addBodyNode(&getRGBArrayASTNode(py))
        .addBodyNode(&resetEnvironmentASTNode(py));
}

Statement &OpenAI::generateASTClassNode(BuilderCodePy &py)
{
    return
    py.concat()
        .addBodyNode(&py.import("gym.spaces"))
        .addBodyNode(
            &py.classDef("OpenAI")
                .addParameter(&py.name("object"))
                .addBodyNode(&generateASTInitNode(py))
                .addBodyNode(&generateASTBodyNode(py))
        )
        .addBodyNode(&generateLinksASTNodes(py));
}

Statement &OpenAI::generateLinksASTNodes(BuilderCodePy &py)
{
    return
    py.concat()
        .addBodyNode(&generateAStepASTNode(py))
        .addBodyNode(&generateScreenASTNode(py));
}

Statement &OpenAI::generateAStepASTNode(BuilderCodePy &py)
{
    if(m_aStep.expired() == false)
    {
        return
        py.concat()
            .addBodyNode(&(m_aStep.lock()->generateASTClassNode(py)));
    }
    else
    {
        throw ExceptionNode{"AStep not found"};
    }
}

Statement &OpenAI::generateScreenASTNode(BuilderCodePy &py)
{
    if(m_screenHandler.expired() == false)
    {
        return
        py.concat()
            .addBodyNode(&(m_screenHandler.lock()->generateASTClassNode(py)));
    }
    else
    {
        throw ExceptionNode{"ScreenHandler not found"};
    }
}

Assignment &OpenAI::initOpenAINameASTNode(BuilderCodePy &py)
{
    return
    py.assign(
        &py.attribute(&py.name("self"), "openAIName"),
        &py.name("\"" + m_openAIName + "\"")
    );
}

Assignment &OpenAI::initNumberCoinsOpenAIASTNode(BuilderCodePy &py)
{
    return
    py.assign(
        &py.attribute(&py.name("self"), "numberCoins"),
        &py.num(m_numberCoins)
    );
}

Assignment &OpenAI::initCounterGamesOpenAIASTNode(BuilderCodePy &py)
{
    return
    py.assign(
        &py.attribute(&py.name("self"), "counterGame"),
        &py.num(1)
    );
}

Assignment &OpenAI::initDoneOpenAIASTNode(BuilderCodePy &py)
{
    return
    py.assign(
        &py.attribute(&py.name("self"), "done"),
        &py.name("False")
    );
}

Assignment &OpenAI::initRGBArrayOpenAIASTNode(BuilderCodePy &py)
{
    return
    py.assign(
        &py.attribute(&py.name("self"), "rgbArray"),
        &py.num(0)
    );
}

Assignment &OpenAI::initRewardOpenAIASTNode(BuilderCodePy &py)
{
    return
    py.assign(
        &py.attribute(&py.name("self"), "reward"),
        &py.num(0)
    );
}

Assignment &OpenAI::initObservationsOpenAIASTNode(BuilderCodePy &py)
{
    return
    py.assign(
        &py.attribute(&py.name("self"), "observations"),
        &py.num(0)
    );
}

Assignment &OpenAI::initEnvironmentOpenAIASTNode(BuilderCodePy &py)
{
    return
    py.assign(
        &py.attribute(&py.name("self"), "environment"),
        &py.call(&py.attribute(&py.name("gym"), "make"))
            .addArgument(&py.attribute(&py.name("self"), "openAIName"))
    );
}

Assignment &OpenAI::initEnvironmentActionSpaceOpenAIASTNode(BuilderCodePy &py)
{
    return
    py.assign(
        &py.attribute(&py.name("self"), "environmentActionSpace"),
        &py.attribute(
            &py.attribute(&py.name("self"), "environment"), "action_space"
        )
    );
}

CallFunction &OpenAI::initResetEnvironmentOpenAIASTNode(BuilderCodePy &py)
{
    return
    py.call(
        &py.attribute(
            &py.attribute(&py.name("self"), "environment"), "reset"
        )
    );
}

FunctionDef &OpenAI::getStatusOpenAIASTNode(BuilderCodePy &py)
{
    return
    py.functionDef("getStatusOpenAI")
        .addParameter(&py.name("self"))
        .addBodyNode(
            &py.ifStmt(&py.attribute(&py.name("self"), "done"))
                .addIfNode(&buildIfCounterGamesStatusASTNode(py))
        )
        .addBodyNode(
            &py.returnStmt(&py.attribute(&py.name("self"), "done"))
        );
}

If &OpenAI::buildIfCounterGamesStatusASTNode(BuilderCodePy &py)
{
    return
    py.ifStmt(
        &py.compare(
            &py.attribute(&py.name("self"), "counterGame"),
            Lt,
            &py.attribute(&py.name("self"), "numberCoins")
        )
    )
        .addIfNode(&assignDoneASTNode(py))
        .addIfNode(&incrementCounterGamesASTNode(py))
        .addIfNode(&initResetEnvironmentOpenAIASTNode(py));
}

Assignment &OpenAI::assignDoneASTNode(BuilderCodePy &py)
{
    return
    py.assign(
        &py.attribute(&py.name("self"), "done"),
        &py.name("False")
    );
}

AugAssign &OpenAI::incrementCounterGamesASTNode(BuilderCodePy &py)
{
    return
    py.augAssign(
        &py.attribute(&py.name("self"), "counterGame"),
        ADD,
        &py.num(1)
    );
}

Statement &OpenAI::getRGBArrayASTNode(BuilderCodePy &py)
{
    return
    py.functionDef("getRGBArray")
        .addParameter(&py.name("self"))
        .addBodyNode(
            &py.returnStmt(
                &py.attribute(&py.name("self"), "rgbArray")
            )
        );
}

Statement &OpenAI::resetEnvironmentASTNode(BuilderCodePy &py)
{
    return
    py.functionDef("resetEnvironment")
        .addParameter(&py.name("self"))
        .addParameter(&py.name("step"))
        .addBodyNode(&buildEnviromentASTNode(py));
}

Assignment &OpenAI::buildEnviromentASTNode(BuilderCodePy &py)
{
    return
    py.assign(
        &py.tuple(
            &py.attribute(&py.name("self"), "observations"),
            &py.attribute(&py.name("self"), "reward")
        )
        .AddExpression(&py.attribute(&py.name("self"), "done"))
        .AddExpression(&py.attribute(&py.name("self"), "rgbArray")),
            &py.call(
                &py.attribute(
                    &py.attribute(&py.name("self"), "environment"), "step"
                )
            )
            .addArgument(&py.name("step"))
    );
}

void OpenAI::gameInitializer()
{
    m_gamesOfOpenAI.push_back("Asterix-ram-v0");
    m_gamesOfOpenAI.push_back("Boxing-ram-v0");
    m_gamesOfOpenAI.push_back("BeamRider-ram-v0");
    m_gamesOfOpenAI.push_back("CrazyClimber-ra,-v0");
    m_gamesOfOpenAI.push_back("DemonAttack-ram-v0");
    m_gamesOfOpenAI.push_back("MsPacMan-ram-v0");
    m_gamesOfOpenAI.push_back("Phoenix-ram-v0");
    m_gamesOfOpenAI.push_back("Skiing-ram-v-0");
    m_gamesOfOpenAI.push_back("Solaris-ra,-v0");
    m_gamesOfOpenAI.push_back("TimePilot-ra,-v0");
}
