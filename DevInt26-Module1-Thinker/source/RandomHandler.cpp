#include "RandomHandler.h"

RandomHandler::RandomHandler()
{
}

RandomHandler::RandomHandler(const string &p_randomName)
:m_randomName{p_randomName}
{
}

RandomHandler::~RandomHandler()
{
}

void RandomHandler::setRandomName(const string &p_randomName)
{
    m_randomName = p_randomName;
}

const string RandomHandler::getRandomName() const
{
    return m_randomName;
}

ToolType RandomHandler::getClassName() const
{
    return e_randomHandler;
}

const string RandomHandler::getIToolName() const
{
    return m_randomName;
}

Json RandomHandler::serialize()
{
    Json jsonAnswer;

    jsonAnswer["RandomName"]= m_randomName;

    return jsonAnswer;
}

void RandomHandler::deserialize(Json &p_json)
{
    this->setRandomName(p_json["RandomName"]);
}

Statement &RandomHandler::generateASTInitNode(BuilderCodePy &py)
{
    return
    py.functionDef("__init__")
        .addParameter(&py.name("self"))
        .addBodyNode(&initStepASTNode(py));
}

Statement &RandomHandler::generateASTBodyNode(BuilderCodePy &py)
{
    return
    py.concat()
        .addBodyNode(&getStepASTNode(py))
        .addBodyNode(&setStepASTNode(py));

}

Statement &RandomHandler::generateASTClassNode(BuilderCodePy &py)
{
    return
    py.concat()
        .addBodyNode(
            &py.import("random")
        )
        .addBodyNode(
            &py.classDef("RandomHandler")
                .addParameter(&py.name("object"))
                .addBodyNode(&generateASTInitNode(py))
                .addBodyNode(&generateASTBodyNode(py))
        );
}

Statement &RandomHandler::setStepASTNode(BuilderCodePy &py)
{
    return
    py.functionDef("setStep")
        .addParameter(&py.name("self"))
        .addBodyNode(
            &py.assign(
                &py.attribute(&py.name("self"), "step"),
                &choiceKeyASTNode(py)
            )
        );
}

CallFunction &RandomHandler::choiceKeyASTNode(BuilderCodePy &py)
{
    return
    py.call(&py.attribute(&py.name("random"), "choice"))
        .addArgument(&py.name(buildMovesArray()));
}

const string RandomHandler::buildMovesArray() const
{
    Number c0(0), c1(1), c2(2), c3(3), c4(4), c5(5), c6(6);
    List list;
    list.AddExpression(&c0)
        .AddExpression(&c1)
        .AddExpression(&c2)
        .AddExpression(&c3)
        .AddExpression(&c4)
        .AddExpression(&c5)
        .AddExpression(&c6);

    BuilderCode visitor = BuilderCode();
    visitor.visit(&list);
    string movesArray = visitor.getCode();
    return movesArray;
}

