#include "KeyboardHandler.h"

KeyboardHandler::KeyboardHandler(const string &p_keyboardName)
:m_keyboardName{p_keyboardName}
{
}

ToolType KeyboardHandler::getClassName() const
{
    return e_keyboardHandler;
}

const string KeyboardHandler::getIToolName() const
{
    return m_keyboardName;
}

Json KeyboardHandler::serialize()
{
    Json jsonAnswer;

    jsonAnswer["keyboardName"]= m_keyboardName;

    return jsonAnswer;
}

void KeyboardHandler::deserialize(Json &p_json)
{
    m_keyboardName = p_json["keyboardName"];
}

Statement &KeyboardHandler::setStepASTNode(BuilderCodePy &py)
{
    return
    py.functionDef("setStep")
        .addParameter(&py.name("self"))
        .addBodyNode(&eventFor(py));
}

Statement &KeyboardHandler::generateASTInitNode(BuilderCodePy &py)
{
    return
    py.functionDef("__init__")
        .addParameter(&py.name("self"))
        .addBodyNode(&initStepASTNode(py));
}

Statement &KeyboardHandler::generateASTBodyNode(BuilderCodePy &py)
{
    return
    py.concat()
        .addBodyNode(&getStepASTNode(py))
        .addBodyNode(&setStepASTNode(py));
}

Statement &KeyboardHandler::generateASTClassNode(BuilderCodePy &py)
{
    return
    py.classDef("KeyboardHandler")
        .addParameter(&py.name("object"))
        .addBodyNode(&generateASTInitNode(py))
        .addBodyNode(&generateASTBodyNode(py));
}

Expression &KeyboardHandler::bodyForEventTypeCompare(BuilderCodePy &py)
{
    return
    py.compare(
        &py.attribute(
            &py.name("event"), "type"), Eq ,
        &py.attribute(
            &py.name("pygame"), "KEYDOWN")
    );
}

Statement &KeyboardHandler::bodyForEventType(BuilderCodePy &py)
{
    return
    py.ifStmt(&bodyForEventTypeCompare(py))
        .addIfNode(&ifSteps(py));
}

Statement &KeyboardHandler::ifSteps(BuilderCodePy &py)
{
    return
    py.ifStmt(&setStepUpASTNode(py))
            .addIfNode(&AssignKeyUp(py))
        .addElifNode(&py.ifStmt(&setStepWASTNode(py))
            .addIfNode(&AssignKeyUp(py))
        )
        .addElifNode(&py.ifStmt(&setStepKP5ASTNode(py))
            .addIfNode(&AssignKeyUp(py))
        )
        .addElifNode(&py.ifStmt(&setStepDownASTNode(py))
            .addIfNode(&AssignKeyDown(py))
        )
        .addElifNode(&py.ifStmt(&setStepDownSASTNode(py))
            .addIfNode(&AssignKeyDown(py))
        )
        .addElifNode(&py.ifStmt(&setStepDownKP2ASTNode(py))
            .addIfNode(&AssignKeyDown(py))
        )
        .addElifNode(&py.ifStmt(&setStepLeftASTNode(py))
            .addIfNode(&AssignKeyLeft(py))
        )
        .addElifNode(&py.ifStmt(&setStepLeftAASTNode(py))
            .addIfNode(&AssignKeyLeft(py))
        )
        .addElifNode(&py.ifStmt(&setStepLeftKP1ASTNode(py))
            .addIfNode(&AssignKeyLeft(py))
        )
        .addElifNode(&py.ifStmt(&setStepRightASTNode(py))
            .addIfNode(&AssignKeyRight(py))
        )
        .addElifNode(&py.ifStmt(&setStepRightDASTNode(py))
            .addIfNode(&AssignKeyRight(py))
        )
        .addElifNode(&py.ifStmt(&setStepRightKP3ASTNode(py))
            .addIfNode(&AssignKeyRight(py))
        );
}

Statement &KeyboardHandler::eventFor(BuilderCodePy &py)
{
    return
    py.forStmt(&py.name("event"),
        &py.call(
            &py.attribute(
                &py.attribute(&py.name("pygame"),"event"),"get")
            )
        )
        .addBody(&bodyForEventType(py));
}

Statement &KeyboardHandler::AssignKeyUp(BuilderCodePy &py)
{
    return
    py.assign(
        &py.attribute(
            &py.name("self"), "step"), &py.num(1)
    );
}

Statement &KeyboardHandler::AssignKeyDown(BuilderCodePy &py)
{
    return
    py.assign(
        &py.attribute(
            &py.name("self"), "step"), &py.num(4)
    );
}

Statement &KeyboardHandler::AssignKeyLeft(BuilderCodePy &py)
{
    return
    py.assign(
        &py.attribute(
            &py.name("self"), "step"), &py.num(3)
    );
}

Statement &KeyboardHandler::AssignKeyRight(BuilderCodePy &py)
{
    return
    py.assign(
        &py.attribute(
            &py.name("self"), "step"), &py.num(2)
    );
}

Expression &KeyboardHandler::setStepUpASTNode(BuilderCodePy &py)
{
    return
    py.compare(
        &py.attribute(&py.name("event"), "key"), Eq, &py.name("K_UP"));

}

Expression &KeyboardHandler::setStepWASTNode(BuilderCodePy &py)
{
    return
    py.compare(
        &py.attribute(&py.name("event"), "key"), Eq, &py.name("K_w"));

}

Expression &KeyboardHandler::setStepKP5ASTNode(BuilderCodePy &py)
{
    return
    py.compare(
        &py.attribute(&py.name("event"), "key"), Eq, &py.name("K_KP5"));

}

Expression &KeyboardHandler::setStepDownASTNode(BuilderCodePy &py)
{
    return
    py.compare(
        &py.attribute(&py.name("event"), "key"), Eq, &py.name("K_DOWN"));
}

Expression &KeyboardHandler::setStepDownSASTNode(BuilderCodePy &py)
{
    return
    py.compare(
        &py.attribute(&py.name("event"), "key"), Eq, &py.name("K_s"));
}

Expression &KeyboardHandler::setStepDownKP2ASTNode(BuilderCodePy &py)
{
    return
    py.compare(
        &py.attribute(&py.name("event"), "key"), Eq, &py.name("K_KP2"));
}


Expression &KeyboardHandler::setStepLeftASTNode(BuilderCodePy &py)
{
    return
    py.compare(
        &py.attribute(&py.name("event"), "key"), Eq, &py.name("K_LEFT"));
}

Expression &KeyboardHandler::setStepLeftAASTNode(BuilderCodePy &py)
{
    return
    py.compare(
        &py.attribute(&py.name("event"), "key"), Eq, &py.name("K_a"));
}

Expression &KeyboardHandler::setStepLeftKP1ASTNode(BuilderCodePy &py)
{
    return
    py.compare(
        &py.attribute(&py.name("event"), "key"), Eq, &py.name("K_KP1"));
}

Expression &KeyboardHandler::setStepRightASTNode(BuilderCodePy &py)
{
    return
    py.compare(
        &py.attribute(&py.name("event"), "key"), Eq, &py.name("K_RIGHT"));
}

Expression &KeyboardHandler::setStepRightDASTNode(BuilderCodePy &py)
{
    return
    py.compare(
        &py.attribute(&py.name("event"), "key"), Eq, &py.name("K_d"));
}

Expression &KeyboardHandler::setStepRightKP3ASTNode(BuilderCodePy &py)
{
    return
    py.compare(
        &py.attribute(&py.name("event"), "key"), Eq, &py.name("K_KP3"));
}
