#include "AgentToolbox.h"

AgentToolbox::AgentToolbox()
{
}

AgentToolbox::~AgentToolbox()
{
}

shared_ptr<Output> AgentToolbox::getNewOutput(const string &p_outputName) const
{
    return make_shared<Output>(p_outputName);
}

shared_ptr<OpenAI> AgentToolbox::getNewOpenAI(const string &p_openAIName) const
{
    return make_shared<OpenAI>(p_openAIName);
}

shared_ptr<Input> AgentToolbox::getNewInput(const string &p_inputName) const
{
    return make_shared<Input>(p_inputName);
}

shared_ptr<ScreenHandler> AgentToolbox::getNewScreenHandler(const int &p_screenSpeed, const int &p_screenHeight, const int &p_screenWidth, const string &p_screenName) const
{
    return make_shared<ScreenHandler>(p_screenSpeed, p_screenHeight, p_screenWidth, p_screenName);
}

shared_ptr<KeyboardHandler> AgentToolbox::getNewKeyboardHandler(const string &p_keyboardName) const
{
    return make_shared<KeyboardHandler>(p_keyboardName);
}

shared_ptr<TensorFlow> AgentToolbox::getNewTensorFlow(const string &p_tensorFlowName) const
{
    return make_shared<TensorFlow>(p_tensorFlowName);
}

shared_ptr<RandomHandler> AgentToolbox::getNewRandomHandler(const string 
    &p_randomHandlerName) const
{
    return make_shared<RandomHandler>(p_randomHandlerName);
}