#include "AStep.h"
#include <iostream>

AStep::AStep()
{
}

AStep::~AStep()
{
}

Statement &AStep::getStepASTNode(BuilderCodePy &py)
{
    return
    py.functionDef("getStep")
        .addParameter(&py.name("self"))
        .addBodyNode(
            &py.returnStmt(
                &py.attribute(&py.name("self"), "step")
            )
        );
}

Statement &AStep::initStepASTNode(BuilderCodePy &py)
{
    return
    py.assign(
        &py.attribute(&py.name("self"), "step"),
            &py.num(0)
    );
}

