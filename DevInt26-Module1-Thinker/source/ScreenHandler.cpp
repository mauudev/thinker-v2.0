#include "ScreenHandler.h"

ScreenHandler::ScreenHandler(const int &p_screenSpeed, const int &p_screenHeight, const int &p_screenWidth, const string &p_screenName)
:m_screenSpeed{p_screenSpeed}, m_screenHeight{p_screenHeight}, m_screenWidth{p_screenWidth}
    , m_screenName{p_screenName}
{
}

ToolType ScreenHandler::getClassName() const
{
    return e_screenHandler;
}

const string ScreenHandler::getIToolName() const
{
    return m_screenName;
}

void ScreenHandler::setScreenHandlerSpeed(const int &p_screenSpeed)
{
    m_screenSpeed = p_screenSpeed;
}

void ScreenHandler::setScreenHandlerHeight(const int &p_screenHeight)
{
    m_screenHeight = p_screenHeight;
}

void ScreenHandler::setScreenHandlerWidth(const int &p_screenWidth)
{
    m_screenWidth = p_screenWidth;
}

const int ScreenHandler::getScreenHandlerSpeed() const
{
    return m_screenSpeed;
}

const int ScreenHandler::getScreenHandlerHeight() const
{
    return m_screenHeight;
}

const int ScreenHandler::getScreenHandlerWidth() const
{
    return m_screenWidth;
}

const string ScreenHandler::generateScreenHandlerSize() const
{
    string left = "(";
    string comma = ",";
    string right = ")";
    string width = to_string(m_screenWidth);
    string height = to_string(m_screenHeight);
    string screenHandlerSize = left+width+comma+height+right;
    return screenHandlerSize;
}

Expression &ScreenHandler::displayArraySpeed(BuilderCodePy &py)
{
    return
    py.call(
        &py.attribute(
                    &py.attribute(
                        &py.name("self"), "clock"), "tick")
    )
    .addArgument(
        &py.attribute(
            &py.name("self"), "screenSpeed")
    );
}


Statement &ScreenHandler::displayArrayTupleRgbArrayAssignation(BuilderCodePy &py)
{
    return
    py.assign(
        &py.tuple(&py.name("rgbArrayMin"), &py.name("rgbArrayMax")),
        &py.tuple(
            &py.call(
                &py.attribute(
                    &py.name("rgbArray"), "min")),
            &py.call(
                &py.attribute(
                    &py.name("rgbArray"), "max"))
        )
    );
}
Statement &ScreenHandler::displayArrayAssingRgbArray(BuilderCodePy &py)
{
    return
    py.assign(&py.name("rgbArray"),
        &py.binOp(
            &py.binOp(
                &py.num(255.0f),
                MULT,
                &py.binOp(
                    &py.name("rgbArray"),
                    SUB,
                    &py.name("rgbArrayMin")
                )
            ),
            DIV,
            &py.binOp(
                &py.name("rgbArrayMax"),
                SUB,
                &py.name("rgbArrayMin")
            )
        )
    );

}

Statement &ScreenHandler::displayArrayAssignPyGameImageIfTrue(BuilderCodePy &py)
{
    return
    py.assign(
        &py.name("pygameImage"),
            &py.call(
                &py.attribute(
                    &py.attribute(
                        &py.name("pygame"), "surfarray"), "make_surface")
            )
            .addArgument(
                &py.name("rgbArray.swapaxes(0, 1) if True else rgbArray")
            )
    );
}

Statement &ScreenHandler::displayArrayAssignPyGameImage(BuilderCodePy &py)
{
    return
    py.assign(
        &py.name("pygameImage"),
            &py.call(
                &py.attribute(
                    &py.attribute(
                        &py.name("pygame"), "transform"), "scale")
            )
            .addArgument(&py.name("pygameImage"))
            .addArgument(&py.attribute(
                &py.name("self"), "screenSize"))
    );
}

Statement &ScreenHandler::getDisplay(BuilderCodePy &py)
{
    return
    py.functionDef("getDisplay")
        .addParameter(&py.name("self"))
        .addBodyNode(
            &py.call(
                &py.attribute(
                    &py.attribute(
                    &py.name("pygame"), "display"), "flip")
            )
        );
}

Statement &ScreenHandler::closeGame(BuilderCodePy &py)
{
    return
    py.functionDef("closeGame")
        .addParameter(&py.name("self"))
        .addBodyNode(
            &py.call(
                    &py.attribute(
                    &py.name("pygame"), "quit"))
        );
}

Expression &ScreenHandler::displayArrayPyGameScreen(BuilderCodePy &py)
{
    return
    py.call(
        &py.attribute(
                    &py.attribute(
                        &py.name("self"), "pygameScreen"), "blit")
    )
    .addArgument(&py.name("pygameImage"))
    .addArgument(&py.name("(0,0)"));
}

Expression &ScreenHandler::displayArrayCallGetDisplay(BuilderCodePy &py)
{
    return
    py.call(
        &py.attribute(
            &py.name("self"), "getDisplay")
    );
}

Statement &ScreenHandler::setDisplayArray(BuilderCodePy &py)
{
    return
    py.functionDef("setDisplayArray")
        .addParameter(&py.name("self"))
        .addParameter(&py.name("rgbArray"))
        .addBodyNode(&displayArraySpeed(py))
        .addBodyNode(&displayArrayTupleRgbArrayAssignation(py))
        .addBodyNode(&displayArrayAssingRgbArray(py))
        .addBodyNode(&displayArrayAssignPyGameImageIfTrue(py))
        .addBodyNode(&displayArrayAssignPyGameImage(py))
        .addBodyNode(&displayArrayPyGameScreen(py))
        .addBodyNode(&displayArrayCallGetDisplay(py));
}

Statement &ScreenHandler::initScreenSpeed(BuilderCodePy &py)
{
    return
    py.assign(
        &py.attribute(
            &py.name("self"), "screenSpeed"),
            &py.num(m_screenSpeed)
    );

}

Statement &ScreenHandler::initScreenSize(BuilderCodePy &py)
{
    return
    py.assign(
        &py.attribute(&py.name("self"), "screenSize"),
        &py.name(ScreenHandler::generateScreenHandlerSize())
    );
}

Statement &ScreenHandler::initClock(BuilderCodePy &py)
{
    return
    py.assign(
        &py.attribute(
            &py.name("self"), "clock"),
        &py.call(
            &py.attribute(
                &py.attribute(
                    &py.name("pygame"),"time"), "Clock")
        )
    );
}

Statement &ScreenHandler::initPygameScreen(BuilderCodePy &py)
{
    return
    py.assign(
        &py.attribute(
        &py.name("self"), "pygameScreen"),
        &py.call(
            &py.attribute(
                &py.attribute(
                    &py.name("pygame"), "display"), "set_mode")
        )
        .addArgument(
                &py.attribute(
                    &py.name("self"), "screenSize")
        )
    );

}

Statement &ScreenHandler::initPygameImage(BuilderCodePy &py)
{
    return
    py.assign(
        &py.attribute(
        &py.name("self"), "pygameImage"),
        &py.num(0)
    );
}

Statement &ScreenHandler::generateImportPyGame(BuilderCodePy &py)
{
    return
    py.import("pygame");
}

Statement &ScreenHandler::generateImportPyGameLocals(BuilderCodePy &py)
{
    return
    py.importFrom("pygame.locals");
}

Statement &ScreenHandler::concatImports(BuilderCodePy &py)
{
    return
    py.concat()
        .addBodyNode(&generateImportPyGame(py))
        .addBodyNode(&generateImportPyGameLocals(py));

}

Statement &ScreenHandler::generateClassNode(BuilderCodePy &py)
{
    return
    py.classDef("ScreenHandler")
        .addParameter(&py.name("object"))
        .addBodyNode(&generateASTInitNode(py))
        .addBodyNode(&generateASTBodyNode(py));

}

Statement &ScreenHandler::generateASTInitNode(BuilderCodePy &py)
{
    return
    py.functionDef("__init__")
        .addParameter(&py.name("self"))
        .addBodyNode(&initScreenSpeed(py))
        .addBodyNode(&initScreenSize(py))
        .addBodyNode(&initClock(py))
        .addBodyNode(&initPygameScreen(py))
        .addBodyNode(&initPygameImage(py));
}

Statement &ScreenHandler::generateASTBodyNode(BuilderCodePy &py)
{
    return
    py.concat()
        .addBodyNode(&closeGame(py))
        .addBodyNode(&getDisplay(py))
        .addBodyNode(&setDisplayArray(py));
}

Statement &ScreenHandler::generateASTClassNode(BuilderCodePy &py)
{
    return
    py.concat()
        .addBodyNode(&concatImports(py))
        .addBodyNode(&generateClassNode(py));
}

Json ScreenHandler::serialize()
{
    Json json;
    json["screenSpeed"]= m_screenSpeed;
    json["screenHeight"] = m_screenHeight;
    json["screenWidth"] = m_screenWidth;
    json["screenName"] = m_screenName;
    return json;
}

void ScreenHandler::deserialize(Json &p_json)
{
    this->setScreenHandlerSpeed(p_json["screenSpeed"]);
    this->setScreenHandlerHeight(p_json["screenHeight"]);
    this->setScreenHandlerWidth(p_json["screenWidth"]);
    m_screenName = p_json["screenName"];
}
