#include "Agent.h"
#include "ExceptionNode.h"

#include <iostream>

Agent::Agent()
{
}

Agent::Agent(const string &p_agentName)
:m_agentName{p_agentName}
{
}

Agent::~Agent()
{
}

void Agent::validateToolType(const ITool &p_tool, ToolType p_toolType) const
{
    if(p_tool.getClassName() != p_toolType)
    {
        throw ExceptionNode{"The tool you are trying to get does not match"
            "the type you requested."};
    }
}

bool Agent::addToolToMap(const string &p_toolName, shared_ptr<ITool> p_tool)
{
    bool validator = m_toolMap.insert(make_pair
        (p_toolName, p_tool)).second;

    return validator;
}

void Agent::addIdtoToMap(const string &p_toolName, shared_ptr<IDTO> p_idto)
{
    m_persistenceMap.insert(make_pair(p_toolName, p_idto));
}

bool Agent::removeTool(const string &p_toolName)
{
    m_persistenceMap.erase(p_toolName);
    return m_toolMap.erase(p_toolName);
}

bool Agent::addNewOutput(shared_ptr<Output> p_output)
{
    string toolName = p_output->getIToolName();
    bool validator = addToolToMap(toolName, p_output);
    if(validator == true)
    {
        addIdtoToMap(toolName, static_pointer_cast<IDTO>(p_output));
    }
    return validator;
}

bool Agent::addNewOpenAI(shared_ptr<OpenAI> p_openAI)
{
    string toolName = p_openAI->getIToolName();
    bool validator = addToolToMap(toolName, p_openAI);
    if(validator == true)
    {
        addIdtoToMap(toolName, static_pointer_cast<IDTO>(p_openAI));
    }
    return validator;
}

bool Agent::addNewScreenHandler(shared_ptr<ScreenHandler> p_screenHandler)
{
    string toolName = p_screenHandler->getIToolName();
    bool validator = addToolToMap(toolName, p_screenHandler);
    if(validator == true)
    {
        addIdtoToMap(toolName, static_pointer_cast<IDTO>(p_screenHandler));
    }
    return validator;
}

bool Agent::addNewInput(shared_ptr<Input> p_input)
{
    string toolName = p_input->getIToolName();
    bool validator = addToolToMap(toolName, p_input);
    if(validator == true)
    {
        addIdtoToMap(toolName, static_pointer_cast<IDTO>(p_input));
    }
    return validator;
}

bool Agent::addNewKeyboardHandler(shared_ptr<KeyboardHandler>
    p_keyboardHandler)
{
    string toolName = p_keyboardHandler->getIToolName();
    bool validator = addToolToMap(toolName, p_keyboardHandler);
    if(validator == true)
    {
        addIdtoToMap(toolName,
            static_pointer_cast<IDTO>(p_keyboardHandler));
    }   
    return validator;
}

bool Agent::addNewTensorFlow(shared_ptr<TensorFlow> p_tensorFlow)
{
    string toolName = p_tensorFlow->getIToolName();
    bool validator = addToolToMap(toolName, p_tensorFlow);
    if(validator == true)
    {
        addIdtoToMap(toolName, static_pointer_cast<IDTO>(p_tensorFlow));
    }
    return validator;
}

bool Agent::addNewRandomHandler(shared_ptr<RandomHandler> p_randomHandler)
{
    string toolName = p_randomHandler->getIToolName();
    bool validator = addToolToMap(toolName, p_randomHandler);
    if(validator == true)
    {
        addIdtoToMap(toolName, static_pointer_cast<IDTO>(p_randomHandler));
    }
    return validator;
}

shared_ptr<ITool> Agent::getTool(const string &p_toolName) const
{
    map<string, shared_ptr<ITool>>::const_iterator toolIterator =
        m_toolMap.find(p_toolName);
    if(toolIterator == m_toolMap.end())
    {
        throw ExceptionNode{"Tool not found!"};
    }
    return toolIterator->second;
}

bool Agent::linkTools(const string &p_toolILinked, const string &p_tool)
{
    shared_ptr<ILinked> toolLinked = 
        dynamic_pointer_cast<ILinked>(getTool(p_toolILinked));
    if(toolLinked != nullptr)
    {
        toolLinked->link(getTool(p_tool));
        return true;
    }
    return false;
}

Output &Agent::getOutput(const string &p_outputName) const
{
    ITool &tool = *(getTool(p_outputName).get()); 
    validateToolType(tool, e_output);
    return static_cast<Output&>(tool);
}

OpenAI &Agent::getOpenAI(const string &p_openAIName) const
{
    ITool &tool = *(getTool(p_openAIName).get()); 
    validateToolType(tool, e_openAI);
    return static_cast<OpenAI&>(tool);
}

Input &Agent::getInput(const string &p_inputName) const
{
    ITool &tool = *(getTool(p_inputName).get()); 
    validateToolType(tool, e_input);
    return static_cast<Input&>(tool);
}

ScreenHandler &Agent::getScreenHandler(const string &p_screenHandlerName) const
{
    ITool &tool = *(getTool(p_screenHandlerName).get()); 
    validateToolType(tool, e_screenHandler);
    return static_cast<ScreenHandler&>(tool);
}

KeyboardHandler &Agent::getKeyboardHandler(const string &p_keyboardHandlerName)
    const
{
    ITool &tool = *(getTool(p_keyboardHandlerName).get()); 
    validateToolType(tool, e_keyboardHandler);
    return static_cast<KeyboardHandler&>(tool);
}

TensorFlow &Agent::getTensorFlow(const string &p_tensorFlowName) const
{
    ITool &tool = *(getTool(p_tensorFlowName).get()); 
    validateToolType(tool, e_tensorFlow);
    return static_cast<TensorFlow&>(tool);
}

RandomHandler &Agent::getRandomHandler(const string &p_randomHandlerName) const
{
    ITool &tool = *(getTool(p_randomHandlerName).get()); 
    validateToolType(tool, e_randomHandler);
    return static_cast<RandomHandler&>(tool);
}

void Agent::setAgentName(const string &p_agentName)
{
    m_agentName = p_agentName;
}

const string Agent::getAgentName() const
{
    return m_agentName;
}

Json Agent::serialize()
{
    Json jsonAnswer;
    jsonAnswer["AgentName"] = m_agentName;

    size_t count = 0;
    for(pair<const string, shared_ptr<IDTO>> &iterator : m_persistenceMap)
    {
        int toolType = (m_toolMap[iterator.first])->getClassName(); 
        jsonAnswer["Tools"][count] =
            (iterator.second)->serialize();
        jsonAnswer["Tools"][count]["ToolType"]  = toolType;
        count++;
    }
    return jsonAnswer;
}

void Agent::deserialize(Json &p_json)
{
    m_toolMap.clear();
    m_persistenceMap.clear();
    
    setAgentName(p_json["AgentName"]);

    ToolType toolType;
    vector<size_t> linkedPositions;
    shared_ptr<ITool> newTool;
    for(size_t i = 0; i < p_json["Tools"].size(); i++)
    {
        toolType = p_json["Tools"][i]["ToolType"];
        if ((toolType < e_openAI))
        {
            switch(toolType)
            {
                case e_keyboardHandler :
                    newTool =
                        static_pointer_cast<ITool>(
                            make_shared<KeyboardHandler>("")
                        );
                    break;
                case e_randomHandler :
                    newTool =
                        static_pointer_cast<ITool>(
                            make_shared<RandomHandler>("")
                        );
                    break;
                case e_tensorFlow :
                    newTool =
                        static_pointer_cast<ITool>(
                            make_shared<TensorFlow>("")
                        );
                    break;
                case e_screenHandler :
                    newTool =
                        static_pointer_cast<ITool>(
                            make_shared<ScreenHandler>(1,1,1,"")
                        );
                    break;
                case e_input :
                    newTool =
                        static_pointer_cast<ITool>(
                            make_shared<Input>("")
                        );
                    break;
                case e_output :
                    newTool = 
                        static_pointer_cast<ITool>(
                            make_shared<Output>("")
                        );
                    break;
                default: break;
            }
            shared_ptr<IDTO> idto = (dynamic_pointer_cast<IDTO>(newTool));
            idto->deserialize(p_json["Tools"][i]);
            addIdtoToMap(newTool->getIToolName(), idto);
            addToolToMap(newTool->getIToolName(), newTool);
        }
        else
        {
            linkedPositions.push_back(i);
        }
    }
    for(size_t i = 0U; i < linkedPositions.size(); ++i)
    { 
        toolType = p_json["Tools"][linkedPositions[i]]["ToolType"];
        switch(toolType)
        {
            case e_openAI: 
                newTool =
                    static_pointer_cast<ITool>(make_shared<OpenAI>(""));
            break;
            /*case e_scriptPython:
                newTool =
                    static_pointer_cast<ITool>(make_shared<ScriptPython>(""));
            break;*/
            default: break;
        }
        shared_ptr<IDTO> idto = (dynamic_pointer_cast<IDTO>(newTool));
        idto->deserialize(p_json["Tools"][linkedPositions[i]]);
        addIdtoToMap(newTool->getIToolName(), idto);
        addToolToMap(newTool->getIToolName(), newTool);
        for (size_t j = 0U;
            j < p_json["Tools"][linkedPositions[i]]["Links"].size(); ++j)
        {
            linkTools(newTool->getIToolName(),
                p_json["Tools"][linkedPositions[i]]["Links"][j]);
        }
    }
}