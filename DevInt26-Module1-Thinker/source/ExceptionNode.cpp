#include "ExceptionNode.h"

ExceptionNode::ExceptionNode()
{
}

ExceptionNode::ExceptionNode(const string &p_sms)
: m_sms{p_sms}
{
}

const char *ExceptionNode::what() const noexcept
{
	return m_sms.data();
}
