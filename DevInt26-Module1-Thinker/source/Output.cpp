#include "Output.h"

Output::Output(const string &p_outputName)
:m_outputName{p_outputName}
{
}

void Output::setOutput(const int &p_output)
{
    m_output = p_output;
}

const int Output::getOutput() const
{
    return m_output;
}

ToolType Output::getClassName() const
{
    return e_output;
}

const string Output::getIToolName() const
{
    return m_outputName;
}

Json Output::serialize()
{
    Json jsonAnswer;
    jsonAnswer["outputName"] = m_outputName;
    return jsonAnswer;
}

void Output::deserialize(Json &p_json)
{
    m_outputName = p_json["outputName"];
}

Statement &Output::generateASTInitNode(BuilderCodePy &py)
{
    return
    py.functionDef("__init__")
        .addParameter(&py.name("self"))
        .addBodyNode(&initOutputNameASTNode(py))
        .addBodyNode(&initOutputASTNode(py));
}

Statement &Output::generateASTBodyNode(BuilderCodePy &py)
{
    return 
    py.concat()
        .addBodyNode(&setOutputASTNode(py))
        .addBodyNode(&setOutputNameASTNode(py))
        .addBodyNode(&getOutputASTNode(py))
        .addBodyNode(&getOutputNameASTNode(py));
}

Statement &Output::generateASTClassNode(BuilderCodePy &py)
{
    return 
    py.classDef("Output")
        .addParameter(&py.name("object"))
        .addBodyNode(&generateASTInitNode(py))
        .addBodyNode(&generateASTBodyNode(py));
}

Assignment &Output::initOutputNameASTNode(BuilderCodePy &py)
{
    return
    py.assign(
        &py.attribute(&py.name("self"), "outputName"),
        &py.name("\"" + m_outputName + "\"")
    );
}

Assignment &Output::initOutputASTNode(BuilderCodePy &py)
{
    return
    py.assign(
        &py.attribute(&py.name("self"), "output"),
        &py.num(m_output)
    );
}

Statement &Output::setOutputASTNode(BuilderCodePy &py)
{
    return
    py.functionDef("setOutput")
        .addParameter(&py.name("self"))
        .addParameter(&py.name("output"))
        .addBodyNode(
            &py.assign(
                &py.attribute(&py.name("self"), "output"),
                &py.name("output")
            )
        );
}

FunctionDef &Output::setOutputNameASTNode(BuilderCodePy &py)
{
    return 
    py.functionDef("setOutputName")
        .addParameter(&py.name("self"))
        .addParameter(&py.name("outputName"))
        .addBodyNode(
            &py.assign(
                &py.attribute(
                    &py.name("self"), "outputName"
                ),
                &py.name("outputName")
            )
        );
}

Statement &Output::getOutputASTNode(BuilderCodePy &py)
{
    return
    py.functionDef("getOutput")
        .addParameter(&py.name("self"))
        .addBodyNode(
            &py.returnStmt(
                &py.attribute(&py.name("self"), "output")
            )
        );
}

FunctionDef &Output::getOutputNameASTNode(BuilderCodePy &py)
{
    return 
    py.functionDef("getOutputName")
        .addParameter(&py.name("self"))
        .addBodyNode(
            &py.returnStmt(
                &py.attribute(&py.name("self"), "outputName")
            )
        );
}