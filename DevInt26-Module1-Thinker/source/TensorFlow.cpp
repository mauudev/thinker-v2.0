#include "TensorFlow.h"

TensorFlow::TensorFlow(const string &p_tensorFlowName)
:m_tensorFlowName{p_tensorFlowName}
{
}

ToolType TensorFlow::getClassName() const
{
    return e_tensorFlow;
}

const string TensorFlow::getIToolName() const
{
    return m_tensorFlowName;
}

Json TensorFlow::serialize()
{
    Json jsonAnswer;
    jsonAnswer["tensorFlowName"] = m_tensorFlowName;
    return jsonAnswer;
}

void TensorFlow::deserialize(Json &p_json)
{
    m_tensorFlowName = p_json["tensorFlowName"];
}

Statement &TensorFlow::setStepASTNode(BuilderCodePy &py)
{
    return
    py.functionDef("setStep")
        .addParameter(&py.name("self"))
        .addBodyNode(&py.name("pass"));
}

Statement &TensorFlow::setASTTensorFlowName(BuilderCodePy &py)
{
    return
    py.functionDef("setTensorFlowName")
        .addParameter(&py.name("self"))
        .addParameter(&py.name("tensorFlowName"))
            .addBodyNode(
                &py.assign(
                    &py.attribute(
                        &py.name("self"),"tensorFlowName"
                    ),
                    &py.name("tensorFlowName")
                )
            );
}

Statement &TensorFlow::getASTTensorFlowName(BuilderCodePy &py)
{
    return
    py.functionDef("getTensorFlowName")
        .addParameter(&py.name("self"))
        .addBodyNode(
            &py.returnStmt(
                &py.attribute(&py.name("self"),"tensorFlowName")
            )
        );
}

Statement &TensorFlow::generateASTInitNode(BuilderCodePy &py)
{
    return
    py.functionDef("__init__")
        .addParameter(&py.name("self"))
            .addBodyNode(
                &py.assign(
                    &py.attribute(&py.name("self"),"tensorFlowName"),
                    &py.name("\""+m_tensorFlowName+"\"")
                )
            )
            .addBodyNode(&initStepASTNode(py));
}

Statement &TensorFlow::generateASTBodyNode(BuilderCodePy &py)
{
    return
    py.concat()
        .addBodyNode(&getASTTensorFlowName(py))
        .addBodyNode(&setASTTensorFlowName(py))
        .addBodyNode(&setStepASTNode(py))
        .addBodyNode(&getStepASTNode(py));
}

Statement &TensorFlow::generateASTClassNode(BuilderCodePy &py)
{
    return
    py.classDef("TensorFlow")
        .addParameter(&py.name("object"))
        .addBodyNode(&generateASTInitNode(py))
        .addBodyNode(&generateASTBodyNode(py));
}
