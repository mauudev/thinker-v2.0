#include "Project.h"
#include "ExceptionNode.h"

Project::Project()
:m_projectName{"Default Project Name"}
{
}

Project::~Project()
{
}

bool Project::addNewAgent(shared_ptr<Agent> p_agent)
{
    bool validator = m_agentMap.insert(std::pair<string, shared_ptr<Agent>>
        (p_agent->getAgentName(), p_agent)).second;
    return validator;
}

Agent &Project::getAgent(const string &p_agentName) const
{
    map<string, shared_ptr<Agent>>::const_iterator agentIterator =
        m_agentMap.find(p_agentName);
    if(agentIterator == m_agentMap.end())
    {
        throw ExceptionNode{"Agent not found!"};
    }
    return *agentIterator->second;
}

string Project::getProjectName() const
{
    return m_projectName;
}

void Project::setProjectName(const string &p_name)
{
    m_projectName = p_name;
}

Json Project::serialize()
{
    Json jsonAnswer;
    jsonAnswer["ProjectName"] = m_projectName;

    size_t accountant = 0;
    map<string, shared_ptr<Agent>>::iterator iterator = m_agentMap.begin();
    while(iterator != m_agentMap.end())
    {
        jsonAnswer["Agents"][accountant] = (iterator->second)->serialize();
        accountant++;
        iterator++;
    }
    return jsonAnswer;
}

void Project::deserialize(Json &p_json)
{
    m_agentMap.clear();

    this->setProjectName(p_json["ProjectName"]);

    size_t agentsCount = p_json["Agents"].size();
    for(size_t i = 0; i < agentsCount; i++)
    {
        shared_ptr<Agent> new_agent = make_shared<Agent>();
        new_agent->deserialize(p_json["Agents"][i]);
        this->addNewAgent(new_agent);
    }
}