#include "ProjectToolbox.h"

ProjectToolbox::ProjectToolbox()
{
}

ProjectToolbox::~ProjectToolbox()
{
}

shared_ptr<Agent> ProjectToolbox::getNewAgent(const string &p_agentName) const
{
    return make_shared<Agent>(p_agentName);
}