#include "Input.h"

Input::Input(const string &p_inputName)
:m_inputName{p_inputName}
{
}

ToolType Input::getClassName() const
{
    return e_input;
}

const string Input::getIToolName() const
{
    return m_inputName;
}

void Input::setNextStep(const int &p_nextStep)
{
    m_nextStep = p_nextStep;
}

const int Input::getNextStep() const
{
    return m_nextStep;
}

Json Input::serialize()
{
    Json jsonAnswer;
    jsonAnswer["inputName"] = m_inputName;
    return jsonAnswer;
}

void Input::deserialize(Json &p_json)
{
    m_inputName = p_json["inputName"];
}

Statement &Input::generateASTInitNode(BuilderCodePy &py)
{
    return
    py.functionDef("__init__")
        .addParameter(&py.name("self"))
        .addBodyNode(&initInputNameASTNode(py))
        .addBodyNode(&initNextStepASTNode(py));
}

Statement &Input::generateASTBodyNode(BuilderCodePy &py)
{
    return
    py.concat()
        .addBodyNode(&setNextStepASTNode(py))
        .addBodyNode(&getNextStepASTNode(py))
        .addBodyNode(&setInputNameASTNode(py))
        .addBodyNode(&getInputNameASTNode(py));
}

Statement &Input::generateASTClassNode(BuilderCodePy &py)
{
    return
    py.classDef("Input")
        .addParameter(&py.name("object"))
        .addBodyNode(&generateASTInitNode(py))
        .addBodyNode(&generateASTBodyNode(py));
}

Assignment &Input::initInputNameASTNode(BuilderCodePy &py)
{
    return
    py.assign(
        &py.attribute(&py.name("self"), "inputName"),
        &py.name("\"" + m_inputName + "\"")
    );
}

Assignment &Input::initNextStepASTNode(BuilderCodePy &py)
{
    return
    py.assign(
        &py.attribute(&py.name("self"), "input"),
        &py.num(m_nextStep)
    );
}

Statement &Input::setNextStepASTNode(BuilderCodePy &py)
{
    return
    py.functionDef("setNextStep")
        .addParameter(&py.name("self"))
        .addParameter(&py.name("nextStep"))
        .addBodyNode(
            &py.assign(
                &py.attribute(&py.name("self"), "nextStep"),
                &py.name("nextStep")
            )
        );
}

FunctionDef &Input::setInputNameASTNode(BuilderCodePy &py)
{
    return 
    py.functionDef("setInputName")
        .addParameter(&py.name("self"))
        .addParameter(&py.name("inputName"))
        .addBodyNode(
            &py.assign(
                &py.attribute(
                    &py.name("self"), "inputName"
                ),
                &py.name("inputName")
            )
        );
}

Statement &Input::getNextStepASTNode(BuilderCodePy &py)
{
    return
    py.functionDef("getNextStep")
        .addParameter(&py.name("self"))
        .addBodyNode(
            &py.returnStmt(
                &py.attribute(&py.name("self"), "nextStep")
            )
        );
}

FunctionDef &Input::getInputNameASTNode(BuilderCodePy &py)
{
    return 
    py.functionDef("getInputName")
        .addParameter(&py.name("self"))
        .addBodyNode(
            &py.returnStmt(
                &py.attribute(&py.name("self"), "inputName")
            )
        );
}