# Git Conventions

## Merge tags

**[COMMITTED]**. Made by a developer when a merge request has made, format like this:
```shell
  [COMMITTED](#story_id) Some description here
```
**[REVIEWED]**. When a reviewer has completed the review. Format like this:

```shell
  [REVIEWED](#story_id) Some description here
```
**[REVIEWED ACCEPTED]**. When there owner has approved the code. Format like this:

```shell
  [REVIEWED ACCEPTED](#story_id) Some description here
```
**[FIXED]**. When the developer fixes all observations

```shell
  [FIXED](#story_id) Some description here
```
**[COMPLETED]**. When the owner accept all changes and the merge is done.

```shell
  [COMPLETED](#story_id) Some description here
```

## Workflow

![git-workflow](git-workflow.png)


## Beginning with git

### Set personal information

```shell
  # put your name
  $ git config user.name "My Name"
  # your personal email
  $ git config user.email "my.personal@email.tld"
```

### Adding your changes
```shell
  # first check your local changes
  $ git status

  # if you only may add specified files, use this
  $ git add ./path/to/file.ext

  # add all changes from a directory
  $ git add ./path/to/directory/*

  # add all changes
  $ git add --all
```

### Commiting all you added

```shell
  # Please be carefully with your message,
  # try to make it simple and concise
  $ git commit -m "My simple and concise message"
```

### Pushing commits

```shell
  # Pushing to master
  $ git push origin master

  # Pushing to a branch
  $ git push origin #story_id
```


## Branches

Choose the number of your story. Use this command only if you  want to create it.

```shell
  # just to create
  $ git branch "#story_id"
  # go to branch
  $ git checkout "#story_id"

  # create and enter the branch
  $ git checkout -b "#story_id"
  ```

## Solving conflicts when  pushing

Take care when your are solving conflicts because the problems they will be really big and maybe you will clone again the repository.

```shell
  # if git say something: your commit is above many commits,
  # don't panic. Go first to the master branch
  $ git checkout master
  # make a pull from master
  $ git pull origin master
  # some files will be updated, now go to your branch
  $ git checkout "#story_id"
  # in this case will use rebase command
  $ git rebase master
  # when the task is ready, you will see a list of files,
  # all of this files are in conflicts
  # solve yourself. once you done, follow this steps
  $ git rebase --continue
  $ git add --all
  # dont make something like commit again, you are solving conflicts
  # not adding new changes. push again like this
  $ git push origin "#story_id"
    ```
