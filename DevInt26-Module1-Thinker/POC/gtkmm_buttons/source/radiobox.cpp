#include "radiobox.h"
#include <iostream>

RadioBox::RadioBox()
:m_box(Gtk::ORIENTATION_VERTICAL)
{
}

RadioBox::~RadioBox()
{
}

void RadioBox::packStart(RadioButton &p_radioButton)
{
    m_box.pack_start(p_radioButton.getInstance(),  Gtk::PACK_EXPAND_WIDGET);
}

void RadioBox::joinGroup(RadioButton &p_firstRadioButton,
    RadioButton &p_secondRadioButton)
{
    p_firstRadioButton.getInstance()
        .join_group(p_secondRadioButton.getInstance());
}

Gtk::Box &RadioBox::getBox()
{
    return m_box;
}