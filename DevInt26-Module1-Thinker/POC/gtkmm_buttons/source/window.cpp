#include <iostream>
#include "window.h"

Window::Window()
{
    this->set_title("example");
    this->set_border_width(10);
    
    this->add(m_grid);
    m_grid.add(m_grid1);
    m_grid.add(m_grid2);
    
    m_grid1.add(m_buttonClose.getInstance());
    m_grid1.add(m_button.getInstance());
    m_grid1.add(m_checkButton.getInstance());
    m_grid1.add(m_fileChooserButton.getInstance());
    m_grid1.add(m_toggleButton.getInstance());
    m_grid1.add(m_linkButton.getInstance());
    m_grid2.add(m_radioBox.getBox());

    m_radioBox.packStart(m_radioButton1);
    m_radioBox.packStart(m_radioButton2);
    m_radioBox.packStart(m_radioButton3);

    m_radioBox.joinGroup(m_radioButton2, m_radioButton1);
    m_radioBox.joinGroup(m_radioButton3, m_radioButton1);

    m_button.getInstance().signal_clicked().connect(
        sigc::mem_fun(m_button, &Button::onButtonClicked));
    m_buttonClose.getInstance().signal_clicked().connect(
        sigc::mem_fun(*this, &Window::onButtonClosed));
    m_checkButton.getInstance().signal_clicked().connect(
        sigc::mem_fun(m_checkButton, &CheckButton::onCheckbuttonClicked));
    m_radioButton1.getInstance().signal_clicked().connect(
        sigc::mem_fun(m_radioButton1, &RadioButton::onRadioButtonClicked));
    m_radioButton2.getInstance().signal_clicked().connect(
        sigc::mem_fun(m_radioButton2, &RadioButton::onRadioButtonClicked));
    m_radioButton3.getInstance().signal_clicked().connect(
        sigc::mem_fun(m_radioButton3, &RadioButton::onRadioButtonClicked));
    m_toggleButton.getInstance().signal_clicked().connect(
        sigc::mem_fun(m_toggleButton, &ToggleButton::onToggleButtonClicked));
    m_linkButton.getInstance().signal_clicked().connect(
        sigc::mem_fun(m_linkButton, &LinkButton::onLinkButtonClicked));

    this->show_all_children();
}

Window::~Window()
{
}

void Window::onButtonClosed()
{
    this->hide();
}