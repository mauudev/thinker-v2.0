#include "checkbutton.h"
#include <iostream>

CheckButton::CheckButton(const std::string &p_label)
:m_checkbutton{p_label}
{
}

CheckButton::~CheckButton()
{
}

void CheckButton::onCheckbuttonClicked() const
{
    std::cout << "The Check Button was clicked: state="
    << (m_checkbutton.get_active() ? "true" : "false") << "\n";
}

Gtk::CheckButton &CheckButton::getInstance()
{
    return m_checkbutton;
}