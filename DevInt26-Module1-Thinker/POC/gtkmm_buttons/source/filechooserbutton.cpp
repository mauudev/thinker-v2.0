#include "filechooserbutton.h"
#include <iostream>

FileChooserButton::FileChooserButton(const std::string &p_label)
:m_fileChooserButton(p_label, Gtk::FILE_CHOOSER_ACTION_OPEN)
{
    m_fileChooserButton.set_local_only(false);
}

FileChooserButton::~FileChooserButton()
{
}

Gtk::FileChooserButton &FileChooserButton::getInstance()
{
    return m_fileChooserButton;
}