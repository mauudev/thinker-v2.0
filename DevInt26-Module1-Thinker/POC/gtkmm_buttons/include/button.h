#ifndef BUTTON_H
#define BUTTON_H

#include <gtkmm/button.h>
#include <string>

class Button
{
    Gtk::Button m_button;
public:
    Button(const std::string &);
    ~Button();
    void onButtonClicked() const;
    Gtk::Button &getInstance();
};

#endif