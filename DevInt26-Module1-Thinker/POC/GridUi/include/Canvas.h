#ifndef GTKMM_CANVAS_H
#define GTKMM_CANVAS_H

#include <gtkmm.h>

class Canvas : public Gtk::DrawingArea
{
public:
    Canvas();
    virtual ~Canvas();
private:
    class CanvasItem
    {
    public:
        CanvasItem(Gtk::Widget* p_canvas, Gtk::ToolButton* p_button, 
                   double p_x,
                   double p_y)
        {
            Glib::ustring icon_name(p_button->get_icon_name());
            if (icon_name.empty())
              icon_name = p_button->get_label();
            auto icon_theme = Gtk::IconTheme::get_for_screen(p_canvas->get_screen());
            int width = 0;
            int height = 0; 
            Gtk::IconSize::lookup(Gtk::ICON_SIZE_DIALOG, width, height);
            this->m_pixbuf = icon_theme->load_icon(icon_name, width, Gtk::ICON_LOOKUP_GENERIC_FALLBACK);
            this->m_x = p_x;
            this->m_y = p_y;
        }
        Glib::RefPtr<Gdk::Pixbuf> m_pixbuf;
        double m_x, m_y;
    };

    void item_draw(const CanvasItem *p_item,
            const Cairo::RefPtr<Cairo::Context>& p_cr,
            bool p_preview);

    bool on_draw(const Cairo::RefPtr<Cairo::Context>& p_cr) override;

    void on_drag_data_received(const Glib::RefPtr<Gdk::DragContext>& p_context,
          int p_x, 
          int p_y, 
          const Gtk::SelectionData& p_selectionData, 
          guint p_info, 
          guint p_time) override;

    bool on_drag_motion(const Glib::RefPtr<Gdk::DragContext>& p_context, 
          int p_x, 
          int p_y, 
          guint p_time) override;

    bool on_drag_drop(const Glib::RefPtr<Gdk::DragContext>& p_context, 
          int p_x, 
          int p_y, 
          guint p_time) override;

    void on_drag_leave(const Glib::RefPtr<Gdk::DragContext>& p_context,
          guint p_time) override;

    bool m_drag_data_requested_for_drop;

    CanvasItem* m_drop_item;

    typedef std::vector<CanvasItem*> type_vec_items;
    type_vec_items m_canvas_items;
};

#endif