#ifndef ACTION_TOOL_H
#define ACTION_TOOL_H
#include<gtkmm/togglebutton.h>
#include<glibmm.h>
#include <gtkmm/image.h>
#include <string>

class ActionTool:public Gtk::ToggleButton
{
    private:
    Gtk::Image m_img;

    public:
    ActionTool();
    ActionTool(const std::string& p_file);

    bool isActive();
    void changeStatus();
};

#endif