#ifndef GTKMM_GRIDWINDOW_H
#define GTKMM_GRIDWINDOW_H
#include <gtkmm.h>
#include "infobar.h"
#include "ToolPaletteGtk.h"
#include "Canvas.h"
#include "CanvasToolBox.h"
#include "menubar.h"


class GridWindow : public Gtk::Window
{
private:
    InfoBar m_infoBar;
    Canvas m_canvas;
    ToolPaletteGtk m_toolPalette;
    CanvasToolBox  m_canvasToolBox;
    MenuBar m_windowMenuBar;
protected:
    Gtk::Grid m_grid;
    Gtk::Grid m_gridBottom;
    Gtk::Grid m_gridHalf;
    Gtk::Grid m_gridMenu;
    Gtk::Grid m_gridTop;

    Gtk::ScrolledWindow m_scrolledWindowCanvas;
    Gtk::ScrolledWindow m_scrolledWindowToolPalette;
public:
    GridWindow();
    ~GridWindow();
    void addGrid();
    
};

#endif