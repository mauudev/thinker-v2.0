#include "GridWindow.h"

GridWindow::GridWindow()
{
    set_title("Thinker");
    set_default_size(-1, -1);
    set_border_width(5);
    addGrid();
}
GridWindow:: ~GridWindow()
{
}
void GridWindow::addGrid()
{
    Canvas &p_auxcanvas = m_canvas;
    Gtk::Grid &p_auxWindowMenuBar = m_windowMenuBar.getGridMenuBar();
    Gtk::InfoBar &p_auxInfoBar = m_infoBar.getInstance();
    Gtk::ToolPalette &p_auxToolPalette = m_toolPalette.getToolPalette();

    add(m_grid);
    m_grid.set_orientation(Gtk::ORIENTATION_VERTICAL);
    m_gridTop.set_orientation(Gtk::ORIENTATION_HORIZONTAL);
    m_gridHalf.set_orientation(Gtk::ORIENTATION_HORIZONTAL);
    m_gridBottom.set_orientation(Gtk::ORIENTATION_HORIZONTAL);

    m_scrolledWindowCanvas.add(p_auxcanvas);
    m_scrolledWindowCanvas.set_size_request(500, 400);
    m_scrolledWindowToolPalette.add(p_auxToolPalette);
    m_scrolledWindowToolPalette.set_size_request(150, 300);

    m_grid.set_column_homogeneous(true);
    m_grid.set_column_spacing(5);
    m_grid.set_row_spacing(5);
    m_gridBottom.attach(p_auxInfoBar,0,0,1,1);
    m_gridHalf.attach(m_scrolledWindowToolPalette,0,0,1,1);
    m_gridHalf.attach(m_scrolledWindowCanvas,1,0,1,1);
    m_gridHalf.attach(m_canvasToolBox,2,0,1,1);
    m_gridTop.attach(p_auxWindowMenuBar,0,0,1,1);

    m_grid.add(m_gridTop);
    m_grid.add(m_gridHalf);
    m_grid.add(m_gridBottom);

    p_auxToolPalette.add_drag_dest(m_canvas,
        Gtk::DEST_DEFAULT_HIGHLIGHT, 
        Gtk::TOOL_PALETTE_DRAG_ITEMS, 
        Gdk::ACTION_COPY);

    show_all_children();
}
