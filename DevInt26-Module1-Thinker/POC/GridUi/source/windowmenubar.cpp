#include "windowmenubar.h"

Window::Window()
{
    set_title("Thinker");
    set_default_size(400,400);
    vbox = Gtk::manage(new Gtk::Box(Gtk::ORIENTATION_VERTICAL, 0));
    add(*vbox);
    Gtk::MenuBar* menuBar = menubar->createMenuBar();
    vbox->pack_start(*menuBar, Gtk::PACK_SHRINK, 0);
    Gtk::Menu* menu1 = menubar->createMenu("File",menuBar);
    menubar->createNestedSubMenu(menu1,"new");
    menubar->createNestedSubMenu(menu1,"open");
    menubar->createNestedSubMenu(menu1,"delete");
    Gtk::Menu* menu2 = menubar->createMenu("Edit",menuBar);
    menubar->createNestedSubMenu(menu2,"new");
    Gtk::Menu* menu3 = menubar->createMenu("About",menuBar);
    menubar->createNestedSubMenu(menu3,"Whut");
    menubar->createNestedSubMenu(menu3,"what");
    show_all();
}

Window::~Window()
{
    delete menubar;
}
