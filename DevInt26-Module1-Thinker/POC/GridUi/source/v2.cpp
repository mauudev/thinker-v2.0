#include "GridWindow.h"

GridWindow::GridWindow()
:m_label{"label 1"}
{
    set_title("Thinker");
    set_default_size(-1, -1);
    set_border_width(5);
    addGrid();
}
GridWindow:: ~GridWindow()
{

}
void GridWindow::addGrid()
{

     m_grid.set_orientation(Gtk::ORIENTATION_VERTICAL);
     m_gridTop.set_orientation(Gtk::ORIENTATION_HORIZONTAL);
     m_gridHalf.set_orientation(Gtk::ORIENTATION_HORIZONTAL);
     m_gridBotton.set_orientation(Gtk::ORIENTATION_HORIZONTAL);

    m_grid.set_column_homogeneous(true);
    m_grid.set_column_spacing(5);
    m_grid.set_row_spacing(5);


    auto& auxToolPalette = m_ToolPalette.getToolPalette();
    m_ScrolledWindowToolpalette.add(auxToolPalette);
    m_ScrolledWindowToolpalette.set_size_request(150, 300);
    auto& auxToolPalette2 = m_ToolPalette2.getToolPalette();
    m_ScrolledWindowToolpalette2.add(auxToolPalette2);
    m_ScrolledWindowToolpalette2.set_size_request(150, 300);
    auto& auxInfoBar = infoBar.getInstance();

    /////////////////////
    Canvas& auxcanvas=m_Canvas;
    m_ScrolledWindowCanvas.add(auxcanvas);
    m_ScrolledWindowCanvas.set_size_request(300, 300);
    //////////////////
    ///////////////////
    m_gridTop.attach(m_label,0,0,1,1);
    m_gridHalf.attach(m_ScrolledWindowToolpalette,0,0,1,1);
    m_gridHalf.attach(m_ScrolledWindowCanvas,1,0,1,1);
    m_gridHalf.attach(m_ScrolledWindowToolpalette2,2,0,1,1);
    m_gridBotton.attach(auxInfoBar,0,0,1,1);
    ///////////////////////

    m_grid.add(m_gridTop);
    m_grid.add(m_gridHalf);
    m_grid.add(m_gridBotton);

      auxToolPalette.add_drag_dest(m_Canvas,
    Gtk::DEST_DEFAULT_HIGHLIGHT, Gtk::TOOL_PALETTE_DRAG_ITEMS, Gdk::ACTION_COPY);
    add(m_grid);
     show_all_children();
}
