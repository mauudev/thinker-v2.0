#include "menubar.h"

MenuBar::MenuBar()
{
    Gtk::MenuBar* menuBar = createMenuBar();
    Gtk::Menu* menu1 = createMenu("File",menuBar);
    createNestedSubMenu(menu1,"new");
    createNestedSubMenu(menu1,"open");
    createNestedSubMenu(menu1,"delete");
    Gtk::Menu* menu2 = createMenu("Edit",menuBar);
    createNestedSubMenu(menu2,"new");
    Gtk::Menu* menu3 = createMenu("About",menuBar);
    createNestedSubMenu(menu3,"Whut");
    createNestedSubMenu(menu3,"what");
    Gtk::Menu* menu4 = createMenu("Help",menuBar);
    Gtk::Menu* menu5 = createMenu("Options",menuBar);
    Gtk::Menu* menu6 = createMenu("About",menuBar);
    Gtk::Menu* menu7 = createMenu("About",menuBar);
    Gtk::Menu* menu8 = createMenu("About",menuBar);

    m_grid.add(*menuBar);
    m_grid.set_valign(Gtk::ALIGN_FILL);
    m_grid.set_halign(Gtk::ALIGN_FILL);
}

Gtk::MenuBar* MenuBar::createMenuBar()
{
    menubar = Gtk::manage(new Gtk::MenuBar());
    return menubar;
}

Gtk::Menu* MenuBar::createMenu(const string& p_label, Gtk::MenuBar *p_menubar)
{
    menuitemDefault = Gtk::manage(new Gtk::MenuItem(p_label, true));
    p_menubar->append(*menuitemDefault);
    menuDefault = Gtk::manage(new Gtk::Menu());
    menuitemDefault->set_submenu(*menuDefault);
    return menuDefault;
}

void MenuBar::createNestedSubMenu(Gtk::Menu * p_menu, const string& p_label)
{
    menuitemDefault = Gtk::manage(new Gtk::MenuItem(p_label, true));
    p_menu->append(*menuitemDefault);
}

MenuBar::~MenuBar()
{
    delete menubar;
    delete menuitemDefault;
    delete menuDefault;
}
Gtk::Grid& MenuBar::getGridMenuBar()
{
    return m_grid;
}

