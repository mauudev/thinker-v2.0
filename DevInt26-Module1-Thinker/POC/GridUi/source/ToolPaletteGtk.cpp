#include "ToolPaletteGtk.h"
ToolPaletteGtk::ToolPaletteGtk()
{
    loadItems();
}
ToolPaletteGtk::~ToolPaletteGtk(){}
void ToolPaletteGtk::loadItems()
{
  auto group = Gtk::manage(new Gtk::ToolItemGroup());

  Gtk::Button *label_button = Gtk::manage(new Gtk::Button("      ToolPalette"));
  label_button->show();
  group->set_label_widget(*label_button);

  Gtk::ToolButton *button;
  button = Gtk::manage(new Gtk::ToolButton());
  button->set_icon_name("go-up");
  button->set_tooltip_text("Show on vertical palettes only");
  group->insert(*button);
  button->set_visible_horizontal(false);

  button = Gtk::manage(new Gtk::ToolButton());
  button->set_icon_name("go-next");
  button->set_tooltip_text("Show on horizontal palettes only");
  group->insert(*button);
  button->set_visible_vertical(false);

  button = Gtk::manage(new Gtk::ToolButton());
  button->set_icon_name("edit-delete");
  button->set_tooltip_text("Do not show at all");
  button->set_no_show_all();
  group->insert(*button);
  button->set_visible_vertical(false);

  button = Gtk::manage(new Gtk::ToolButton());
  button->set_icon_name("view-fullscreen");
  button->set_tooltip_text("Expanded this item");
  group->insert(*button);
  gtk_container_child_set (GTK_CONTAINER (group->gobj()), GTK_WIDGET (button->gobj()),
                           "homogeneous", TRUE,
                           "expand", TRUE,
                           NULL);

  button = Gtk::manage(new Gtk::ToolButton());
  button->set_icon_name("help-contents");
  button->set_tooltip_text("A regular item");
  group->insert(*button);

  m_ToolPalette.add(*group);
}
Gtk::ToolPalette &ToolPaletteGtk::getToolPalette(){
        return m_ToolPalette;
    }
