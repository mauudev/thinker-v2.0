#ifndef REMOVE_ELEMENT_EXAMPLE_H
#define REMOVE_ELEMENT_EXAMPLE_H

#include <gtkmm.h>
#include "GenericButton.h"

class ExampleButtonBox : public Gtk::Frame
{
public:
    ExampleButtonBox(bool horizontal,
        const Glib::ustring &title,
        gint spacing,
        Gtk::ButtonBoxStyle layout);

    bool windowEventKeyPress(GdkEventKey*);
    void removeActiveElements();

protected:
    Gtk::ButtonBox* m_buttonBox; 
    vector<GenericButton*> m_btnList;
    bool m_eventHandlerFlag;
};
#endif 
