#include "examplewindow.h"

void ExampleWindow::loadIconItems()
{
    Glib::RefPtr<Gtk::IconTheme> iconTheme = 
        Gtk::IconTheme::get_for_screen(get_screen());
    typedef std::vector<Glib::ustring> typeStringvec;
    typeStringvec iconNames = iconTheme->list_icons();
    typeStringvec contexts = iconTheme->list_contexts();
    std::sort(contexts.begin(), contexts.end());
    int requestedIconSize = 5;
    int requestedIconHeight = 5;

    Gtk::IconSize::lookup(Gtk::ICON_SIZE_BUTTON, requestedIconSize, 
        requestedIconHeight);

    const guint maxIconsPerGroup = 100;

    typeStringvec::const_iterator iter = contexts.begin();

    const Glib::ustring context_name = *iter;
    Gtk::ToolItemGroup *group =
        Gtk::manage(new Gtk::ToolItemGroup(context_name));
    m_toolPalette.add(*group);
    typeStringvec iconNamesForContext = iconTheme
        ->list_icons(context_name);
    std::sort(iconNamesForContext.begin(), iconNamesForContext.end());
    guint iconsCount = 0;
    for (typeStringvec::const_iterator iconIterator = iconNamesForContext
        .begin(); iconIterator != iconNamesForContext
        .end(); ++iconIterator)
    {
        const Glib::ustring iconName = *iconIterator;
        Glib::RefPtr<Gdk::Pixbuf> pixbuf;
        try
        {
            pixbuf = iconTheme->load_icon(iconName, requestedIconSize, 
                        Gtk::ICON_LOOKUP_GENERIC_FALLBACK);
        }
        catch (const Gtk::IconThemeError &)
        {
            continue;
        }

        if (pixbuf->get_width() > 20*requestedIconSize ||
            pixbuf->get_height() > 20*requestedIconSize)
        {
            continue;
        }

        Gtk::Image *image = Gtk::manage(new Gtk::Image(pixbuf));
        Gtk::ToolButton *button = Gtk::manage
            (new Gtk::ToolButton(*image, iconName));
        button->set_tooltip_text(iconName);
        button->set_is_important();
        group->insert(*button);      
        ++iconsCount;
        if(iconsCount >= maxIconsPerGroup)
            break;
    }
}

ExampleWindow::ExampleWindow()
:m_vBox(Gtk::ORIENTATION_VERTICAL, 1), m_hBox(Gtk::ORIENTATION_HORIZONTAL, 1)
{
    set_title("Example");
    set_size_request(10, 600);
    set_border_width(1);
    add(m_vBox);
    m_refTreeModelOrientation = Gtk::ListStore::create(m_columnsOrientation);
    Gtk::TreeModel::Row row = *(m_refTreeModelOrientation->append());
    loadIconItems();
    m_vBox.pack_start(m_hBox, Gtk::PACK_EXPAND_WIDGET);
    m_scrolledWindowPalette.set_policy(Gtk::POLICY_NEVER,
        Gtk::POLICY_AUTOMATIC);
    m_scrolledWindowPalette.set_border_width(6);
    m_scrolledWindowPalette.add(m_toolPalette);
    m_hBox.pack_start(m_scrolledWindowPalette);
    m_scrolledWindowCanvas.add(m_canvas);
    m_scrolledWindowCanvas.set_size_request(200, -1);;
    m_hBox.pack_start(m_scrolledWindowCanvas);
    m_toolPalette.add_drag_dest(m_canvas,
    Gtk::DEST_DEFAULT_HIGHLIGHT, Gtk::TOOL_PALETTE_DRAG_ITEMS, 
        Gdk::ACTION_COPY);

    show_all_children();
}

ExampleWindow::~ExampleWindow()
{
}

