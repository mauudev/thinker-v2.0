#ifndef GTKMM_EXAMPLE_CANVAS_H
#define GTKMM_EXAMPLE_CANVAS_H

#include <gtkmm.h>

class Canvas : public Gtk::DrawingArea
{
public:
    Canvas();

    virtual ~Canvas();

    class CanvasItem
    {
    public:
        CanvasItem(Gtk::Widget *p_canvas, 
            Gtk::ToolButton *p_button,
            double p_x,
            double p_y)
        {
            Glib::ustring iconName(p_button->get_icon_name());
            if (iconName.empty())
            {
                iconName = p_button->get_label();
            }
            Glib::RefPtr<Gtk::IconTheme> iconTheme = 
                Gtk::IconTheme::get_for_screen(p_canvas->get_screen());
            int width = 0;
            int height = 0;
            Gtk::IconSize::lookup(Gtk::ICON_SIZE_DIALOG, width, height);
            this->m_pixbuf = iconTheme->load_icon(iconName, width, 
                Gtk::ICON_LOOKUP_GENERIC_FALLBACK);
            this->m_x = p_x;
            this->m_y = p_y;
        }

        Glib::RefPtr<Gdk::Pixbuf> m_pixbuf;
        double m_x;
        double m_y;
    };

    void itemDraw(const CanvasItem *p_item,
        const Cairo::RefPtr<Cairo::Context> &p_cairo,
        bool p_preview);

    bool on_draw(const Cairo::RefPtr<Cairo::Context> &p_cairo) override;

    void on_drag_data_received(const Glib::RefPtr<Gdk::DragContext> &p_context,
        int p_x,
        int p_y,
        const Gtk::SelectionData &p_selectionData,
        guint p_info,
        guint p_time) override;

    bool on_drag_motion(const Glib::RefPtr<Gdk::DragContext> &p_context, 
        int p_x,
        int p_y,
        guint p_time) override;

    bool on_drag_drop(const Glib::RefPtr<Gdk::DragContext> &p_context,
        int p_x,
        int p_y,
        guint p_time) override;



    bool m_dragDataRequestedForDrop;
    CanvasItem *m_dropItem;
    typedef std::vector<CanvasItem*> typeVecItems;
    typeVecItems m_canvasItems;
};

#endif
