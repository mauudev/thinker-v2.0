#include "examplewindow.h"
#include "examplebuttonbox.h"

ExampleWindow::ExampleWindow()
: m_vBoxMain(Gtk::ORIENTATION_VERTICAL),
    m_vBox(Gtk::ORIENTATION_VERTICAL),
    m_menuBar("MenuBar"),
    m_toolBoxCanvas(""),
    m_infoBar("InfoBar")
{
    set_title("Thinker");
    set_default_size(650, 350);
    set_border_width(6);
    add(m_vBoxMain);
    m_vBoxMain.pack_start(m_menuBar, Gtk::PACK_EXPAND_WIDGET, 5);
    m_vBox.set_border_width(5);
    m_vBoxMain.pack_start(m_toolBoxCanvas, Gtk::PACK_EXPAND_WIDGET, 5);
    m_hBox.set_border_width(5);

    m_toolBoxCanvas.add(m_hBox);
    m_hBox.pack_start(*Gtk::manage(new ExampleButtonBox(false, "ToolBox", 5,
        Gtk::BUTTONBOX_START)), Gtk::PACK_EXPAND_WIDGET,5);
    m_hBox.pack_start(*Gtk::manage(new ExampleButtonBox(false, "Canvas", 5,
        Gtk::BUTTONBOX_START)), Gtk::PACK_EXPAND_WIDGET, 5);
    m_hBox.pack_start(*Gtk::manage(new ExampleButtonBox(false, "ToolBox2", 5,
        Gtk::BUTTONBOX_START)), Gtk::PACK_EXPAND_WIDGET, 5);

    m_vBoxMain.pack_start(m_infoBar, Gtk::PACK_EXPAND_WIDGET, 5);
    m_vBox.set_border_width(5);
    m_infoBar.add(m_vBox);

    show_all_children();
}

ExampleWindow::~ExampleWindow()
{
}

