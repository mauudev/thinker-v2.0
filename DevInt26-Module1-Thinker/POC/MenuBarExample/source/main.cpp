#include "GUI.h"

int main(int argc, char *argv[])
{
    Glib::RefPtr<Gtk::Application> app = Gtk::Application::create();
    GUI mygui;
    return app->run(mygui);
}
