PROJECT(move_objets)

CMAKE_MINIMUM_REQUIRED(VERSION 3.0.0)

ADD_COMPILE_OPTIONS(-g -std=c++11)

FIND_PACKAGE(X11 REQUIRED)
FIND_PACKAGE(PkgConfig REQUIRED)

PKG_CHECK_MODULES(GTKMM REQUIRED gtkmm-3.0)
PKG_CHECK_MODULES(GLIB REQUIRED glib-2.0)

INCLUDE_DIRECTORIES(include)
INCLUDE_DIRECTORIES(${X11_INCLUDE_DIR})
INCLUDE_DIRECTORIES(${GTKMM_INCLUDE_DIRS})
INCLUDE_DIRECTORIES(${GLIB_INCLUDE_DIRS})

LINK_DIRECTORIES(${GTKMM_LIBRARY_DIRS})
LINK_DIRECTORIES(${GLIB_LDFLAGS})

ADD_DEFINITIONS(${GTKMM_CFLAGS_OTHER})

SET(src_program
    source/main.cpp
    source/drawing_canvas.cpp
    source/window_canvas.cpp
    )

ADD_EXECUTABLE(main ${src_program})

TARGET_LINK_LIBRARIES(main ${GTKMM_LIBRARIES})

