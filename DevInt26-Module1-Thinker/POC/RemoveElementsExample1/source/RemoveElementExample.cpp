#include "RemoveElementExample.h"
#include <iostream>
#include <unistd.h>

using namespace std;

ExampleButtonBox::ExampleButtonBox(bool horizontal,
    const Glib::ustring &title,
    gint spacing,
    Gtk::ButtonBoxStyle layout)
: Gtk::Frame(title),
    m_buttonRemove("Delete")
{
    Gtk::ButtonBox *buttonBox = nullptr;

    if(horizontal)
        buttonBox = Gtk::manage( new Gtk::ButtonBox(
            Gtk::ORIENTATION_HORIZONTAL) );
    else
        buttonBox = Gtk::manage( new Gtk::ButtonBox(
            Gtk::ORIENTATION_VERTICAL) );

    buttonBox->set_border_width(5);
    add(*buttonBox);

    buttonBox->set_layout(layout);
    buttonBox->set_spacing(spacing);
    buttonBox->add(m_buttonRemove);

    m_buttonRemove.signal_clicked().connect(
        sigc::bind<Gtk::ButtonBox*>(sigc::mem_fun(*this, 
            &ExampleButtonBox::on_click_remove_btn), buttonBox));

    for(int i = 1; i < 5; i++)
    {
        GenericButton *btn = new GenericButton(i,"Test Btn " + 
            std::to_string(i));
        btnList.push_back(btn);
        buttonBox->add(*btn);
    }
}

void ExampleButtonBox::on_click_remove_btn(Gtk::ButtonBox *buttonBox)
{
    int i = 0;
    while(i < btnList.size())
    {
        if(btnList[i]->get_active()){
            buttonBox->remove(*btnList[i]);
            btnList.erase(btnList.begin()+i);
            i = 0;
            continue;
        }
        i++;
        cout << i << endl;
        
    }
    cout << "Button " << " removed !" << " array size: " 
        << btnList.size() << "\n";
}

void ExampleButtonBox::removeElement(vector<GenericButton*> list, 
    Gtk::ButtonBox *buttonBox, int i)
{
    buttonBox->remove(*list[i]);
    list.erase(list.begin()+i);
}
