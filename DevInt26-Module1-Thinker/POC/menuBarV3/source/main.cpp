#include "window.h"

int main()
{
    Glib::RefPtr<Gtk::Application> app = Gtk::Application::create();
    Window window;
    return app->run(window);
}
