#ifndef ASTEP_H
#define ASTEP_H
#include <BuilderCodePy.h>
#include "ITool.h"
#include "INodeAST.h"

class AStep: public ITool, public INodeAST
{
public:
    AStep();

    virtual ~AStep();

    Statement &getStepASTNode(BuilderCodePy&);

    Statement &initStepASTNode(BuilderCodePy&);

    virtual Statement &setStepASTNode(BuilderCodePy&) = 0;

};

#endif
