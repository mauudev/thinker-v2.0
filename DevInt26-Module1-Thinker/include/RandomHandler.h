#ifndef RANDOM_HANDLER_H
#define RANDOM_HANDLER_H

#include <iostream>
#include <IDTO.h>
#include <json.hpp>
#include <string>
#include <PythonAST.h>
#include <BuilderCodePy.h>
#include <BuilderCode.h>
#include <PythonInjector.h>
#include <INodeAST.h>
#include <ToolType.h>
#include <AStep.h>


using namespace std;
using Json = nlohmann::json;

class RandomHandler: public IDTO, public AStep
{
private:
    string m_randomName;

public:
    RandomHandler();
    RandomHandler(const string&);
    ~RandomHandler();

    void setRandomName(const string&);
    const string getRandomName() const;

    ToolType getClassName() const override;
    const string getIToolName() const override;

    Json serialize();
    void deserialize(Json&);

    Statement &generateASTInitNode(BuilderCodePy&) override;
    Statement &generateASTBodyNode(BuilderCodePy&) override;
    Statement &generateASTClassNode(BuilderCodePy&) override;

    Statement &setStepASTNode(BuilderCodePy&) override;

private:
    CallFunction &choiceKeyASTNode(BuilderCodePy&);
    const string buildMovesArray() const;

};

#endif
