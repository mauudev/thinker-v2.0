#ifndef ILINKED_H
#define ILINKED_H

#include <memory>
#include "ITool.h"

class ILinked
{
public:

    ILinked(){}

    virtual ~ILinked(){}

    virtual void link(shared_ptr<ITool>) = 0;

};

#endif