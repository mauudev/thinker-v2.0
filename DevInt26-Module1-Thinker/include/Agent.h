#ifndef AGENT_H
#define AGENT_H

#include <string>
#include <map>
#include <vector>
#include <memory>
#include <IDTO.h>
#include <json.hpp>
#include <INodeAST.h>
#include <ITool.h>
#include <ToolType.h>
#include "Output.h"
#include "KeyboardHandler.h"
#include "OpenAI.h"
#include "Input.h"
#include "ScreenHandler.h"
#include "TensorFlow.h"
#include "RandomHandler.h"

using namespace std;
using Json = nlohmann::json;

class Agent: public IDTO
{
private:
    string m_agentName;

    map<string, shared_ptr<ITool>> m_toolMap;
    map<string, shared_ptr<IDTO>> m_persistenceMap;

    bool addToolToMap(const string &p_toolName, shared_ptr<ITool>);
    void addIdtoToMap(const string &p_toolName, shared_ptr<IDTO>);

    shared_ptr<ITool> getTool(const string&) const;

    void validateToolType(const ITool&, ToolType) const;

public:
    Agent();
    Agent(const string&);
    ~Agent();

    bool addNewOutput(shared_ptr<Output>);
    bool addNewOpenAI(shared_ptr<OpenAI>);
    bool addNewInput(shared_ptr<Input>);
    bool addNewScreenHandler(shared_ptr<ScreenHandler>);
    bool addNewKeyboardHandler(shared_ptr<KeyboardHandler>);
    bool addNewTensorFlow(shared_ptr<TensorFlow>);
    bool addNewRandomHandler(shared_ptr<RandomHandler>);

    Output &getOutput(const string&) const;
    OpenAI &getOpenAI(const string&) const;
    Input &getInput(const string&) const;
    ScreenHandler &getScreenHandler(const string&) const;
    KeyboardHandler &getKeyboardHandler(const string&) const;
    TensorFlow &getTensorFlow(const string&) const;
    RandomHandler &getRandomHandler(const string&) const;

    void setAgentName(const string&);
    const string getAgentName() const;

    bool removeTool(const string&);
    bool linkTools(const string&, const string&);

    Json serialize();
    void deserialize(Json&);

};
#endif
