#ifndef TENSOR_FLOW_H
#define TENSOR_FLOW_H

#include <string>
#include <IDTO.h>
#include <json.hpp>
#include <INodeAST.h>
#include <AStep.h>
#include <ToolType.h>

using namespace std;
using Json = nlohmann::json;

class TensorFlow: public IDTO, public AStep
{
    string m_tensorFlowName;
public:

    TensorFlow(const string&);

    ToolType getClassName() const override;

    const string getIToolName() const override;

    Json serialize();

    void deserialize(Json&);

    Statement &setStepASTNode(BuilderCodePy&) override;

    Statement &setASTTensorFlowName(BuilderCodePy&);

    Statement &getASTTensorFlowName(BuilderCodePy&);

    Statement &generateASTInitNode(BuilderCodePy&) override;

    Statement &generateASTBodyNode(BuilderCodePy&) override;

    Statement &generateASTClassNode(BuilderCodePy&) override;
};

#endif
