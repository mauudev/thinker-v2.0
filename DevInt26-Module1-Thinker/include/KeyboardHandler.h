#ifndef KEYBOARD_HANDLER_H
#define KEYBOARD_HANDLER_H

#include <iostream>
#include <IDTO.h>
#include <json.hpp>
#include <string>
#include <PythonAST.h>
#include <BuilderCodePy.h>
#include <BuilderCode.h>
#include <PythonInjector.h>
#include <INodeAST.h>
#include <ToolType.h>
#include <AStep.h>

using namespace std;
using Json = nlohmann::json;

class KeyboardHandler: public IDTO, public AStep
{
private:
    string m_keyboardName;

public:
    KeyboardHandler(const string&);

    ToolType getClassName() const override;
    const string getIToolName() const override;

    Json serialize();
    void deserialize(Json&);

    Statement &setStepASTNode(BuilderCodePy&) override;

    Statement &generateASTInitNode(BuilderCodePy&) override;
    Statement &generateASTBodyNode(BuilderCodePy&) override;
    Statement &generateASTClassNode(BuilderCodePy&) override;

private:

    Expression &bodyForEventTypeCompare(BuilderCodePy&);
    Statement &bodyForEventType(BuilderCodePy&);
    Statement &ifSteps(BuilderCodePy&);
    Statement &eventFor(BuilderCodePy&);

    Statement &AssignKeyUp(BuilderCodePy&);
    Statement &AssignKeyDown(BuilderCodePy&);
    Statement &AssignKeyLeft(BuilderCodePy&);
    Statement &AssignKeyRight(BuilderCodePy&);

    Expression &setStepUpASTNode(BuilderCodePy&);
    Expression &setStepWASTNode(BuilderCodePy&);
    Expression &setStepKP5ASTNode(BuilderCodePy&);
    Expression &setStepDownASTNode(BuilderCodePy&);
    Expression &setStepDownSASTNode(BuilderCodePy&);
    Expression &setStepDownKP2ASTNode(BuilderCodePy&);
    Expression &setStepLeftASTNode(BuilderCodePy&);
    Expression &setStepLeftAASTNode(BuilderCodePy&);
    Expression &setStepLeftKP1ASTNode(BuilderCodePy&);
    Expression &setStepRightASTNode(BuilderCodePy&);
    Expression &setStepRightDASTNode(BuilderCodePy&);
    Expression &setStepRightKP3ASTNode(BuilderCodePy&);
};

#endif
