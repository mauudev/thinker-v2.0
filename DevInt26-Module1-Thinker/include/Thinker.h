#ifndef THINKER_H
#define THINKER_H

#include "Project.h"
#include "ProjectToolbox.h"
#include "AgentToolbox.h"
#include <memory>

using namespace std;

class Thinker
{
private:
    unique_ptr<ProjectToolbox> m_projectToolbox;
    unique_ptr<AgentToolbox> m_agentToolbox;
    unique_ptr<Project> m_project;

    Thinker();
    ~Thinker();

public:
    static Thinker &getInstance();

    Project &newProject();

    ProjectToolbox &getProjectToolbox() const;

    AgentToolbox &getAgentToolbox() const;

    Project &getProject() const;
};

#endif