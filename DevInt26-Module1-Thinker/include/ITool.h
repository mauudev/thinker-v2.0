#ifndef ITOOL_H
#define ITOOL_H

#include <string>
#include <ToolType.h>

using namespace std;

class ITool
{
public:

    ITool(){}

    virtual ~ITool(){}

    virtual ToolType getClassName() const = 0;

    virtual const string getIToolName() const = 0;
};

#endif
