#ifndef EXCEPTION_NODE_H
#define EXCEPTION_NODE_H
#include <exception>
#include <string>

using namespace std;

class ExceptionNode: public exception
{
private:
    string m_sms = "Error: The configuration of the nodes is incomplete";

public:
    ExceptionNode();

    ExceptionNode(const string&);

    const char *what() const noexcept override;
};
#endif
