#ifndef AGENT_TOOLBOX_H
#define AGENT_TOOLBOX_H

#include <memory>
#include "Output.h"
#include "KeyboardHandler.h"
#include "OpenAI.h"
#include "Input.h"
#include "ScreenHandler.h"
#include "TensorFlow.h"
#include "RandomHandler.h"

using namespace std;

class AgentToolbox
{
public:
    AgentToolbox();

    ~AgentToolbox();


    shared_ptr<Output> getNewOutput(const string&) const;

    shared_ptr<OpenAI> getNewOpenAI(const string&) const;

    shared_ptr<Input> getNewInput(const string&) const;

    shared_ptr<ScreenHandler> getNewScreenHandler(const int&, const int&,
        const int&, const string&) const;

    shared_ptr<KeyboardHandler> getNewKeyboardHandler(const string&) const;

    shared_ptr<TensorFlow> getNewTensorFlow(const string&) const;

    shared_ptr<RandomHandler> getNewRandomHandler(const string&) const;
};

#endif
