#ifndef INPUT_H
#define INPUT_H

#include <BuilderCodePy.h>
#include <iostream>
#include <fstream>
#include <INodeAST.h>
#include <IDTO.h>
#include <json.hpp>
#include <ToolType.h>
#include <ITool.h>

using namespace std;
using Json = nlohmann::json;

class Input : public IDTO, public INodeAST, public ITool
{
private:
    string m_inputName;
    int m_nextStep = 0;

public:
    Input(const string&);

    void setNextStep(const int&);

    const int getNextStep() const;

    ToolType getClassName() const override;
    const string getIToolName() const override;

    Json serialize();
    void deserialize(Json&);

    Statement &generateASTInitNode(BuilderCodePy&) override;
    Statement &generateASTBodyNode(BuilderCodePy&) override;
    Statement &generateASTClassNode(BuilderCodePy&) override;

private:
    Assignment &initInputNameASTNode(BuilderCodePy&);
    Assignment &initNextStepASTNode(BuilderCodePy&);
    Statement &setNextStepASTNode(BuilderCodePy&);
    FunctionDef &setInputNameASTNode(BuilderCodePy&);
    Statement &getNextStepASTNode(BuilderCodePy&);
    FunctionDef &getInputNameASTNode(BuilderCodePy&);
};
#endif
