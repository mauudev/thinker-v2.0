#ifndef SCREENHANDLER_H
#define SCREENHANDLER_H

#include <string>
#include <IDTO.h>
#include <json.hpp>
#include <PythonAST.h>
#include <BuilderCodePy.h>
#include <BuilderCode.h>
#include <PythonInjector.h>
#include <INodeAST.h>
#include <ToolType.h>
#include <ITool.h>

using namespace std;
using Json = nlohmann::json;

class ScreenHandler: public IDTO, public INodeAST, public ITool
{
private:
    int m_screenSpeed;
    int m_screenHeight;
    int m_screenWidth;
    string m_screenName;

public:
    ScreenHandler(const int&, const int&, const int&, const string&);

    ToolType getClassName() const override;

    const string getIToolName() const override;

    void setScreenHandlerSpeed(const int&);

    void setScreenHandlerHeight(const int&);

    void setScreenHandlerWidth(const int&);

    const int getScreenHandlerSpeed() const;

    const int getScreenHandlerHeight() const;

    const int getScreenHandlerWidth() const;

    const string generateScreenHandlerSize() const;

    Expression &displayArraySpeed(BuilderCodePy&);

    Expression &displayArrayPyGameScreen(BuilderCodePy&);

    Expression &displayArrayCallGetDisplay(BuilderCodePy&);

    Statement &displayArrayTupleRgbArrayAssignation(BuilderCodePy&);

    Statement &displayArrayAssingRgbArray(BuilderCodePy&);

    Statement &displayArrayAssignPyGameImageIfTrue(BuilderCodePy&);

    Statement &displayArrayAssignPyGameImage(BuilderCodePy&);

    Statement &getDisplay(BuilderCodePy&);

    Statement &closeGame(BuilderCodePy&);

    Statement &setDisplayArray(BuilderCodePy&);

    Statement &initScreenSpeed(BuilderCodePy&);

    Statement &initScreenSize(BuilderCodePy&);

    Statement &initClock(BuilderCodePy&);

    Statement &initPygameScreen(BuilderCodePy&);

    Statement &initPygameImage(BuilderCodePy&);

    Statement &generateImportPyGame(BuilderCodePy&);

    Statement &generateImportPyGameLocals(BuilderCodePy&);

    Statement &concatImports(BuilderCodePy&);

    Statement &generateClassNode(BuilderCodePy&);

    Statement &generateASTInitNode(BuilderCodePy&) override;

    Statement &generateASTBodyNode(BuilderCodePy&) override;

    Statement &generateASTClassNode(BuilderCodePy&) override;

    Json serialize();

    void deserialize(Json&);

};

#endif
