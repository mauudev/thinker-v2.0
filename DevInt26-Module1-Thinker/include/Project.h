#ifndef PROJECT_H
#define PROJECT_H

#include <iostream>
#include <json.hpp>
#include <Persistence.h>
#include <map>
#include <string>
#include <memory>
#include "Agent.h"

using Json = nlohmann::json;

class Project: public Persistence
{
private:
    string m_projectName;
    map<string, shared_ptr<Agent>> m_agentMap;

public:
    Project();

    ~Project();

    string getProjectName() const;

    void setProjectName(const string&);

    bool addNewAgent(shared_ptr<Agent>);

    Agent &getAgent(const string&) const;

    Json serialize();

    void deserialize(Json&);
};

#endif
