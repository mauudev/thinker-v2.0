#ifndef OPEN_AI_H
#define OPEN_AI_H

#include <string>
#include <memory>
#include <IDTO.h>
#include <json.hpp>
#include <INodeAST.h>
#include <ITool.h>
#include <ToolType.h>
#include <ILinked.h>
#include <ScreenHandler.h>
#include <AStep.h>
#include <KeyboardHandler.h>
#include <RandomHandler.h>
#include <TensorFlow.h>

using namespace std;
using Json = nlohmann::json;

class OpenAI: public IDTO, public INodeAST, public ITool, public ILinked
{
private:
    string m_openAIName;
    int m_numberCoins;
    vector<string> m_gamesOfOpenAI;
    weak_ptr<ScreenHandler> m_screenHandler;
    weak_ptr<AStep> m_aStep;

public:
    OpenAI(const string&);

    vector<string> getVectorGame();

    ToolType getClassName() const override;
    const string getIToolName() const override;

    void link(shared_ptr<ITool>) override;

    Json serialize();
    void deserialize(Json&);

    Statement &generateASTInitNode(BuilderCodePy&) override;
    Statement &generateASTBodyNode(BuilderCodePy&) override;
    Statement &generateASTClassNode(BuilderCodePy&) override;

private:
    Statement &generateLinksASTNodes(BuilderCodePy&);
    Statement &generateAStepASTNode(BuilderCodePy&);
    Statement &generateScreenASTNode(BuilderCodePy&);
    Assignment &initOpenAINameASTNode(BuilderCodePy&);
    Assignment &initNumberCoinsOpenAIASTNode(BuilderCodePy&);
    Assignment &initCounterGamesOpenAIASTNode(BuilderCodePy&);
    Assignment &initDoneOpenAIASTNode(BuilderCodePy&);
    Assignment &initRGBArrayOpenAIASTNode(BuilderCodePy&);
    Assignment &initRewardOpenAIASTNode(BuilderCodePy&);
    Assignment &initObservationsOpenAIASTNode(BuilderCodePy&);
    Assignment &initEnvironmentOpenAIASTNode(BuilderCodePy&);
    Assignment &initEnvironmentActionSpaceOpenAIASTNode(BuilderCodePy&);
    CallFunction &initResetEnvironmentOpenAIASTNode(BuilderCodePy&);

    FunctionDef &getStatusOpenAIASTNode(BuilderCodePy&);
    If &buildIfCounterGamesStatusASTNode(BuilderCodePy&);
    Assignment &assignDoneASTNode(BuilderCodePy&);
    AugAssign &incrementCounterGamesASTNode(BuilderCodePy&);

    Statement &getRGBArrayASTNode(BuilderCodePy&);

    Statement &resetEnvironmentASTNode(BuilderCodePy&);
    Assignment &buildEnviromentASTNode(BuilderCodePy&);

    void gameInitializer();
};
#endif
