#ifndef OUTPUT_H
#define OUTPUT_H

#include <BuilderCodePy.h>
#include <iostream>
#include <fstream>
#include <json.hpp>
#include <INodeAST.h>
#include <IDTO.h>
#include <ToolType.h>
#include <ITool.h>

using Json = nlohmann::json;
using namespace std;

class Output: public IDTO, public INodeAST, public ITool
{
private:
    string m_outputName;
    int m_output = 0;

public:
    Output(const string&);

    void setOutput(const int&);

    const int getOutput() const;

    ToolType getClassName() const override;
    const string getIToolName() const override;

    Json serialize();
    void deserialize(Json&);

    Statement &generateASTInitNode(BuilderCodePy&) override;
    Statement &generateASTBodyNode(BuilderCodePy&) override;
    Statement &generateASTClassNode(BuilderCodePy&) override;

private:
    Assignment &initOutputNameASTNode(BuilderCodePy&);
    Assignment &initOutputASTNode(BuilderCodePy&);
    Statement &setOutputASTNode(BuilderCodePy&);
    FunctionDef &setOutputNameASTNode(BuilderCodePy&);
    Statement &getOutputASTNode(BuilderCodePy&);
    FunctionDef &getOutputNameASTNode(BuilderCodePy&);
};
#endif
