#ifndef PROJECT_TOOLBOX_H
#define PROJECT_TOOLBOX_H

#include <string>
#include <memory>

#include "Agent.h"
#include "Toolbox.h"
#include "OpenAI.h"

using namespace std;

class ProjectToolbox : public Toolbox
{
public:
    ProjectToolbox();

    ~ProjectToolbox();

    shared_ptr<Agent> getNewAgent(const string&) const;

};

#endif
