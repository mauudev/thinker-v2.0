#ifndef TOOL_TYPE_H
#define TOOL_TYPE_H

enum ToolType
{
    e_screenHandler,
    e_keyboardHandler,
    e_randomHandler,
    e_tensorFlow,
    e_input,
    e_output,
    // All the classes that implement ILinked should go after e_openAI
    e_openAI
    // e_scriptPython
};

#endif