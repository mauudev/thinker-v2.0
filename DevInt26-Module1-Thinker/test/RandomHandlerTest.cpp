#include "gtest/gtest.h"
#include "Thinker.h"
#include <iostream>

TEST(RandomHandlerIsNotNull, myRandomHandlerTest)
{
    Project *project = Thinker::getInstance()->newProject();
    string agentName = "agente de prueba";
    project->addNewAgent(Thinker::getInstance()->getProjectToolbox()
        ->getNewAgent(agentName));
    project->getAgent(agentName)->addNewRandomHandler(Thinker::getInstance()
        ->getAgentToolbox()->getNewRandomHandler("RandomHandler 1"));

    ASSERT_NE(nullptr, project->getAgent(agentName)->getRandomHandler());
}

TEST(RandomHandlerIsNull, myRandomHandlerTest)
{
    Project *project = Thinker::getInstance()->newProject();
    string agentName = "agente de prueba";
    project->addNewAgent(Thinker::getInstance()->getProjectToolbox()
        ->getNewAgent(agentName));

    ASSERT_EQ(nullptr, project->getAgent(agentName)->getRandomHandler());
}

TEST(CompareDirectionMemoryRandomHandler, myRandomHandlerTest)
{
    Project *project = Thinker::getInstance()->newProject();
    string agentName = "agente de prueba";
    RandomHandler *m_RandomHandler = new RandomHandler("Random 1");
    project->addNewAgent(Thinker::getInstance()->getProjectToolbox()
        ->getNewAgent(agentName));
    project->getAgent(agentName)->addNewRandomHandler(m_RandomHandler);

    ASSERT_EQ(m_RandomHandler, project->getAgent(agentName)->getRandomHandler());
}

TEST(VerifyOneInstanceRandomHandler, myRandomHandlerTest)
{
    RandomHandler *m_RandomHandler = new RandomHandler("Random 1");
    RandomHandler *m_RandomHandler1 = new RandomHandler("Random 2");
    Project *project = Thinker::getInstance()->newProject();
    string agentName = "agente de prueba";
    project->addNewAgent(Thinker::getInstance()->getProjectToolbox()
        ->getNewAgent(agentName));
    project->getAgent(agentName)->addNewRandomHandler(m_RandomHandler);
    project->getAgent(agentName)->addNewRandomHandler(m_RandomHandler1);

    ASSERT_EQ(m_RandomHandler1, project->getAgent(agentName)->getRandomHandler());
}