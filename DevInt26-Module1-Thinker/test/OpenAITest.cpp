#include "gtest/gtest.h"
#include "Thinker.h"

TEST(GetCustomOpenAI, myOpenAITest)
{
    Project *project = Thinker::getInstance()->newProject();
    string agentName = "agente de prueba";
    project->addNewAgent(Thinker::getInstance()->getProjectToolbox()
    ->getNewAgent(agentName));

    string gameName = "MsPacMan-ram-v0";
    project->getAgent(agentName)
        ->addNewOpenAI(Thinker::getInstance()->getAgentToolbox()
        ->getNewOpenAI(gameName));

    ASSERT_EQ(gameName, project->getAgent(agentName)->getOpenAI()
    ->getOpenAIName());
}

TEST(IfOpenAIisNull, myOpenAITest)
{
    Project *project = Thinker::getInstance()->newProject();
    string agentName = "agente de prueba";
    project->addNewAgent(Thinker::getInstance()->getProjectToolbox()
    ->getNewAgent(agentName));

    Agent *m_agent = project->getAgent(agentName);

    ASSERT_EQ (m_agent->getOpenAI(), nullptr);
}

TEST(TestOneOpenAI, myOpenAITest)
{
    Project *project = Thinker::getInstance()->newProject();
    string agentName = "agente de prueba";
    project->addNewAgent(Thinker::getInstance()->getProjectToolbox()
        ->getNewAgent(agentName));

    string gameName = "MsPacMan-ram-v0";
    string gameName1 = "MsPacManROA";
    project->getAgent(agentName)->
    addNewOpenAI(Thinker::getInstance()->getAgentToolbox()
        ->getNewOpenAI(gameName));

    project->getAgent(agentName)->
    addNewOpenAI(Thinker::getInstance()->getAgentToolbox()
        ->getNewOpenAI(gameName1));

    ASSERT_NE(gameName, project->getAgent(agentName)->getOpenAI()
    ->getOpenAIName());
}

TEST(ItoolOpenAI, getClassNameOpenAI)
{
    Project *project = Thinker::getInstance()->newProject();
    string agentName = "agente de prueba";
    project->addNewAgent(Thinker::getInstance()->getProjectToolbox()
    ->getNewAgent(agentName));

    string gameName = "MsPacMan-ram-v0";
    project->getAgent(agentName)
        ->addNewOpenAI(Thinker::getInstance()->getAgentToolbox()
        ->getNewOpenAI(gameName));

    ASSERT_EQ("OpenAI", project->getAgent(agentName)->getOpenAI()
    ->getClassName());
}