#include "gtest/gtest.h"
#include "Thinker.h"
#include <iostream>

TEST(KeyboardHandlerIsNotNull, myKeyboardHandlerTest)
{
    Project *project = Thinker::getInstance()->newProject();
    string agentName = "agente de prueba";
    project->addNewAgent(Thinker::getInstance()->getProjectToolbox()
        ->getNewAgent(agentName));
    project->getAgent(agentName)->addNewKeyboardHandler(Thinker::getInstance()
        ->getAgentToolbox()->getNewKeyboardHandler("KeyboardHandler 1"));

    ASSERT_NE(nullptr, project->getAgent(agentName)->getKeyboardHandler());
}

TEST(KeyboardHandlerIsNull, myKeyboardHandlerTest)
{
    Project *project = Thinker::getInstance()->newProject();
    string agentName = "agente de prueba";
    project->addNewAgent(Thinker::getInstance()->getProjectToolbox()
        ->getNewAgent(agentName));

    ASSERT_EQ(nullptr, project->getAgent(agentName)->getKeyboardHandler());
}

TEST(CompareDirectionMemoryKeyboardHandler, myKeyboardHandlerTest)
{
    Project *project = Thinker::getInstance()->newProject();
    string agentName = "agente de prueba";
    KeyboardHandler *m_keyboardHandler = new KeyboardHandler("Keyboard 1");
    project->addNewAgent(Thinker::getInstance()->getProjectToolbox()
        ->getNewAgent(agentName));
    project->getAgent(agentName)->addNewKeyboardHandler(m_keyboardHandler);

    ASSERT_EQ(m_keyboardHandler, project->getAgent(agentName)->getKeyboardHandler());
}

TEST(VerifyOneInstanceKeyboardHandler, myKeyboardHandlerTest)
{
    KeyboardHandler *m_keyboardHandler = new KeyboardHandler("Keyboard 1");
    KeyboardHandler *m_keyboardHandler1 = new KeyboardHandler("Keyboard 2");
    Project *project = Thinker::getInstance()->newProject();
    string agentName = "agente de prueba";
    project->addNewAgent(Thinker::getInstance()->getProjectToolbox()
        ->getNewAgent(agentName));
    project->getAgent(agentName)->addNewKeyboardHandler(m_keyboardHandler);
    project->getAgent(agentName)->addNewKeyboardHandler(m_keyboardHandler1);

    ASSERT_EQ(m_keyboardHandler1, project->getAgent(agentName)->getKeyboardHandler());
}

TEST(IToolKeyboard, getClassNameKeyboardHandler)
{
    Project *project = Thinker::getInstance()->newProject();
    string agentName = "agente de prueba";
    KeyboardHandler *m_keyboardHandler = new KeyboardHandler("Keyboard 1");
    project->addNewAgent(Thinker::getInstance()->getProjectToolbox()
        ->getNewAgent(agentName));
    project->getAgent(agentName)->addNewKeyboardHandler(m_keyboardHandler);

    ASSERT_EQ("KeyboardHandler", project->getAgent(agentName)->getKeyboardHandler()->getClassName());
}