#include "gtest/gtest.h"
#include "Thinker.h"

using namespace std;

TEST(GetNewInputTest, GetNewInput)
{
    Project *m_project = Thinker::getInstance()->newProject();
    const string agentName = "agente de prueba";
    ProjectToolbox *m_projectToolbox = Thinker::getInstance()
    ->getProjectToolbox();
    Agent *m_agentNew = m_projectToolbox->getNewAgent(agentName);
    AgentToolbox *m_agentToolbox = Thinker::getInstance()->getAgentToolbox();
    Input *m_agentInput = m_agentToolbox->getNewInput();
    Input *m_agentOfInput = m_agentNew->getInput();
    ASSERT_NE(m_agentInput, nullptr);
}

TEST(GetInputTest, GetInput)
{
    Project *m_project = Thinker::getInstance()->newProject();
    const string agentName = "agente de prueba";
    ProjectToolbox *m_projectToolbox = Thinker::getInstance()
    ->getProjectToolbox();
    Agent *m_agentNew = m_projectToolbox->getNewAgent(agentName);
    AgentToolbox *m_agentToolbox = Thinker::getInstance()->getAgentToolbox();
    Input *m_agentInput = m_agentToolbox->getNewInput();
    Input *m_agentOfInput = m_agentNew->getInput();
    ASSERT_EQ(m_agentOfInput, m_agentNew->getInput());
}

TEST(InputTest, VerifyNotInputAgent)
{
    Project *m_project = Thinker::getInstance()->newProject();
    ProjectToolbox *m_projectToolbox = Thinker::getInstance()
        ->getProjectToolbox();
    string m_agentName = "agente de prueba";
    Agent *m_agent = m_projectToolbox->getNewAgent(m_agentName);
    m_project->addNewAgent(m_agent);
    Agent *m_agentOfProject = m_project->getAgent(m_agentName);
    AgentToolbox *m_agentToolbox = Thinker::getInstance()->getAgentToolbox();
    Input *m_agentInput1 = m_agentToolbox->getNewInput();
    ASSERT_EQ (m_agentOfProject->getInput(), nullptr);
}

TEST(CreateOneInputTest, VerifyAddNewInputAgent)
{
    Project *m_project = Thinker::getInstance()->newProject();
    ProjectToolbox *m_projectToolbox = Thinker::getInstance()
        ->getProjectToolbox();
    string m_agentName = "agente de prueba";
    Agent *m_agent = m_projectToolbox->getNewAgent(m_agentName);
    m_project->addNewAgent(m_agent);
    Agent *m_agentOfProject = m_project->getAgent(m_agentName);
    AgentToolbox *m_agentToolbox = Thinker::getInstance()->getAgentToolbox();
    Input *m_agentInput1 = m_agentToolbox->getNewInput();
    Input *m_agentInputReference0 = m_agentOfProject
        ->getInput();
    m_agentOfProject->addNewInput(m_agentInput1);
    ASSERT_NE (m_agentOfProject->getInput(), nullptr);
}

TEST(CreateTwoInputTest, VerifyAddNewInputAgent)
{
    Project *m_project = Thinker::getInstance()->newProject();
    ProjectToolbox *m_projectToolbox = Thinker::getInstance()
        ->getProjectToolbox();
    string m_agentName = "agente de prueba";
    Agent *m_agent = m_projectToolbox->getNewAgent(m_agentName);
    m_project->addNewAgent(m_agent);
    Agent *m_agentOfProject = m_project->getAgent(m_agentName);
    AgentToolbox *m_agentToolbox = Thinker::getInstance()->getAgentToolbox();
    Input *m_agentInput1 = m_agentToolbox->getNewInput();
    Input *m_agentInputReference0 = m_agentOfProject
        ->getInput();
    m_agentOfProject->addNewInput(m_agentInput1);
    Input *m_agentInput2 = m_agentToolbox->getNewInput();
    m_agentOfProject->addNewInput(m_agentInput2);
    ASSERT_NE (m_agentOfProject->getInput(), m_agentInput1);
}
