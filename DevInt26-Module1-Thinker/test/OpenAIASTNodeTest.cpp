#include "gtest/gtest.h"
#include "Thinker.h"
#include "OpenAI.h"
#include "Agent.h"
#include <BuilderCodePy.h>
#include <PythonAST.h>
#include <BuilderCode.h>
#include <PythonInjector.h>
#include <string>

TEST(classNodeOpenAI, areEquals)
{
    Project &project = Thinker::getInstance().newProject();
    string agentName = "agente de prueba";
    project.addNewAgent(Thinker::getInstance().getProjectToolbox()
    .getNewAgent(agentName));

    string gameName = "MsPacMan-ram-v0";
    project.getAgent(agentName)
        .addNewOpenAI(Thinker::getInstance().getAgentToolbox()
        .getNewOpenAI(gameName));

    int screenSpeed = 25;
    int screenHeight = 400;
    int screenWidth = 500;
    const string screenName = "Screen Handler";

    project.getAgent(agentName)
        .addNewScreenHandler(Thinker::getInstance().getAgentToolbox()
        .getNewScreenHandler(screenSpeed, screenHeight, screenWidth, screenName));

    string keyboardHandlerName = "First KeyboardHandler";

    project.getAgent(agentName)
        .addNewKeyboardHandler(Thinker::getInstance().getAgentToolbox()
        .getNewKeyboardHandler(keyboardHandlerName));

    project.getAgent(agentName).linkTools(gameName, screenName);
    project.getAgent(agentName).linkTools(gameName, keyboardHandlerName);

    OpenAI &openIAtest = (project.getAgent(agentName)).getOpenAI(gameName);

    const string pythonCodeExpected = ""
    "\nimport gym.spaces\n"
    "class OpenAI(object):\n"
    "\tdef __init__(self):\n"
    "\t\tself.openAIName = \"MsPacMan-ram-v0\"\n"
    "\t\tself.numberCoins = 3\n"
    "\t\tself.counterGame = 1\n"
    "\t\tself.done = False\n"
    "\t\tself.rgbArray = 0\n"
    "\t\tself.reward = 0\n"
    "\t\tself.observations = 0\n"
    "\t\tself.environment = gym.make(self.openAIName)\n"
    "\t\tself.environmentActionSpace = self.environment.action_space\n"
    "\t\tself.environment.reset()\n"
    "\t\n"
    "\tdef getStatusOpenAI(self):\n"
    "\t\tif self.done:\n"
    "\t\t\tif self.counterGame < self.numberCoins:\n"
    "\t\t\t\tself.done = False\n"
    "\t\t\t\tself.counterGame += 1 \n"
    "\t\t\t\tself.environment.reset()\n"
    "\t\treturn self.done\n"
    "\n"
    "\tdef getRGBArray(self):\n"
    "\t\treturn self.rgbArray\n"
    "\n"
    "\tdef resetEnvironment(self, step):\n"
    "\t\tself.observations, self.reward, self.done, self.rgbArray = self.environment.step(step)\n"
    "\n\n"
    "class KeyboardHandler(object):\n"
    "\tdef __init__(self):\n"
    "\t\tself.step = 0\n"
    "\t\n"
    "\tdef getStep(self):\n"
    "\t\treturn self.step\n"
    "\n"
    "\tdef setStep(self):\n"
    "\t\tfor event in pygame.event.get() :\n"
    "\t\t\tif event.type == pygame.KEYDOWN:\n"
    "\t\t\t\tif event.key == K_UP:\n"
    "\t\t\t\t\tself.step = 1\n"
    "\t\t\t\telif event.key == K_w:\n"
    "\t\t\t\t\tself.step = 1\n"
    "\t\t\t\telif event.key == K_KP5:\n"
    "\t\t\t\t\tself.step = 1\n"
    "\t\t\t\telif event.key == K_DOWN:\n"
    "\t\t\t\t\tself.step = 4\n"
    "\t\t\t\telif event.key == K_s:\n"
    "\t\t\t\t\tself.step = 4\n"
    "\t\t\t\telif event.key == K_KP2:\n"
    "\t\t\t\t\tself.step = 4\n"
    "\t\t\t\telif event.key == K_LEFT:\n"
    "\t\t\t\t\tself.step = 3\n"
    "\t\t\t\telif event.key == K_a:\n"
    "\t\t\t\t\tself.step = 3\n"
    "\t\t\t\telif event.key == K_KP1:\n"
    "\t\t\t\t\tself.step = 3\n"
    "\t\t\t\telif event.key == K_RIGHT:\n"
    "\t\t\t\t\tself.step = 2\n"
    "\t\t\t\telif event.key == K_d:\n"
    "\t\t\t\t\tself.step = 2\n"
    "\t\t\t\telif event.key == K_KP3:\n"
    "\t\t\t\t\tself.step = 2\n"
    "\n\n\n"
    "import pygame\n"
    "from pygame.locals import *\n"
    "\n"
    "class ScreenHandler(object):\n"
    "\tdef __init__(self):\n"
    "\t\tself.screenSpeed = 25\n"
    "\t\tself.screenSize = (500,400)\n"
    "\t\tself.clock = pygame.time.Clock()\n"
    "\t\tself.pygameScreen = pygame.display.set_mode(self.screenSize)\n"
    "\t\tself.pygameImage = 0\n"
    "\t\n"
    "\tdef closeGame(self):\n"
    "\t\tpygame.quit()\n"
    "\tdef getDisplay(self):\n"
    "\t\tpygame.display.flip()\n"
    "\tdef setDisplayArray(self, rgbArray):\n"
    "\t\tself.clock.tick(self.screenSpeed)\n"
    "\t\trgbArrayMin, rgbArrayMax = rgbArray.min(), rgbArray.max()\n"
    "\t\trgbArray = ((255.0*(rgbArray-rgbArrayMin))/(rgbArrayMax-rgbArrayMin))\n"
    "\t\tpygameImage = pygame.surfarray.make_surface(rgbArray.swapaxes(0, 1) if True else rgbArray)\n"
    "\t\tpygameImage = pygame.transform.scale(pygameImage, self.screenSize)\n"
    "\t\tself.pygameScreen.blit(pygameImage, (0,0))\n"
    "\t\tself.getDisplay()";

    BuilderCodePy py;
    py.addCode(&openIAtest.generateASTClassNode(py));
    const string pythonCode = py.generateCode();

    ASSERT_EQ(pythonCodeExpected, pythonCode);
}
