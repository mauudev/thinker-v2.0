#include "gtest/gtest.h"
#include "Thinker.h"
#include "Output.h"
#include "Agent.h"
#include <BuilderCodePy.h>
#include <PythonAST.h>
#include <string>

TEST(classNodeOutput, AreEquals)
{
    Project *m_project = Thinker::getInstance()->newProject();
    ProjectToolbox *m_projectToolbox = Thinker::getInstance()
        ->getProjectToolbox();
    string m_agentName = "agente de prueba";
    Agent *m_agent = m_projectToolbox->getNewAgent(m_agentName);
    m_project->addNewAgent(m_agent);
    Agent *m_agentOfProject = m_project->getAgent(m_agentName);
    AgentToolbox *m_agentToolbox = Thinker::getInstance()->getAgentToolbox();
    Output *m_agentOutput1 = m_agentToolbox->getNewOutput();

    const string pythonCode1 = ""
    "class Output(object):\n"
    "\tdef __init__(self):\n"
    "\t\tself.outputName = \"DefaultNameOutput\"\n"
    "\t\tself.output = 0\n"
    "\t\n"
    "\tdef setOutput(self, output):\n"
    "\t\tself.output = output\n"
    "\tdef setOutputName(self, outputName):\n"
    "\t\tself.outputName = outputName\n"
    "\tdef getOutput(self):\n"
    "\t\treturn self.output\n"
    "\n"
    "\tdef getOutputName(self):\n"
    "\t\treturn self.outputName\n";

    BuilderCodePy py;
    py.addCode(&m_agentOutput1->generateASTClassNode(py));
    const string pythonCode2 = py.generateCode();

    ASSERT_EQ(pythonCode2, pythonCode1);
}
