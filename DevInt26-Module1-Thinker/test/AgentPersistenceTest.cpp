#include "gtest/gtest.h"
#include "Thinker.h"

TEST(AgentSerialize, AgentPersistence)
{
    Project *project = Thinker::getInstance()->newProject();
    string agentName = "agente de prueba";
    project->addNewAgent(Thinker::getInstance()->getProjectToolbox()
        ->getNewAgent(agentName));

    Json jsonAgent = project->getAgent(agentName)->serialize();

    ASSERT_EQ(jsonAgent["AgentName"], agentName);
}

TEST(AgentDeserialize, AgentPersistence)
{
    Project *project = Thinker::getInstance()->newProject();
    string agentName = "agente de prueba";
    project->addNewAgent(Thinker::getInstance()->getProjectToolbox()
        ->getNewAgent(agentName));
    Agent *agent = project->getAgent(agentName);
    string gameName = "MsPacMan-ram-v0";
    agent->addNewOpenAI(Thinker::getInstance()->getAgentToolbox()
        ->getNewOpenAI(gameName));
    Json jsonAgent = agent->serialize();

    Project *project2 = Thinker::getInstance()->newProject();
    project2->addNewAgent(Thinker::getInstance()->getProjectToolbox()
        ->getNewAgent("Default Agent"));
    Agent *agentCopy = project2->getAgent("Default Agent");
    agentCopy->deserialize(jsonAgent);
    
    ASSERT_EQ(agent->getOpenAI()->getOpenAIName(), agentCopy->getOpenAI()
        ->getOpenAIName());    
}