#include "gtest/gtest.h"
#include "Thinker.h"
#include "OpenAI.h"
#include "Agent.h"
#include <BuilderCodePy.h>
#include <PythonAST.h>
#include <BuilderCode.h>
#include <PythonInjector.h>
#include <string>

TEST(classNodeTensorFlow, areEquals)
{
    Project *project = Thinker::getInstance()->newProject();
    string agentName = "agente de prueba";
    project->addNewAgent(Thinker::getInstance()->getProjectToolbox()
    ->getNewAgent(agentName));

    string tensorFlowName = "Default tensorFlowName";
    project->getAgent(agentName)
        ->addNewTensorFlow(Thinker::getInstance()->getAgentToolbox()
        ->getNewTensorFlow(tensorFlowName));

    TensorFlow *tensorFlowtest = (project->getAgent(agentName))->getTensorFlow();

    string pythonCode1 =""
    "class TensorFlow(object):\n"
    "\tdef __init__(self):\n"
    "\t\tself.tensorFlowName = \"Default tensorFlowName\"\n"
    "\t\tself.step = 0\n"
    "\t\n"
    "\tdef getTensorFlowName(self):\n"
    "\t\treturn self.tensorFlowName\n"
    "\n\tdef setTensorFlowName(self, tensorFlowName):\n"
    "\t\tself.tensorFlowName = tensorFlowName\n"
    "\tdef setStep(self):\n"
    "\t\tpass\n"
    "\tdef getStep(self):\n"
    "\t\treturn self.step\n";

    string pythonCode2;
    BuilderCodePy py;
    py.addCode(&tensorFlowtest->generateASTClassNode(py));
    pythonCode2 = py.generateCode();
    ASSERT_EQ(pythonCode1, pythonCode2);
}
