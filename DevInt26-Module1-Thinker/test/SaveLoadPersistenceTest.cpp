#include "gtest/gtest.h"
#include "Thinker.h"

using namespace std;

bool exists(const string &p_name)
{
    FILE *file = fopen(p_name.c_str(), "r");
    if (file)
    {
        fclose(file);
        return true;
    }
    else
    {
        return false;
    }
}

TEST(VerifySavePersistenciaTest, Persistence)
{
    Project *m_projectSerialize = new Project();
    m_projectSerialize->setFileName("../bin/ProjectDevInt26.json");
    m_projectSerialize->save();

    ASSERT_TRUE(exists("../bin/ProjectDevInt26.json"));
}

TEST(VerifyLoadPersistenciaTest, Persistence)
{
    Project *m_projectDerialize = new Project();
    m_projectDerialize->setFileName("../bin/ProjectDevInt26.json");
    m_projectDerialize->load();

    m_projectDerialize->setFileName("../bin/ProjectDevInt26Copy.json");
    m_projectDerialize->save();

    ASSERT_TRUE(exists("../bin/ProjectDevInt26Copy.json"));
}

TEST(VerifyLoadCorrectPersistenceTest, Persistence){

    ifstream fileJson1("../bin/ProjectDevInt26.json");
    ifstream fileJson2("../bin/ProjectDevInt26Copy.json");

    string genFile1((std::istreambuf_iterator<char>(fileJson1)),
        istreambuf_iterator<char>());

    string genFile2((std::istreambuf_iterator<char>(fileJson2)),
        istreambuf_iterator<char>());

    ASSERT_EQ(genFile1, genFile2);
}
