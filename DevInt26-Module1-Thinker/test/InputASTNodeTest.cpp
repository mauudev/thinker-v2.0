#include "gtest/gtest.h"
#include "Thinker.h"
#include "Output.h"
#include "Agent.h"
#include <BuilderCodePy.h>
#include <PythonAST.h>
#include <string>

TEST(classNodeInput, AreEquals)
{
    Project *m_project = Thinker::getInstance()->newProject();
    ProjectToolbox *m_projectToolbox = Thinker::getInstance()
        ->getProjectToolbox();
    string m_agentName = "agente de prueba";
    Agent *m_agent = m_projectToolbox->getNewAgent(m_agentName);
    m_project->addNewAgent(m_agent);
    Agent *m_agentOfProject = m_project->getAgent(m_agentName);
    AgentToolbox *m_agentToolbox = Thinker::getInstance()->getAgentToolbox();

    Input *m_agentInput1 = m_agentToolbox->getNewInput();

    const string pythonCode1 = ""
    "class Input(object):\n"
    "\tdef __init__(self):\n"
    "\t\tself.inputName = \"DefaultInputName\"\n"
    "\t\tself.input = 0\n"
    "\t\n"
    "\tdef setNextStep(self, nextStep):\n"
    "\t\tself.nextStep = nextStep\n"
    "\tdef getNextStep(self):\n"
    "\t\treturn self.nextStep\n"
    "\n"
    "\tdef setInputName(self, inputName):\n"
    "\t\tself.inputName = inputName\n"
    "\tdef getInputName(self):\n"
    "\t\treturn self.inputName\n";

    BuilderCodePy py;
    py.addCode(&m_agentInput1->generateASTClassNode(py));
    const string pythonCode2 = py.generateCode();

    ASSERT_EQ(pythonCode2, pythonCode1);
}
