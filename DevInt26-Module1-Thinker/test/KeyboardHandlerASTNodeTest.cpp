#include "gtest/gtest.h"
#include "Thinker.h"
#include "KeyboardHandler.h"
#include <BuilderCodePy.h>
#include <PythonAST.h>
#include <BuilderCode.h>
#include <PythonInjector.h>
#include <string>

TEST(classASTNodeEvenKeyboard, event)
{
    Project *m_project = Thinker::getInstance()->newProject();
    string m_agentName = "agente de prueba";
    m_project->addNewAgent(Thinker::getInstance()->getProjectToolbox()
        ->getNewAgent(m_agentName));
    m_project->getAgent(m_agentName)
        ->addNewKeyboardHandler(Thinker::getInstance()
        ->getAgentToolbox()->getNewKeyboardHandler("KeyboardHandler 1"));

    KeyboardHandler *keyboardHandlertest = m_project->getAgent(m_agentName)
        ->getKeyboardHandler();

    string pythonCodeExpected = ""
"class KeyboardHandler(object):\n"
"\tdef __init__(self):\n"
"\t\tself.step = 0\n"
"\t\n"
"\tdef getStep(self):\n"
"\t\treturn self.step\n"
"\n"
"\tdef setStep(self):\n"
"\t\tfor event in pygame.event.get() :\n"
"\t\t\tif event.type == pygame.KEYDOWN:\n"
"\t\t\t\tif event.key == K_UP:\n"
"\t\t\t\t\tself.step = 1\n"
"\t\t\t\telif event.key == K_w:\n"
"\t\t\t\t\tself.step = 1\n"
"\t\t\t\telif event.key == K_KP5:\n"
"\t\t\t\t\tself.step = 1\n"
"\t\t\t\telif event.key == K_DOWN:\n"
"\t\t\t\t\tself.step = 4\n"
"\t\t\t\telif event.key == K_s:\n"
"\t\t\t\t\tself.step = 4\n"
"\t\t\t\telif event.key == K_KP2:\n"
"\t\t\t\t\tself.step = 4\n"
"\t\t\t\telif event.key == K_LEFT:\n"
"\t\t\t\t\tself.step = 3\n"
"\t\t\t\telif event.key == K_a:\n"
"\t\t\t\t\tself.step = 3\n"
"\t\t\t\telif event.key == K_KP1:\n"
"\t\t\t\t\tself.step = 3\n"
"\t\t\t\telif event.key == K_RIGHT:\n"
"\t\t\t\t\tself.step = 2\n"
"\t\t\t\telif event.key == K_d:\n"
"\t\t\t\t\tself.step = 2\n"
"\t\t\t\telif event.key == K_KP3:\n"
"\t\t\t\t\tself.step = 2";


    string pythonCodeResult;
    BuilderCodePy py;
    py.addCode(&keyboardHandlertest->generateASTClassNode(py));
    pythonCodeResult = py.generateCode();
    ASSERT_EQ(pythonCodeExpected, pythonCodeResult);
}
