#include "gtest/gtest.h"
#include "Thinker.h"
#include "RandomHandler.h"
#include <BuilderCodePy.h>
#include <PythonAST.h>
#include <BuilderCode.h>
#include <PythonInjector.h>
#include <string>

TEST(classNodeRandom, creation)
{
    Project *m_project = Thinker::getInstance()->newProject();
    string m_agentName = "agente de prueba";
    m_project->addNewAgent(Thinker::getInstance()->getProjectToolbox()
        ->getNewAgent(m_agentName));
    m_project->getAgent(m_agentName)
        ->addNewRandomHandler(Thinker::getInstance()
        ->getAgentToolbox()->getNewRandomHandler("RandomHandler 1"));

    RandomHandler *RandomHandlertest = m_project->getAgent(m_agentName)
        ->getRandomHandler();

    string pythonCodeExpected = ""
"\n"
"import random\n"
"class RandomHandler(object):\n"
"\tdef __init__(self):\n"
"\t\tself.step = 0\n"
"\t\n"
"\tdef getStep(self):\n"
"\t\treturn self.step\n"s
"\n"
"\tdef setStep(self):\n"
"\t\tself.step = random.choice([0, 1, 2, 3, 4, 5, 6])";

    BuilderCodePy py;
    py.addCode(&RandomHandlertest->generateASTClassNode(py));
    const string pythonCode = py.generateCode();
    ASSERT_EQ(pythonCodeExpected, pythonCode);
}