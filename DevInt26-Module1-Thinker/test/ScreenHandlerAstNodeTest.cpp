#include "gtest/gtest.h"
#include "Thinker.h"
#include <BuilderCodePy.h>
#include <PythonAST.h>
#include <BuilderCode.h>
#include <PythonInjector.h>
#include <string>

TEST(classNodeScreenHandler, areEquals)
{
    Project *project = Thinker::getInstance()->newProject();
    string agentName = "agente de prueba";
    project->addNewAgent(Thinker::getInstance()->getProjectToolbox()
        ->getNewAgent(agentName));

    int speed = 25;
    int screenHeight = 400;
    int screenWidth = 400;
    const string screenName = "Screen Handler";

    project->getAgent(agentName)
        ->addNewScreenHandler(Thinker::getInstance()->getAgentToolbox()
        ->getNewScreenHandler(speed, screenHeight, screenWidth, screenName));

    ScreenHandler *sc = (project->getAgent(agentName))->getScreenHandler();

    const string py_expected = "\n\nimport pygame\nfrom pygame.locals import *\n\nclass ScreenHandler(object):\n\tdef __init__(self):\n\t\tself.screenSpeed = 25\n\t\tself.screenSize = (400,400)\n\t\tself.clock = pygame.time.Clock()\n\t\tself.pygameScreen = pygame.display.set_mode(self.screenSize)\n\t\tself.pygameImage = 0\n\t\n\tdef closeGame(self):\n\t\tpygame.quit()\n\tdef getDisplay(self):\n\t\tpygame.display.flip()\n\tdef setDisplayArray(self, rgbArray):\n\t\tself.clock.tick(self.screenSpeed)\n\t\trgbArrayMin, rgbArrayMax = rgbArray.min(), rgbArray.max()\n\t\trgbArray = ((255.0*(rgbArray-rgbArrayMin))/(rgbArrayMax-rgbArrayMin))\n\t\tpygameImage = pygame.surfarray.make_surface(rgbArray.swapaxes(0, 1) if True else rgbArray)\n\t\tpygameImage = pygame.transform.scale(pygameImage, self.screenSize)\n\t\tself.pygameScreen.blit(pygameImage, (0,0))\n\t\tself.getDisplay()";

    BuilderCodePy py;
    string py_actual;
    py.addCode(&sc->generateASTClassNode(py));
    py_actual = py.generateCode();
    ASSERT_EQ(py_actual, py_expected);
}
