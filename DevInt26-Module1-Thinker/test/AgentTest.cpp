#include "gtest/gtest.h"

#include "Thinker.h"

TEST(GetCustomAgentName, myAgentTest)
{
    Project *project = Thinker::getInstance()->newProject();
    string agentName = "agente de prueba";
    project->addNewAgent(Thinker::getInstance()->getProjectToolbox()
    ->getNewAgent(agentName));
    ASSERT_EQ(agentName, project->getAgent(agentName)->getAgentName());
}

TEST(GetDefaultAgentName, myAgentTest)
{
    string agentName = "agente de prueba";
    Project *project = Thinker::getInstance()->newProject();
    project->addNewAgent(Thinker::getInstance()->getProjectToolbox()
    ->getNewAgent("Default Agent"));
    ASSERT_NE(agentName, project->getAgent("Default Agent")->getAgentName());
}

TEST(nullAgent, myAgentTest)
{
    Project *project = Thinker::getInstance()->newProject();
    project->addNewAgent(Thinker::getInstance()->getProjectToolbox()
    ->getNewAgent("Default Agent"));
    ASSERT_FALSE(project->getAgent("Agent 1"));
}
