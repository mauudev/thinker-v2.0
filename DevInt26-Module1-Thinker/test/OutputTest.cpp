#include "gtest/gtest.h"
#include "Thinker.h"

using namespace std;

TEST(OutputTest, VerifyNotOutput)
{
    //create a project
    Project *m_project = Thinker::getInstance()->newProject();

    // we ask for a projectToolbox to Thinker
    ProjectToolbox *m_projectToolbox = Thinker::getInstance()
        ->getProjectToolbox();

    // we create an agent
    string m_agentName = "agente de prueba";
    Agent *m_agent = m_projectToolbox->getNewAgent(m_agentName);

    // we added the agent to the project
    m_project->addNewAgent(m_agent);

    // we create a reference of an agent that is in a project by its name
    Agent *m_agentOfProject = m_project->getAgent(m_agentName);

    // we ask for an agentToolbox to the Thinker
    AgentToolbox *m_agentToolbox = Thinker::getInstance()->getAgentToolbox();

    // from the agentToolbox we create the agent output
    Output *m_agentOutput1 = m_agentToolbox->getNewOutput();

    // get reference of agent1
    Output *m_agentOutputReference0 = m_agentOfProject
        ->getOutput();

    // test 1 verifies that there is no data output for an agent
    ASSERT_EQ (m_agentOfProject->getOutput(), nullptr);

    // test 2 verifies agent Output was created
    ASSERT_NE (m_agentOutput1, nullptr);
}

TEST(CreateOneOutputTest, VerifyAddNewOutputAgent)
{
    //create a project
    Project *m_project = Thinker::getInstance()->newProject();

    // we ask for a projectToolbox to Thinker
    ProjectToolbox *m_projectToolbox = Thinker::getInstance()
        ->getProjectToolbox();

    // we create an agent
    string m_agentName = "agente de prueba";
    Agent *m_agent = m_projectToolbox->getNewAgent(m_agentName);

    // we added the agent to the project
    m_project->addNewAgent(m_agent);

    // we create a reference of an agent that is in a project by its name
    Agent *m_agentOfProject = m_project->getAgent(m_agentName);

    // we ask for an agentToolbox to the Thinker
    AgentToolbox *m_agentToolbox = Thinker::getInstance()->getAgentToolbox();

    // from the agentToolbox we create the agent output
    Output *m_agentOutput1 = m_agentToolbox->getNewOutput();

    // get reference of agent1
    Output *m_agentOutputReference0 = m_agentOfProject
        ->getOutput();

    // we add a data output to an agent
    m_agentOfProject->addNewOutput(m_agentOutput1);

    // test 3: verify that there is a data output for an agent
    ASSERT_NE (m_agentOfProject->getOutput(), nullptr);
}

TEST(CreateTwoOutputTest, VerifyAddNewOutputAgent)
{
    //create a project
    Project *m_project = Thinker::getInstance()->newProject();

    // we ask for a projectToolbox to Thinker
    ProjectToolbox *m_projectToolbox = Thinker::getInstance()
        ->getProjectToolbox();

    // we create an agent
    string m_agentName = "agente de prueba";
    Agent *m_agent = m_projectToolbox->getNewAgent(m_agentName);

    // we added the agent to the project
    m_project->addNewAgent(m_agent);

    // we create a reference of an agent that is in a project by its name
    Agent *m_agentOfProject = m_project->getAgent(m_agentName);

    // we ask for an agentToolbox to the Thinker
    AgentToolbox *m_agentToolbox = Thinker::getInstance()->getAgentToolbox();

    // from the agentToolbox we create the agent output
    Output *m_agentOutput1 = m_agentToolbox->getNewOutput();

    // get reference of agent1
    Output *m_agentOutputReference0 = m_agentOfProject
        ->getOutput();

    // we add a data output to an agent
    m_agentOfProject->addNewOutput(m_agentOutput1);

    //create other agentOutput
    Output *m_agentOutput2 = m_agentToolbox->getNewOutput();
    m_agentOfProject->addNewOutput(m_agentOutput2);

    // test 4 verify that agentOfProject have other agentOutput
    ASSERT_NE (m_agentOfProject->getOutput(), m_agentOutput1);
}