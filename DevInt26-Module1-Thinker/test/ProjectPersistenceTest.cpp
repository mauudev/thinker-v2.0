#include "gtest/gtest.h"
#include "Thinker.h"

TEST(Serializem_projectTest, serialize)
{
    Project *m_project = Thinker::getInstance()->newProject();
    m_project->addNewAgent(Thinker::getInstance()->getProjectToolbox()
    ->getNewAgent("Agent 1"));
    m_project->addNewAgent(Thinker::getInstance()->getProjectToolbox()
    ->getNewAgent("Agent 2"));
    m_project->addNewAgent(Thinker::getInstance()->getProjectToolbox()
    ->getNewAgent("Agent 3"));
    
    Json m_jsonPoject = m_project->serialize();

    ASSERT_NE(m_jsonPoject, nullptr);
    ASSERT_EQ(m_jsonPoject["Agents"].size(), 3);
    ASSERT_EQ(m_jsonPoject["ProjectName"], "Default Project Name");
}

TEST(Deserializem_projectTest, deserialize)
{
    Project *m_project = Thinker::getInstance()->newProject();
    m_project->addNewAgent(Thinker::getInstance()->getProjectToolbox()
    ->getNewAgent("Agent 1"));
    m_project->addNewAgent(Thinker::getInstance()->getProjectToolbox()
    ->getNewAgent("Agent 2"));
    m_project->addNewAgent(Thinker::getInstance()->getProjectToolbox()
    ->getNewAgent("Agent 3"));

    Json m_jsonPoject = m_project->serialize();

    Project *m_project2 = Thinker::getInstance()->newProject();
    m_project2->deserialize(m_jsonPoject);
    Json m_jsonPoject2 = m_project2->serialize();

    ASSERT_EQ(m_jsonPoject, m_jsonPoject2);
}