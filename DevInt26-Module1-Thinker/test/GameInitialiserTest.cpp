#include "gtest/gtest.h"
#include "Thinker.h"
#include <vector>

TEST(checkListRecovery, myGameInitialiserTest)
{
    Project *project = Thinker::getInstance()->newProject();
    string agentName = "agente de prueba";
    project->addNewAgent(Thinker::getInstance()->getProjectToolbox()
        ->getNewAgent(agentName));

     string gameName = "MsPacMan-ram-v0";
     project->getAgent(agentName)
        ->addNewOpenAI(Thinker::getInstance()->getAgentToolbox()
        ->getNewOpenAI(gameName));

    vector<string> games = project->getAgent(agentName)->getOpenAI()
        ->getVectorGame();

    vector<string> m_gamesTest;
    m_gamesTest.push_back("Asterix-ram-v0");
    m_gamesTest.push_back("Boxing-ram-v0");
    m_gamesTest.push_back("BeamRider-ram-v0");
    m_gamesTest.push_back("CrazyClimber-ra,-v0");
    m_gamesTest.push_back("DemonAttack-ram-v0");
    m_gamesTest.push_back("MsPacMan-ram-v0");
    m_gamesTest.push_back("Phoenix-ram-v0");
    m_gamesTest.push_back("Skiing-ram-v-0");
    m_gamesTest.push_back("Solaris-ra,-v0");
    m_gamesTest.push_back("TimePilot-ra,-v0");


    bool gameExist = true;

    for (int i = 0; i < games.size(); ++i)
    {
        if(games[i] != m_gamesTest[i])
        {
            gameExist = false; 
            break;
        }

    }

    ASSERT_EQ(gameExist, true);

}

TEST(checkGameExist, myGameInitialiserTest)
{
    Project *project = Thinker::getInstance()->newProject();
    string agentName = "agente de prueba";
    project->addNewAgent(Thinker::getInstance()->getProjectToolbox()
        ->getNewAgent(agentName));

     string gameName = "MsPacMan-ram-v0";
     project->getAgent(agentName)
        ->addNewOpenAI(Thinker::getInstance()->getAgentToolbox()
        ->getNewOpenAI(gameName));

    vector<string> games = project->getAgent(agentName)->getOpenAI()
        ->getVectorGame();

    string nameGame = project->getAgent(agentName)->getOpenAI()
        ->getOpenAIName();

    bool gameExist = false;

    for (int i = 0; i < games.size(); ++i)
    {
        if(nameGame == games[i])
        {
            gameExist = true;
            break;
        }
    }

    ASSERT_EQ(gameExist, true);
}
