#include "gtest/gtest.h"
#include "Thinker.h"

TEST(Serializem_randomHandlerTest, serialize)
{
    Project *m_project = Thinker::getInstance()->newProject();
    m_project->addNewAgent(Thinker::getInstance()->getProjectToolbox()
        ->getNewAgent("Agent"));

    string randomHandlerName = "First RandomHandler";

    m_project->getAgent("Agent")
        ->addNewRandomHandler(Thinker::getInstance()->getAgentToolbox()
        ->getNewRandomHandler(randomHandlerName));

    Json m_jsonRandom = m_project->getAgent("Agent")->getRandomHandler()
        ->serialize();

    ASSERT_EQ(m_jsonRandom["RandomName"], randomHandlerName);
}

TEST(Deserializem_randomHandlerTest, deserialize)
{
    Project *m_project = Thinker::getInstance()->newProject();
    m_project->addNewAgent(Thinker::getInstance()->getProjectToolbox()
        ->getNewAgent("Agent"));

    string randomHandlerName = "First RandomHandler";

    m_project->getAgent("Agent")
        ->addNewRandomHandler(Thinker::getInstance()->getAgentToolbox()
        ->getNewRandomHandler(randomHandlerName));

    Json m_jsonrandom = m_project->getAgent("Agent")->getRandomHandler()
        ->serialize();

    RandomHandler *random = Thinker::getInstance()->getProject()
        ->getAgent("Agent")->getRandomHandler();

    random->deserialize(m_jsonrandom);

    Json m_jsonrandom2 = random->serialize();

    ASSERT_EQ(m_jsonrandom2, m_jsonrandom);
}

