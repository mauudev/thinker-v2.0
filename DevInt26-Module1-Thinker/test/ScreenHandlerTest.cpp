#include "gtest/gtest.h"
#include "Thinker.h"

TEST(GetSpeedScreenHandler, myScreenHandlerTest)
{
    Project *project = Thinker::getInstance()->newProject();
    string agentName = "agente de prueba";
    project->addNewAgent(Thinker::getInstance()->getProjectToolbox()
        ->getNewAgent(agentName));

    int screenSpeed = 25;
    int screenHeight = 400;
    int screenWidth = 400;
    const string screenName = "Screen Handler";

    project->getAgent(agentName)
        ->addNewScreenHandler(Thinker::getInstance()->getAgentToolbox()
        ->getNewScreenHandler(screenSpeed, screenHeight, screenWidth, screenName));

    ASSERT_EQ(screenSpeed, project->getAgent(agentName)->getScreenHandler()
        ->getScreenHandlerSpeed());
}

TEST(GetHeightScreenHandler, myScreenHandlerTest)
{
    Project *project = Thinker::getInstance()->newProject();
    string agentName = "agente de prueba";
    project->addNewAgent(Thinker::getInstance()->getProjectToolbox()
        ->getNewAgent(agentName));

    int screenSpeed = 25;
    int screenHeight = 400;
    int screenWidth = 400;
    const string screenName = "Screen Handler";

    project->getAgent(agentName)
        ->addNewScreenHandler(Thinker::getInstance()->getAgentToolbox()
        ->getNewScreenHandler(screenSpeed, screenHeight, screenWidth, screenName));

    ASSERT_EQ(screenHeight, project->getAgent(agentName)->getScreenHandler()
        ->getScreenHandlerHeight());
}

TEST(GetWidthScreenHandler, myScreenHandlerTest)
{
    Project *project = Thinker::getInstance()->newProject();
    string agentName = "agente de prueba";
    project->addNewAgent(Thinker::getInstance()->getProjectToolbox()
        ->getNewAgent(agentName));

    int screeSpeed = 25;
    int screenHeight = 400;
    int screenWidth = 400;
    const string screenName = "Screen Handler";

    project->getAgent(agentName)
        ->addNewScreenHandler(Thinker::getInstance()->getAgentToolbox()
        ->getNewScreenHandler(screeSpeed, screenHeight, screenWidth, screenName));

    ASSERT_EQ(screenWidth, project->getAgent(agentName)->getScreenHandler()
        ->getScreenHandlerWidth());
}

TEST(IfScreenHandlerisNull, myScreenHandlerTest)
{
    Project *project = Thinker::getInstance()->newProject();
    string agentName = "agente de prueba";
    project->addNewAgent(Thinker::getInstance()->getProjectToolbox()
        ->getNewAgent(agentName));

    Agent *m_agent = project->getAgent(agentName);

    ASSERT_EQ (m_agent->getScreenHandler(), nullptr);
}

TEST(ItoolScreenHandler, getClassNameScreenHandler)
{
    Project *project = Thinker::getInstance()->newProject();
    string agentName = "agente de prueba";
    project->addNewAgent(Thinker::getInstance()->getProjectToolbox()
        ->getNewAgent(agentName));

    int screenSpeed = 25;
    int screenHeight = 400;
    int screenWidth = 400;
    const string screenName = "Screen Handler";

    project->getAgent(agentName)
        ->addNewScreenHandler(Thinker::getInstance()->getAgentToolbox()
        ->getNewScreenHandler(screenSpeed, screenHeight, screenWidth, screenName));

    ASSERT_EQ("ScreenHandler", project->getAgent(agentName)->getScreenHandler()
        ->getClassName());
}


