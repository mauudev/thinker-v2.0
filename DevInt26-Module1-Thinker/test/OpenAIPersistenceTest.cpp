#include "gtest/gtest.h"
#include "Thinker.h"

TEST(GetCustomOpenAISerialize, myOpenAISerializeTest)
{
    Project &project = Thinker::getInstance().newProject();
    string agentName = "agente de prueba";
    project.addNewAgent(Thinker::getInstance().getProjectToolbox().getNewAgent(agentName));

    string gameName = "MsPacMan-ram-v0";
    project.getAgent(agentName)
        .addNewOpenAI(Thinker::getInstance().getAgentToolbox()
        .getNewOpenAI(gameName));

    int screenSpeed = 25;
    int screenHeight = 400;
    int screenWidth = 500;
    const string screenName = "Screen Handler";

    project.getAgent(agentName)
        .addNewScreenHandler(Thinker::getInstance().getAgentToolbox()
        .getNewScreenHandler(screenSpeed, screenHeight, screenWidth, screenName));

    string keyboardHandlerName = "First KeyboardHandler";

    project.getAgent(agentName)
        .addNewKeyboardHandler(Thinker::getInstance().getAgentToolbox()
        .getNewKeyboardHandler(keyboardHandlerName));

    project.getAgent(agentName).linkTools(gameName, screenName);
    project.getAgent(agentName).linkTools(gameName, keyboardHandlerName);

    Json jsonOpenAI = project.getAgent(agentName).getOpenAI(gameName).serialize();

    ASSERT_EQ(jsonOpenAI["openAIName"], gameName);
}

// TEST(GetOpenAIDeserialize, myOpenAIDeserializeTest)
// {
//     Project *project = Thinker::getInstance().newProject();
//     string agentName = "agente de prueba";
//     project.addNewAgent(Thinker::getInstance()->getProjectToolbox()
//         ->getNewAgent(agentName));

//     string gameName = "MsPacMan-ram-v0";
//     project->getAgent(agentName)
//         ->addNewOpenAI(Thinker::getInstance()->getAgentToolbox()
//         ->getNewOpenAI(gameName));
//     Json jsonOpenAI = project->getAgent(agentName)->getOpenAI()->serialize();

//     string agentName1 = "agentito de prueba";
//     project->addNewAgent(Thinker::getInstance()->getProjectToolbox()
//         ->getNewAgent(agentName1));

//     project->getAgent(agentName1)->addNewOpenAI(Thinker::getInstance()
//         ->getAgentToolbox()->getNewOpenAI("Default"));

//     project->getAgent(agentName1)->getOpenAI()->deserialize(jsonOpenAI);

//     ASSERT_EQ(gameName, project->getAgent(agentName1)->getOpenAI()
//         ->getOpenAIName());
// }

