#include "gtest/gtest.h"
#include <BuilderCodePy.h>
#include <INodeAST.h>

#include "Thinker.h"

using namespace std;

TEST(GenerateAgentASTNodeTest, AST)
{
    Project *project = Thinker::getInstance()->newProject();
    string agentName = "Defaul Agent Name";
    project->addNewAgent(Thinker::getInstance()->getProjectToolbox()
        ->getNewAgent(agentName));

    BuilderCodePy py;
    string compare = ""
    "class Agent(object):\n"
    "\tdef __init__(self):\n"
        "\t\tself.agentName = \"Defaul Agent Name\"\n"
        "\t\tself.openAI = OpenAI()\n"
        "\t\tself.screen = ScreenHandler()\n"
        "\t\tself.keyboard = KeyboardHandler()\n"
        "\t\n"
    "\tdef setAgentName(self, agentName):\n"
        "\t\tself.agentName = agentName\n"
    "\tdef getAgentName(self):\n"
        "\t\treturn self.agentName\n"
        "\n"
    "\tdef execute(self):\n"
        "\t\twhile not self.openAI.getStatusOpenAI():\n"
            "\t\t\tself.keyboard.setStep()\n"
            "\t\t\tself.openAI.resetEnvironment(self.keyboard.getStep())\n"
            "\t\t\tself.screen.setDisplayArray(self.openAI.getRGBArray())\n"
        "\t\tself.screen.closeGame()";

    Agent *agent = project->getAgent(agentName);
    py.addCode(&agent->generateASTClassNode(py));
    string script;
    script = py.generateCode();


    ASSERT_EQ(compare, script);
}

TEST(GenerateAllASTNodeTest, AST)
{
    Project *project = Thinker::getInstance()->newProject();
    string agentName = "Defaul Agent Name";
    project->addNewAgent(Thinker::getInstance()->getProjectToolbox()
        ->getNewAgent(agentName));

    string gameName = "MsPacMan-ram-v0";

    Agent *agent = project->getAgent(agentName);

    AgentToolbox *agentToolbox = Thinker::getInstance()->getAgentToolbox();

    size_t speed = 25;
    size_t screenHeight = 400;
    size_t screenWidth = 400;
    string screenHandlerName = "Screen Handler";

    project->getAgent(agentName)->addNewOpenAI(Thinker::getInstance()
    ->getAgentToolbox()->getNewOpenAI(gameName));

    project->getAgent(agentName)
        ->addNewScreenHandler(Thinker::getInstance()->getAgentToolbox()
        ->getNewScreenHandler(speed, screenHeight, screenWidth, screenHandlerName));

    project->getAgent(agentName)->addNewKeyboardHandler(Thinker::getInstance()
        ->getAgentToolbox()->getNewKeyboardHandler("KeyboardHandler"));

    project->getAgent(agentName)->addNewOutput(Thinker::getInstance()
        ->getAgentToolbox()->getNewOutput());

    project->getAgent(agentName)->addNewInput(Thinker::getInstance()
        ->getAgentToolbox()->getNewInput());

    project->getAgent(agentName)->addNewTensorFlow(Thinker::getInstance()
        ->getAgentToolbox()->getNewTensorFlow("Default TensorFlow"));

    BuilderCodePy py;

    const string script = py.generateCode(&agent->buildASTNode(py));

    const string code = agent->generateCode();

    ASSERT_EQ(code, script);
}
