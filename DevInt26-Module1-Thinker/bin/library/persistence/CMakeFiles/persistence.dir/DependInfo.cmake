# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/root/Desktop/DevInt26-Module1-Thinker/library/persistence/source/Persistence.cpp" "/root/Desktop/DevInt26-Module1-Thinker/bin/library/persistence/CMakeFiles/persistence.dir/source/Persistence.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "GTEST"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../library/python-ast/include"
  "/usr/include/glib-2.0"
  "/usr/lib/x86_64-linux-gnu/glib-2.0/include"
  "../library/PythonBEER/include"
  "../include"
  "../library/persistence/include"
  "../thirdparty/jsonlib"
  "../thirdparty/googletest/googletest/include"
  "../thirdparty/googletest/googletest"
  "../library/persistence/../../thirdparty/jsonlib"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
