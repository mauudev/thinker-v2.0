# CMake generated Testfile for 
# Source directory: /root/Desktop/DevInt26-Module1-Thinker
# Build directory: /root/Desktop/DevInt26-Module1-Thinker/bin
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(thinker "thinker")
add_test(tests "tests")
subdirs(thirdparty/googletest)
subdirs(library/persistence)
subdirs(library/python-ast)
subdirs(library/PythonBEER)
