# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/root/Desktop/DevInt26-Module1-Thinker/thirdparty/googletest/googletest/src/gtest-all.cc" "/root/Desktop/DevInt26-Module1-Thinker/bin/thirdparty/googletest/googlemock/gtest/CMakeFiles/gtest.dir/src/gtest-all.cc.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "GTEST"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../thirdparty/googletest/googletest/include"
  "../thirdparty/googletest/googletest"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
